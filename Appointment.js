import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  FlatList,
  Dimensions,
  ActivityIndicator,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
const GLOBAL = require('./Global');
import Header from './Header.js';
const window = Dimensions.get('window');
import moment from 'moment';
import Button from 'react-native-button';
import {TextField} from 'react-native-material-textfield-plus';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class Appointment extends Component {
  state = {
    name: '',
    email: '',
    phone: '',
    company: '',
    loading: false,
    results: [],
    visible: false,
  };

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
    };
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  componentDidMount() {
    //        this.loadAppointments()
    this.props.navigation.addListener('willFocus', this._handleStateChange);
  }

  _handleStateChange = state => {
    this.loadAppointments();
  };

  loadAppointments() {
    const url = GLOBAL.BASE_URL + 'list_apointment';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        user_id: GLOBAL.user_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(JSON.stringify(responseJson.list_appointment));

        if (responseJson.status == true) {
          this.setState({results: responseJson.list_appointment});
        } else {
          this.setState({results: []});
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  }

  confirmCancel = (item, index) => {
    this.showLoading();
    const url = GLOBAL.BASE_URL + 'cancel_appointment';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        booking_id: item.booking_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        this.hideLoading();
        //                alert(JSON.stringify(responseJson))

        if (responseJson.status == true) {
          alert('Appointment cancelled successfully!');
          this.loadAppointments();
        } else {
          alert('Something went wrong!');
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  };
  onPressResc = (item, index) => {
    GLOBAL.appointment_details = item;
    this.props.navigation.navigate('AppointmentResc');
  };

  onPressCancel = (item, index) => {
    Alert.alert(
      'Cancel Appointment',
      'Are you sure you want to cancel this appointment?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Confirm',
          onPress: () => this.confirmCancel(item, index),
        },
      ],
      {
        cancelable: false,
      },
    );
    return true;
  };

  openDetail = item => {
    GLOBAL.appointment_details = item;
    this.props.navigation.navigate('AppointmentDetail');
  };

  postRating = (item, index) => {
    GLOBAL.appointment_details = item;
    this.props.navigation.navigate('RatingPost');
  };

  viewRating = (item, index) => {
    GLOBAL.reviews = item;
    this.props.navigation.navigate('RatingView');
  };

  renderItem = ({item, index}) => {
    var bdate = moment(item.appointment_date).format('DD-MM-YY');

    return (
      <TouchableOpacity
        style={{
          marginTop: 10,
          marginLeft: 15,
          marginBottom: 10,
          marginRight: 15,
        }}
        onPress={() => this.openDetail(item)}
        activeOpacity={0.6}>
        <View
          style={{
            backgroundColor: 'white',
            color: 'white',
            flexDirection: 'column',
            flex: 1,
            height: 'auto',
            borderRadius: 6,
            width: Dimensions.get('window').width - 30,
            shadowColor: '#D3D3D3',
            shadowOffset: {width: 0, height: 1},
            shadowOpacity: 0.6,
            shadowRadius: 2,
          }}>
          {item.module == '1' && item.booking_type == 'Emergency Visit' && (
            <View>
              <Text
                style={{
                  color: 'black',
                  fontSize: 14,
                  fontFamily: 'Konnect-Medium',
                  marginTop: 8,
                }}>
                {' '}
                Appointment ID : {item.appointment_id}
              </Text>
              <Text
                style={{
                  color: 'black',
                  fontSize: 14,
                  fontFamily: 'Konnect-Medium',
                  marginTop: 8,
                }}>
                {' '}
                Booking Type : {item.booking_type}
              </Text>
              <Text
                style={{
                  color: 'black',
                  fontSize: 14,
                  fontFamily: 'Konnect-Medium',
                  marginTop: 8,
                }}>
                {' '}
                Booking ID : {item.booking_id}
              </Text>
              <Text
                style={{
                  color: 'black',
                  fontSize: 14,
                  fontFamily: 'Konnect-Medium',
                  marginTop: 8,
                }}>
                {' '}
                Booking Status : {item.booking_status}
              </Text>
            </View>
          )}

          {item.booking_type != 'Emergency Visit' && (
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{
                  width: 70,
                  height: 70,
                  borderRadius: 35,
                  marginTop: 15,
                  marginLeft: 5,
                }}
                source={{uri: item.doctor_image}}
              />

              <View
                style={{
                  flexDirection: 'column',
                  marginTop: 15,
                  marginLeft: 10,
                  width: '100%',
                }}>
                <Text
                  style={{
                    color: 'black',
                    fontSize: 14,
                    fontFamily: 'Konnect-Medium',
                    marginTop: 8,
                  }}>
                  {item.doctor_name}
                </Text>
                <Text
                  style={{
                    color: 'grey',
                    fontSize: 14,
                    fontFamily: 'Konnect-Medium',
                    marginTop: 15,
                    width: '85%',
                  }}>
                  Booking For: {item.booking_for}
                </Text>
                <Text
                  style={{
                    color: 'grey',
                    fontSize: 14,
                    fontFamily: 'Konnect-Medium',
                    marginTop: 8,
                    width: '85%',
                  }}>
                  Booking Type: {item.booking_type}
                </Text>
                <Text
                  style={{
                    color: 'grey',
                    fontSize: 14,
                    fontFamily: 'Konnect-Medium',
                    marginTop: 8,
                  }}>
                  Booking Status: {item.booking_status}
                </Text>
                {/*                            <Text style={{color:'grey', fontSize:14,fontFamily:'Konnect-Medium',marginTop:8}}>{item.remain_days} Days Remaining</Text>*/}
                <Text
                  style={{
                    color: 'rgb(80, 80, 80)',
                    fontSize: 14,
                    fontFamily: 'Konnect-Medium',
                    marginTop: 8,
                  }}>
                  {bdate}, {item.appointment_time}
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 20,
                    marginBottom: 15,
                  }}>
                  {item.reshedule_power == 1 && (
                    <Button
                      style={{
                        fontSize: 15,
                        color: '#31c6f2',
                        fontFamily: 'Konnect-Medium',
                        marginRight: 50,
                      }}
                      containerStyle={{
                        overflow: 'hidden',
                        justifyContent: 'center',
                      }}
                      onPress={() => this.onPressResc(item, index)}>
                      RESCHEDULE
                    </Button>
                  )}
                  {item.cancel_power == 1 && (
                    <Button
                      style={{
                        fontSize: 15,
                        color: '#FF0000',
                        fontFamily: 'Konnect-Medium',
                      }}
                      containerStyle={{
                        overflow: 'hidden',
                        justifyContent: 'center',
                      }}
                      onPress={() => this.onPressCancel(item, index)}>
                      CANCEL
                    </Button>
                  )}
                </View>
              </View>
            </View>
          )}
          {item.status == '1' && item.rating_flag == 0 && (
            <Button
              style={{
                fontSize: 13,
                color: 'white',
                fontFamily: 'Konnect-Medium',
              }}
              onPress={() => {
                this.postRating(item, index);
              }}
              containerStyle={{
                overflow: 'hidden',
                padding: 7,
                height: 30,
                borderRadius: 5,
                justifyContent: 'center',
                marginLeft: 50,
                marginBottom: 10,
                backgroundColor: '#800000',
                position: 'absolute',
                top: 20,
                right: 10,
              }}>
              Rate Doctor
            </Button>
          )}

          {item.status == '1' && item.rating_flag == 1 && (
            <Button
              style={{
                fontSize: 13,
                color: 'white',
                fontFamily: 'Konnect-Medium',
              }}
              onPress={() => {
                this.viewRating(item, index);
              }}
              containerStyle={{
                overflow: 'hidden',
                padding: 7,
                height: 30,
                borderRadius: 5,
                justifyContent: 'center',
                marginLeft: 50,
                marginBottom: 10,
                backgroundColor: '#800000',
                position: 'absolute',
                top: 20,
                right: 10,
              }}>
              View Rating
            </Button>
          )}
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header
          navigation={this.props.navigation}
          headerName={'YOUR APPOINTMENTS'}
        />
        {this.state.results.length == 0 && (
          <Text
            style={{
              fontSize: 13,
              marginTop: 15,
              color: 'black',
              fontFamily: 'Konnect-Medium',
              alignSelf: 'center',
              textAlign: 'center',
            }}>
            No Appointments booked yet!
          </Text>
        )}

        {this.state.results.length != 0 && (
          <FlatList
            data={this.state.results}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderItem}
            extraData={this.state}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    flex: 1,
    backgroundColor: '#f1f1f1',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },

  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
});
