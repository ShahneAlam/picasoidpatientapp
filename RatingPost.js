import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Alert,
    TouchableOpacity,
    TextInput,
    Image,
    ImageBackground,
    Linking,
    FlatList,
    Dimensions,
    ActivityIndicator
} from 'react-native';
const window = Dimensions.get('window');
const GLOBAL = require('./Global');
import React, {Component} from 'react';
import Button from 'react-native-button';
import ImagePicker from 'react-native-image-picker';
import Header from './Header.js';
import StarRating from 'react-native-star-rating';


class RatingPost extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            avatarSource: null,
            image:'',
            addmore:[],
            startedadd:0,
            loading:false,
            starCount:1,
            review:'',
            disabled:true
        }        
    }



    componentWillUnmount(){

    }



    static navigationOptions = ({ navigation }) => {
        return {
              header: () => null,

        }
    }


    showLoading() {
        this.setState({loading: true})
    }

    hideLoading() {
        this.setState({loading: false})
    }

//     renderItem1=({item,index}) => {
//         alert(JSON.stringify(item))
//         return(

//    <View style={{backgroundColor:'white',color :'white',flexDirection:'row' , margin: 10, shadowColor: '#000',
//             shadowOffset: { width: 0, height: 1 },
//             shadowOpacity: 0.6,
//             shadowRadius: 2,
//             elevation: 2,
//             height:110,
//             borderRadius:5  }}>
// {/*            <Image style={{width:90, height:90, resizeMode:'contain', margin:10}} source={require('./profile.png')}/>*/}
//             <View style={{flexDirection:'column', marginTop:20, marginLeft:10, marginRight:10}}>
//                 <Text style={{color:'black',fontFamily:"Konnect-Regular", fontSize:20, color:'#800000'}}>Dr. Priya Duass</Text>
//                 <Text style={{color:'black',fontFamily:"Konnect-Regular", fontSize:15, marginTop:5}}>MBBS, MS - Ophthalmology</Text>
//                 <Text style={{color:'black', fontFamily:"Konnect-Regular",fontSize:15}}>Ophthalmologist/Eye Surgeon</Text>

//             </View>
//         </View>

//     )
//     }


submitReview=()=>{
//    alert(JSON.stringify(GLOBAL.appointment_details))
console.log(JSON.stringify({
                "user_id": GLOBAL.user_id,
                "doctor_id": GLOBAL.appointment_details.doctor_id,
                "review": this.state.review,
                "rating": this.state.starCount,
                "booking_id": GLOBAL.appointment_details.booking_id
                 
            }))

        const url = GLOBAL.BASE_URL +  'rating_doctor'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },


            body: JSON.stringify({
                "user_id": GLOBAL.user_id,
                "doctor_id": GLOBAL.appointment_details.doctor_id,
                "review": this.state.review,
                "rating": this.state.starCount,
                "booking_id": GLOBAL.appointment_details.booking_id
                 
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson))
                if (responseJson.status == true) {
                    this.props.navigation.goBack()
                    alert('Review submitted successfully.')
                }else{
                    alert('Something went wrong!')
                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });

}

onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }
    render() {

        return(
            <View style={{flex:1}}>
            <Header navigation={this.props.navigation}
            headerName={'RATE DOCTOR'}/>

   <View style={{backgroundColor:'white',color :'white',flexDirection:'row' , margin: 10, shadowColor: '#000',
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.6,
            shadowRadius: 2,
            elevation: 2,
            height:'auto',
            borderRadius:5  }}>
            <Image style={{width:90, height:90, margin:10, borderRadius:45}} source={{uri: GLOBAL.mypimage}}/>
            <View style={{flexDirection:'column', marginTop:10, marginLeft:10, marginRight:10, width:'67%', }}>
                <Text style={{color:'black',fontFamily:"Konnect-Regular", fontSize:20, color:'#800000'}}>{GLOBAL.myname}</Text>
    <StarRating containerStyle={{width:'40%', marginTop:5, marginBottom:5}}
        disabled={false}
        maxStars={5}
        fullStarColor={'#800000'}        
        starSize={22}        
        rating={parseInt(this.state.starCount)}
        selectedStar={(rating) => this.onStarRatingPress(rating)}
      />
     <Text style={{fontSize:15,fontFamily:'Konnect-Medium',color:'black',marginTop:10}}>Write your Review</Text>

      <TextInput style={{ fontSize: 16, borderWidth: 1,width:'100%', borderColor:'lightgrey', marginBottom:10, height:150}}
           placeholder="comment goes here..."
           returnKeyType='go'
           onChangeText={(text)=> this.setState({review : text})}
           autoCorrect={false}
           value ={this.state.review}
           onSubmitEditing = {() => this.setState({disabled:false})}
           textAlignVertical= {'top'}
        />

        </View>

        </View>
                    <Button
                        style={{padding:8,marginTop:'20%',fontSize: 20, color: 'white',backgroundColor:'#800000',marginLeft:'5%',width:'90%',height:40,fontFamily:'Konnect-Medium',borderRadius:4}}
                        styleDisabled={{color: 'black',backgroundColor:'grey'}}
                        disabled={this.state.disabled}
                        onPress={() => this.submitReview()}>
                        SUBMIT
                    </Button>

            </View>
        );
    }
}

export default RatingPost;