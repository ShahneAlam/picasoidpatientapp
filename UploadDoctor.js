import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  StatusBar,
  Modal,
  Linking,
} from 'react-native';
import Button from 'react-native-button';
const window = Dimensions.get('window');
import Header from './Header.js';
const GLOBAL = require('./Global');
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};

export default class UploadDoctor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recognized: '',
      started: '',
    };
  }

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
    };
  };

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    const url = GLOBAL.BASE_URL + 'doctor_booking_users_attachments';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        user_id: GLOBAL.user_id,
        limit_from: '0',
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //  console.log(JSON.stringify(responseJson))

        if (responseJson.status == true) {
          //                    this.setState({tone: 1})
          this.setState({results: responseJson.list});
        } else {
          this.setState({results: []});
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  };

  renderRowItem1s = itemData => {
    return (
      <View
        style={{
          width: '30%',
          margin: 10,
          height: 'auto',
          shadowColor: '#000',
          backgroundColor: 'white',
        }}>
        <TouchableOpacity onPress={() => Linking.openURL(itemData.item)}>
          <Image
            style={{
              width: '100%',
              height: 150,
              resizeMode: 'cover',
              alignSelf: 'center',
            }}
            source={{uri: itemData.item}}
          />

          {/*                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80}}>
                    {itemData.item.image}
                </Text>
                */}
        </TouchableOpacity>
      </View>
    );
  };

  renderRowItem1ss = itemData => {
    return (
      <View
        style={{
          width: '30%',
          margin: 10,
          height: 'auto',
          shadowColor: '#000',
          backgroundColor: 'white',
        }}>
        <TouchableOpacity onPress={() => Linking.openURL(itemData.item)}>
          <Image
            style={{
              width: '100%',
              height: 150,
              resizeMode: 'contain',
              alignSelf: 'center',
            }}
            source={{uri: itemData.item}}
          />

          {/*                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80}}>
                    {itemData.item.image}
                </Text>
                */}
        </TouchableOpacity>
      </View>
    );
  };

  renderRowItem1 = itemData => {
    return (
      <View
        style={{
          width: '95%',
          margin: 10,
          height: 'auto',
          shadowColor: '#000',
          borderRadius: 10,
          backgroundColor: 'white',
          justifyContent: 'center',
          elevation: 5,
        }}>
        <View style={{width: '100%', flexDirection: 'row'}}>
          <Text
            style={{
              fontSize: 15,
              margin: 10,
              color: 'black',
              fontFamily: 'Konnect-Medium',
            }}>
            Doctor Name:
          </Text>
          <Text
            style={{
              fontSize: 15,
              fontFamily: 'Konnect-Medium',
              color: '#bfbfbf',
              marginTop: 10,
            }}>
            {itemData.item.doctor_name}
          </Text>
        </View>

        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              width: '40%',
              flexDirection: 'row',
              backgroundColor: 'white',
            }}>
            <Text
              style={{
                fontSize: 11,
                margin: 10,
                color: 'black',
                fontFamily: 'Konnect-Medium',
              }}>
              Booking Id:
            </Text>
            <Text
              style={{
                fontSize: 11,
                fontFamily: 'Konnect-Medium',
                color: '#bfbfbf',
                marginTop: 10,
              }}>
              {itemData.item.booking_id}
            </Text>
          </View>

          <View
            style={{
              width: '42%',
              flexDirection: 'row',
              backgroundColor: 'white',
            }}>
            <Text
              style={{
                fontSize: 11,
                margin: 10,
                color: 'black',
                fontFamily: 'Konnect-Medium',
              }}>
              Booking Date:
            </Text>
            <Text
              style={{
                fontSize: 11,
                fontFamily: 'Konnect-Medium',
                color: '#bfbfbf',
                marginTop: 10,
              }}>
              {itemData.item.booking_date.substring(0, 8)}
            </Text>
          </View>
        </View>

        <Text
          style={{
            fontSize: 15,
            margin: 10,
            color: 'black',
            fontFamily: 'Konnect-Medium',
          }}>
          Personal Upload's
        </Text>

        {itemData.item.user_uploads.length == 0 && (
          <Text
            style={{
              fontSize: 11,
              margin: 10,
              color: '#800000',
              fontFamily: 'Konnect-Regular',
            }}>
            No Docs uploaded!
          </Text>
        )}
        {itemData.item.user_uploads.length != 0 && (
          <FlatList
            style={{marginTop: 5, marginRight: 10, width: '90%'}}
            data={itemData.item.user_uploads}
            numColumns={3}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderRowItem1s}
            extraData={this.state}
          />
        )}

        <Text
          style={{
            fontSize: 15,
            margin: 10,
            color: 'black',
            fontFamily: 'Konnect-Medium',
          }}>
          Doctor Upload's
        </Text>

        {itemData.item.doctor_uploads.length == 0 && (
          <Text
            style={{
              fontSize: 11,
              margin: 10,
              color: '#800000',
              fontFamily: 'Konnect-Regular',
            }}>
            No Docs uploaded!
          </Text>
        )}

        {itemData.item.doctor_uploads.length != 0 && (
          <FlatList
            style={{marginTop: 5, marginRight: 10, width: '90%'}}
            data={itemData.item.doctor_uploads}
            numColumns={3}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderRowItem1ss}
            extraData={this.state}
          />
        )}
      </View>
    );
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <Header
          navigation={this.props.navigation}
          headerName={'DOCTOR BOOKING'}
        />

        <FlatList
          style={{marginTop: 5}}
          data={this.state.results}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderRowItem1}
          extraData={this.state}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  appBar: {
    backgroundColor: '#800000',
  },
});
