import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  StatusBar,
  Modal,
} from 'react-native';
import Button from 'react-native-button';
const window = Dimensions.get('window');

import Header from './Header.js';
const GLOBAL = require('./Global');
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};

export default class UploadHealthcare extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recognized: '',
      started: '',
      moviesList: [
        {
          back: require('./patients.png'),
          title: 'Doctor Uploads',
          image: require('./patient.png'),
        },
        {
          back: require('./labs.png'),
          title: 'Lab Uploads',
          image: require('./lab.png'),
        },
      ],
    };
  }

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
    };
  };

  selectedFirst = index => {
    if (index == 0) {
      this.props.navigation.navigate('UploadDoctor');
    } else if (index == 1) {
      this.props.navigation.navigate('UploadLab');
    }
  };

  renderRowItem1 = itemData => {
    return (
      <TouchableOpacity onPress={() => this.selectedFirst(itemData.index)}>
        <View
          style={{
            width: window.width - 8,
            margin: 4,
            height: 200,
            borderRadius: 30,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
          }}>
          <Image
            source={itemData.item.image}
            style={{width: window.width - 12, height: '100%', borderRadius: 22}}
          />
          <Image
            source={itemData.item.back}
            style={{
              width: 60,
              height: 60,
              marginTop: -100,
              marginLeft: 10,
              resizeMode: 'contain',
            }}
          />

          <Text
            style={{
              fontSize: 18,
              margin: 10,
              fontFamily: 'Konnect-Regular',
              color: 'white',
            }}>
            {itemData.item.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={{flex: 1}}>
        <Header
          navigation={this.props.navigation}
          headerName={'HEALTHCARE UPLOAD'}
        />
        <FlatList
          style={{marginTop: 5}}
          data={this.state.moviesList}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderRowItem1}
          extraData={this.state}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  appBar: {
    backgroundColor: '#800000',
  },
});
