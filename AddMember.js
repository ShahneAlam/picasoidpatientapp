import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
const GLOBAL = require('./Global');
import Button from 'react-native-button';
const window = Dimensions.get('window');
import Header from './Header.js';
//import { Dropdown } from 'react-native-material-dropdown-v2-fixed';
import {Dropdown} from 'react-native-material-dropdown';
import DatePicker from 'react-native-datepicker';

import {TextField} from 'react-native-material-textfield-plus';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
type Props = {};

let customDatesStyles = [];

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class AddMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recognized: '',
      started: '',
      results: [],
      name: '',
      email: '',
      mobile: '',
      relation: '',
      gender: '',
      dob: '',
      value: '',
      rdata: [],
    };
  }

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
    };
  };

  getIndex = index => {
    this.setState({relation: this.state.rdata[index].label});
    //        alert(this.state.relation)
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  componentDidMount() {
    this.getRelations();
  }

  getRelations() {
    const url = GLOBAL.BASE_URL + 'relationships';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        key: 'relation',
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //      alert(JSON.stringify(responseJson))
        if (responseJson.status == true) {
          //                        console.log('yes')
          var rece = responseJson.relation;
          const transformed = rece.map(({id, name}) => ({
            label: name,
            value: id,
          }));
          console.log(transformed);
          this.setState({rdata: transformed});
          //                        arrayholder = responseJson.list
        } else {
          this.setState({rdata: []});
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  }

  _handlePress() {
    if (this.state.name == '') {
      alert('Please Enter Member Name');
    } else if (this.state.email == '') {
      alert('Please Enter Member EmailId');
    } else if (this.state.mobile == '') {
      alert('Please Enter Mobile Number');
    } else if (this.state.relation == '') {
      alert('Please select relation');
    } else if (this.state.dob == '') {
      alert('Please select Date of Birth');
    } else {
      var type = '';
      if (this.state.value == 0) {
        type = 'Male';
      } else {
        type = 'Female';
      }

      const url = GLOBAL.BASE_URL + 'add_member';

      fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },

        body: JSON.stringify({
          user_id: GLOBAL.user_id,
          member_name: this.state.name,
          member_email: this.state.email,
          member_mobile: this.state.mobile,
          member_relation: this.state.relation,
          member_dob: this.state.dob,
          gender: type,
        }),
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log(JSON.stringify(responseJson));
          if (responseJson.status == true) {
            this.props.navigation.goBack();
            alert('Member Added Successfully!');
          } else {
            this.props.navigation.goBack();
            alert('You can add only 4 members!');
          }
        })
        .catch(error => {
          console.error(error);
          this.hideLoading();
        });
    }
  }

  login = () => {
    this.props.navigation.navigate('NurseTime');
  };

  selectedFirst = indexs => {
    this.props.navigation.navigate('BookingAppointmentDetail');
  };

  _renderItems = ({item, index}) => {
    return (
      <TouchableOpacity onPress={() => this.selectedFirst(index)}>
        {index == 0 && (
          <Image
            style={{width: 60, height: 60, margin: 10, resizeMode: 'contain'}}
            source={require('./myself.png')}
          />
        )}
        {index != 0 && (
          <Image
            style={{width: 60, height: 60, margin: 10, resizeMode: 'contain'}}
            source={require('./add.png')}
          />
        )}

        {index == 0 && (
          <Text
            style={{
              fontSize: 14,
              color: '#800000',
              fontFamily: 'Konnect-Regular',
              textAlign: 'center',
            }}>
            {item.name}
          </Text>
        )}

        {index != 0 && (
          <Text
            style={{
              fontSize: 14,
              color: 'rgba(0,0,0,0.5)',
              fontFamily: 'Konnect-Regular',
              textAlign: 'center',
            }}>
            {item.name}
          </Text>
        )}
      </TouchableOpacity>
    );
  };
  render() {
    var speciality = GLOBAL.speciality;

    var radio_props_one = [
      {label: 'Male', value: 0},
      {label: 'Female', value: 1},
    ];
    let {phone} = this.state;
    let {email} = this.state;
    let {name} = this.state;
    let {mobile} = this.state;
    let {gender} = this.state;
    let {relation} = this.state;
    let {dob} = this.state;
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header navigation={this.props.navigation} headerName={'ADD MEMBER'} />

        <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
          <View
            style={{
              backgroundColor: 'white',
              borderRadius: 8,
              marginLeft: 10,
              width: window.width - 20,
            }}>
            <View style={{marginLeft: 10}}>
              <TextField
                label="Name"
                value={name}
                onChangeText={name => this.setState({name})}
                tintColor={'#800000'}
              />
              <TextField
                label="Email"
                value={email}
                onChangeText={email => this.setState({email})}
                tintColor={'#800000'}
              />

              <TextField
                label="Mobile"
                value={mobile}
                keyboardType={'numeric'}
                maxLength={10}
                onChangeText={mobile => this.setState({mobile})}
                tintColor={'#800000'}
              />

              <Dropdown
                containerStyle={{width: '100%', height: 50, marginTop: -10}}
                fontSize={16}
                fontColor={'#000000'}
                labelFontSize={13}
                dropdownPosition={-4.2}
                onChangeText={(value, index) => this.getIndex(index)}
                label={'Select Relationship'}
                data={this.state.rdata}
              />

              <Text
                style={{
                  fontSize: 12,
                  color: 'rgba(0,0,0,0.5)',
                  fontFamily: 'Konnect-Medium',
                  marginTop: 30,
                }}>
                Gender
              </Text>

              <RadioForm
                style={{marginTop: 12}}
                labelStyle={{paddingRight: 20}}
                radio_props={radio_props_one}
                initial={0}
                buttonSize={10}
                formHorizontal={true}
                buttonColor={'#800000'}
                labelHorizontal={true}
                animation={false}
                labelColor={'black'}
                selectedButtonColor={'#800000'}
                onPress={value => {
                  this.setState({value: value});
                }}
              />

              <Text
                style={{
                  fontSize: 12,
                  color: 'rgba(0,0,0,0.5)',
                  fontFamily: 'Konnect-Medium',
                  marginTop: 20,
                }}>
                Date of Birth
              </Text>

              <DatePicker
                style={{width: 200, marginTop: 10}}
                date={this.state.dob}
                mode="date"
                showIcon={false}
                placeholder={this.state.dob}
                format="YYYY-MM-DD"
                minDate="1950-01-01"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateInput: {
                    marginLeft: -130,
                    borderWidth: 0,
                    color: 'black',
                  },
                }}
                onDateChange={dob => {
                  this.setState({dob: dob});
                }}
              />
            </View>
          </View>

          <Button
            style={{
              padding: 7,
              marginTop: 18,
              fontSize: 20,
              color: 'white',
              backgroundColor: '#800000',
              marginLeft: '5%',
              width: '90%',
              height: 40,
              fontFamily: 'Konnect-Medium',
              borderRadius: 4,
            }}
            styleDisabled={{color: 'red'}}
            onPress={() => this._handlePress()}>
            PROCEED
          </Button>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f1f1f1',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,
    top: window.height / 2,
    opacity: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
