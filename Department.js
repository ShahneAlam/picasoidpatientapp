import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Button from 'react-native-button';
const window = Dimensions.get('window');
const GLOBAL = require('./Global');
import Header from './Header.js';
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import store from 'react-native-simple-store';
export default class Department extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recognized: '',
      started: '',
      results: [],
      department: [],
      images: [],
    };
  }

  componentWillUnmount() {}

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
      // title: 'SELECT',
      // headerTitleStyle :{textAlign: 'center',alignSelf:'center',color :'black'},
      // headerStyle:{
      //     backgroundColor:'white',
      // },
      // headerTintColor :'#800000',
      // animations: {
      //     setRoot: {
      //         waitForRender: false
      //     }
      // }
    };
  };

  hideLoading() {
    this.setState({loading: false});
  }

  showLoading() {
    this.setState({loading: true});
  }
  fetchDepartment(res) {
    if (res == null || res.length == 0) {
      const url = GLOBAL.BASE_URL + 'departments';

      fetch(url, {
        method: 'get',
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == true) {
            this.setState({images: responseJson.depart});

            var array = [];
            for (var i = 0; i < this.state.images.length; i++) {
              var dict = {
                name: this.state.images[i].name,
                selected: '',
                id: this.state.images[i].id,
              };
              array.push(dict);
            }

            this.setState({results: array});
          }
        })
        .catch(error => {
          console.error(error);
          this.hideLoading();
        });
    } else {
      this.setState({results: res[0].array});
    }
  }

  componentDidMount() {
    store.get('departments').then(res =>
      //alert(JSON.stringify(res))
      this.fetchDepartment(res),
    );
  }

  login = () => {
    store.delete('departments');
    var dict = {
      array: this.state.results,
    };

    store.push('departments', dict);
    //GLOBAL.department = this.state.results
    GLOBAL.depart = this.state.results;
    //   alert(JSON.stringify(GLOBAL.depart))
    //  return
    GLOBAL.did = '';
    for (let i = 0; i < this.state.results.length; i++) {
      if (this.state.results[i].selected == 'Y') {
        GLOBAL.did = GLOBAL.did + this.state.results[i].id + ',';
      }
    }
    // alert(GLOBAL.did)
    this.props.navigation.goBack();
  };

  check = () => {
    this.setState({isSecure: !this.state.isSecure});
  };

  selectedFirst = indexs => {
    let movie = this.state.results;

    let selectedIndex = movie[indexs];

    if (selectedIndex.selected == '') {
      selectedIndex.selected = 'Y';
    } else {
      selectedIndex.selected = '';
    }
    this.state.results[indexs] = selectedIndex;

    this.setState({results: this.state.results});

    //           GLOBAL.depart = indexs
    // //        alert(JSON.stringify(GLOBAL.depart))

    //         this.props.navigation.goBack()
  };

  _renderItems = ({item, index}) => {
    return (
      <TouchableOpacity onPress={() => this.selectedFirst(index)}>
        <View style={{flexDirection: 'row', width: '95%', alignSelf: 'center'}}>
          <Text
            style={{
              width: '90%',
              marginTop: 10,
              marginBottom: 8,
              color: 'black',
              fontFamily: 'Konnect-Regular',
            }}>
            {item.name}
          </Text>

          {item.selected != '' && (
            <Image
              style={{
                width: 22,
                height: 22,
                resizeMode: 'contain',
                marginTop: 10,
              }}
              source={require('./check.png')}
            />
          )}

          {item.selected == '' && (
            <Image
              style={{
                width: 22,
                height: 22,
                resizeMode: 'contain',
                marginTop: 10,
              }}
              source={require('./uncheck.png')}
            />
          )}
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    let {phone} = this.state;
    let {email} = this.state;
    let {name} = this.state;
    let {company} = this.state;
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header
          navigation={this.props.navigation}
          headerName={'SELECT DEPARTMENT'}
        />

        <FlatList
          style={{flexGrow: 0, height: window.height - 150}}
          data={this.state.results}
          numColumns={1}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItems}
          extraData={this.state}
        />

        <Button
          style={{
            padding: 8,
            marginTop: 14,
            fontSize: 20,
            color: 'white',
            backgroundColor: '#800000',
            marginLeft: '5%',
            width: '90%',
            height: 40,
            fontFamily: 'Konnect-Medium',
            borderRadius: 4,
          }}
          styleDisabled={{color: 'red'}}
          onPress={() => this.login()}>
          SAVE
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    backgroundColor: '#f1f1f1',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
});
