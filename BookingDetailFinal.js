import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
const window = Dimensions.get('window');
import CalendarStrip from 'react-native-calendar-strip';
import Button from 'react-native-button';
import Header from './Header.js';
const GLOBAL = require('./Global');
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};
import moment from 'moment';

let customDatesStyles = [];

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class BookingDetailFinal extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      phone: '',
      company: '',
      loading: false,
      visible: false,
      time: [],
      selected: false,
      disabled: true,
      data: [],
    };
  }

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
    };
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  dates = date => {
    var t = new Date(date);
    var s = moment(t).format('YYYY-MM-DD');
    GLOBAL.date = s;
    //        alert(GLOBAL.date)
    this.calculateDay(s);
    this.setState({disabled: true});
  };
  componentDidMount() {
    let startDate = moment();
    for (let i = 0; i < 700; i++) {
      customDatesStyles.push({
        startDate: startDate.clone().add(i, 'days'), // Single date since no endDate provided
        dateNameStyle: styles.dateNameStyle,
        dateNumberStyle: styles.dateNumberStyle,

        // Random color...
        dateContainerStyle: {
          shadowOpacity: 1.0,
          shadowRadius: 1,
          shadowColor: 'black',
          shadowOffset: {textAlign: 'left', height: 0, width: 0},
          margin: 5,
          width: 40,
          borderRadius: 0,
          backgroundColor: 'white',
        },
      });
    }
    var date = new Date();
    var s = moment(date).format('YYYY-MM-DD');

    this.calculateDay(s);
    GLOBAL.date = moment(date).format('YYYY-MM-DD');
  }

  login = () => {
    if (GLOBAL.date == '' || GLOBAL.time == '') {
      alert('Please select date and time');
      return;
    }

    GLOBAL.hbooking_time = GLOBAL.time;
    GLOBAL.hbooking_date = GLOBAL.date;
    //        GLOBAL.hDoctor_id = GLOBAL.

    if (GLOBAL.appointmentArray.can_book_doctor_free != 0) {
      this.props.navigation.navigate('AskConfirmation');

      // const url = GLOBAL.BASE_URL + 'add_permanent_booking'

      // fetch(url, {
      //     method: 'POST',
      //     headers: {
      //         'Content-Type': 'application/json',
      //     },

      //     body: JSON.stringify({

      //         "user_id": GLOBAL.user_id,
      //         "for": "4",
      //         "module": GLOBAL.onlinetype,
      //         "doctor_id": GLOBAL.appointmentArray.id,
      //         "booking_for": "self",
      //         "member_id": '',
      //         "booking_time": GLOBAL.time,
      //         "booking_date": GLOBAL.date,
      //         "name": GLOBAL.onlinename,
      //         "gender": GLOBAL.onlinegender,
      //         "dob": GLOBAL.onlinedob,
      //         "address": GLOBAL.onlineaddress,
      //         "area": GLOBAL.onlinearea,
      //         "pincode": GLOBAL.onlinecity,
      //         "city_state": GLOBAL.onlinecity,
      //         "problem":GLOBAL.hproblem,
      //         "coupan_code": 0,
      //         "coupan_code_id": 0,
      //         "total_amount": 0,
      //         "discount_amount": 0,
      //         "images": GLOBAL.listofimages,
      //         "wallet_amount": '',
      //         "referral_amount": '',
      //         "is_package":"1"

      //     }),
      // }).then((response) => response.json())
      //     .then((responseJson) => {

      //         // alert(JSON.stringify(responseJson))

      //         //  this.rajorPay()
      //         if (responseJson.status == true) {

      //             if(GLOBAL.myonlinetype == 'offline'){
      //             GLOBAL.finalType=5
      //             this.props.navigation.navigate('Ccavenue')

      //             }else if(GLOBAL.myonlinetype =='online'){
      //             GLOBAL.finalType=4
      //             this.props.navigation.navigate('Ccavenue')

      //             }

      //         } else {

      //         }
      //     })
      //     .catch((error) => {
      //         console.error(error);
      //         this.hideLoading()
      //     });
    } else {
      this.props.navigation.navigate('AskConfirmation');
    }
  };

  selectedFirst = indexs => {
    var a = this.state.time;
    for (var i = 0; i < this.state.time.length; i++) {
      this.state.time[i].is_selected = '';
    }
    var index = a[indexs];
    if (index.is_selected == '') {
      index.is_selected = 'Y';
      GLOBAL.time = index.time;
    } else {
      index.is_selected = '';
    }
    this.state.time[indexs] = index;
    this.setState({time: this.state.time});
    this.setState({disabled: false});
  };

  _renderItems = ({item, index}) => {
    return (
      <TouchableOpacity
        style={{marginBottom: 10}}
        onPress={() => this.selectedFirst(index)}
        activeOpacity={0.99}>
        {item.is_selected == '' && (
          <View
            style={{
              height: 60,
              shadowOpacity: 1.0,
              justifyContent: 'center',
              alignItems: 'center',
              elevation: 5,
              shadowRadius: 1,
              shadowColor: 'black',
              shadowOffset: {textAlign: 'left', height: 0, width: 0},
              margin: 5,
              borderRadius: 4,
              backgroundColor: 'white',
            }}>
            <Text
              style={{
                color: '#707070',
                fontSize: 16,
                padding: 8,
                fontFamily: 'Konnect-Medium',
              }}>
              {item.time}
            </Text>
          </View>
        )}

        {item.is_selected != '' && (
          <View
            style={{
              height: 60,
              shadowOpacity: 1.0,
              justifyContent: 'center',
              alignItems: 'center',
              elevation: 5,
              shadowRadius: 1,
              shadowColor: 'black',
              shadowOffset: {textAlign: 'left', height: 0, width: 0},
              margin: 5,
              borderRadius: 4,
              backgroundColor: '#800000',
            }}>
            <Text
              style={{
                color: 'white',
                fontSize: 16,
                padding: 8,
                fontFamily: 'Konnect-Medium',
              }}>
              {item.time}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
  };

  componentWillUnmount() {}

  calculateDay(date) {
    var dec_onlineType;

    if (GLOBAL.onlinetypes == 'free') {
      dec_onlineType = GLOBAL.onlinetypes;
    } else {
      dec_onlineType = GLOBAL.onlinetype;
    }

    console.log(dec_onlineType + '-----ds');
    const url = GLOBAL.BASE_URL + 'common_time_slots_comm';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        user_id: GLOBAL.user_id,
        select_date: date,
        for_time: dec_onlineType,
        id: GLOBAL.appointmentArray.id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status == true) {
          this.setState({time: responseJson.slot});
        } else {
          this.setState({time: []});
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  }

  showLoading() {
    this.setState({loading: true});
  }

  render() {
    var speciality = GLOBAL.appointmentArray.speciality_detail_array;
    let {phone} = this.state;
    let {email} = this.state;
    let {name} = this.state;
    let {company} = this.state;
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header navigation={this.props.navigation} headerName={'BOOKING'} />

        <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
          <View
            style={{
              flex: 1,
              marginLeft: 5,
              width: window.width - 10,
              backgroundColor: 'white',
              marginTop: 10,
              marginBottom: 10,
              borderRadius: 10,
            }}>
            <View style={{flexDirection: 'row', width: '100%'}}>
              <View>
                <Image
                  style={{width: 60, height: 60, borderRadius: 30, margin: 10}}
                  source={{uri: GLOBAL.appointmentArray.image}}
                />

                <View
                  style={{
                    backgroundColor: '#800000',
                    borderRadius: 4,
                    width: 40,
                    height: 20,
                    marginTop: 2,
                    flexDirection: 'row',
                    justifyItems: 'center',
                    alignItems: 'center',
                    alignSelf: 'center',
                  }}>
                  <Image
                    style={{
                      width: 8,
                      height: 8,
                      marginLeft: 4,
                      resizeMode: 'contain',
                    }}
                    source={require('./star.png')}
                  />

                  <Text
                    style={{
                      marginLeft: 5,
                      fontSize: 8,
                      marginTop: 3,
                      color: 'white',
                      fontFamily: 'Konnect-Medium',
                    }}>
                    {GLOBAL.appointmentArray.ratting}
                  </Text>
                </View>
                {GLOBAL.appointmentArray.doctor_avail_status == 1 && (
                  <Text
                    style={{
                      fontSize: 9,
                      color: '#3DBA56',
                      fontFamily: 'Konnect-Medium',
                      width: 50,
                      textAlign: 'center',
                      alignSelf: 'center',
                    }}>
                    Online
                  </Text>
                )}
                {GLOBAL.appointmentArray.doctor_avail_status != 1 && (
                  <Text
                    style={{
                      fontSize: 9,
                      color: 'red',
                      fontFamily: 'Konnect-Medium',
                      width: 50,
                      textAlign: 'center',
                      alignSelf: 'center',
                    }}>
                    Offline
                  </Text>
                )}
              </View>

              <View>
                <View style={{flexDirection: 'row', width: '100%'}}>
                  <Text
                    style={{
                      marginLeft: 5,
                      fontSize: 18,
                      color: '#3A3A3A',
                      fontFamily: 'Konnect-Medium',
                      width: '70%',
                      marginTop: 10,
                    }}>
                    {GLOBAL.appointmentArray.name}
                  </Text>
                </View>

                <View style={{flexDirection: 'row'}}>
                  <Text
                    style={{
                      marginLeft: 5,
                      fontSize: 12,
                      color: '#8F8F8F',
                      height: 'auto',
                      fontFamily: 'Konnect-Medium',
                      width: '80%',
                    }}>
                    {speciality}
                  </Text>
                </View>

                <View style={{flexDirection: 'row'}}>
                  <Image
                    style={{width: 20, height: 20, resizeMode: 'contain'}}
                    source={require('./location.png')}
                  />

                  <Text
                    style={{
                      marginLeft: 5,
                      width: window.width - 150,
                      height: 30,
                      fontSize: 12,
                      color: '#8F8F8F',
                      fontFamily: 'Konnect-Medium',
                    }}>
                    Hospital/Clinic: {GLOBAL.appointmentArray.lat_long_address}
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 10,
                  }}>
                  <View>
                    <Text
                      style={{
                        fontSize: 12,
                        color: '#AAAAAA',
                        fontFamily: 'Konnect-Medium',
                      }}>
                      Experience
                    </Text>
                    <Text
                      style={{
                        fontSize: 16,
                        color: '#3A3A3A',
                        fontFamily: 'Konnect-Medium',
                        textAlign: 'center',
                      }}>
                      {GLOBAL.appointmentArray.experience} Years
                    </Text>
                  </View>

                  <View>
                    <Text
                      style={{
                        fontSize: 12,
                        color: '#AAAAAA',
                        fontFamily: 'Konnect-Medium',
                      }}>
                      Likes
                    </Text>
                    <Text
                      style={{
                        fontSize: 16,
                        color: '#3A3A3A',
                        fontFamily: 'Konnect-Medium',
                        textAlign: 'center',
                      }}>
                      {GLOBAL.appointmentArray.like}
                    </Text>
                  </View>

                  <View style={{marginRight: 50}}>
                    <Text
                      style={{
                        fontSize: 12,
                        color: '#AAAAAA',
                        fontFamily: 'Konnect-Medium',
                      }}>
                      Reviews
                    </Text>
                    <Text
                      style={{
                        fontSize: 16,
                        color: '#3A3A3A',
                        fontFamily: 'Konnect-Medium',
                        textAlign: 'center',
                      }}>
                      {GLOBAL.appointmentArray.total_review}
                    </Text>
                  </View>
                </View>

                {/*                                <Text style={{fontSize : 12,color :'#800000',fontFamily:'Konnect-Medium',marginBottom:10}}>

                                    Consult online for ₹ {GLOBAL.appointmentArray.online_consult_chat_price}/- onwards
                                </Text>
*/}
              </View>
            </View>
          </View>

          <Text
            style={{
              marginLeft: 5,
              marginTop: 10,
              fontSize: 18,
              color: '#707070',
              height: 'auto',
              fontFamily: 'Konnect-Regular',
              width: window.width - 80,
            }}>
            Select Date
          </Text>

          <CalendarStrip
            calendarAnimation={{
              type: 'sequence',
              duration: 30,
            }}
            daySelectionAnimation={{
              color: 'black',
              type: 'background',
              duration: 300,
              highlightColor: '#800000',
            }}
            style={{height: 120, paddingTop: 15}}
            calendarHeaderStyle={{
              borderColor: 'black',
            }}
            calendarColor={'white'}
            //  highlightDateNameStyle={{color: 'white'}}
            // highlightDateNumberStyle={{color: 'white'}}
            iconContainer={{flex: 0.1}}
            onDateSelected={date => this.dates(date)}
          />

          <Text
            style={{
              marginLeft: 5,
              marginTop: 10,
              fontSize: 18,
              color: '#707070',
              height: 'auto',
              fontFamily: 'Konnect-Regular',
              width: window.width - 80,
            }}>
            Select Time Slot
          </Text>

          {this.state.time.length == 0 && (
            <Text
              style={{
                fontSize: 13,
                marginTop: 15,
                color: 'black',
                fontFamily: 'Konnect-Medium',
                alignSelf: 'center',
                textAlign: 'center',
              }}>
              No Time slots!
            </Text>
          )}

          {this.state.time.length != 0 && (
            <FlatList
              style={{flexGrow: 0, margin: 8}}
              data={this.state.time}
              numColumns={1}
              horizontal={true}
              keyExtractor={(item, index) => index.toString()}
              renderItem={this._renderItems}
            />
          )}

          <Button
            style={{
              padding: 7,
              marginTop: '20%',
              fontSize: 20,
              color: 'white',
              backgroundColor: '#800000',
              marginLeft: '5%',
              width: '90%',
              height: 40,
              fontFamily: 'Konnect-Medium',
              borderRadius: 4,
            }}
            styleDisabled={{color: 'black', backgroundColor: 'grey'}}
            disabled={this.state.disabled}
            onPress={() => this.login()}>
            PROCEED
          </Button>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f1f1f1',
    height: window.height,
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,
    top: window.height / 2,
    opacity: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
