import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
const GLOBAL = require('./Global');
import {NavigationActions, StackActions, DrawerActions} from 'react-navigation';
import Button from 'react-native-button';
const window = Dimensions.get('window');
import Header from './Header.js';
import {TextField} from 'react-native-material-textfield-plus';
import ImagePicker from 'react-native-image-picker';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
type Props = {};
const options = {
  title: 'Select Image',
  maxWidth: 300,
  maxHeight: 500,
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
let customDatesStyles = [];

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class Pharmacy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recognized: '',
      started: '',
      results: [],
      name: '',
      myimages: [],
      value: '',
      dob: '',
      address: [],
      area: '',
      city: '',
      phone: '',
      path: '',
      avatarSource: '',
      member: [],
      images: [
        {
          name: 'Myself',
          selected: '',
          myself: 'Y',
        },
        {
          name: 'Someone else',
          selected: '',
          myself: 'N',
        },
      ],
    };
  }

  componentWillUnmount() {}

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
    };
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }
  getSelection = index => {
    for (let i = 0; i < 2; i++) {
      this.state.moviesList[i].selected = '';
    }

    this.setState({moviesList: this.state.moviesList});

    let indexs = this.state.moviesList;
    let targetPost = this.state.moviesList[index];
    if (targetPost.selected == '') {
      targetPost.selected = 'Y';
    } else {
      targetPost.selected = '';
    }
    indexs[index] = targetPost;
    this.setState({moviesList: indexs});
  };

  showLoading() {
    this.setState({loading: true});
  }
  _handleStateChange = state => {
    this.setState({address: GLOBAL.selectedAddress});

    //   const interests = [...interest, ...a];
    //
    // var b = interest.concat(a)
    //
  };

  componentDidMount() {
    this.props.navigation.addListener('willFocus', this._handleStateChange);
    const url = GLOBAL.BASE_URL + 'list_upload_images_lab';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        user_id: GLOBAL.user_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //  this.rajorPay()
        if (responseJson.status == true) {
          this.setState({myimages: responseJson.list});
          //  alert(JSON.stringify(responseJson.list))
          this.setState({path: responseJson.path});
        } else {
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  }
  _handlePress() {
    if (this.state.phone == '') {
      alert('Please enter problem');
    } else {
      var imgid = '';

      for (var i = 0; i < this.state.myimages.length; i++) {
        imgid = imgid + this.state.myimages[i].image + '|';
      }
      if (imgid == '') {
      } else {
        imgid = imgid.slice(0, -1);
      }

      this.setState({loading: true});
      const url = GLOBAL.BASE_URL + 'add_lab_test';

      fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },

        body: JSON.stringify({
          user_id: GLOBAL.user_id,
          lab_id: GLOBAL.labid,
          content: this.state.phone,
          imagess: imgid,
        }),
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({loading: false});
          if (responseJson.status == true) {
            this.props.navigation.dispatch(
              StackActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({
                    routeName: 'DrawerNavigator',
                    params: {someParams: 'parameters goes here...'},
                  }),
                ],
              }),
            );
            this.props.navigation.navigate('LabHistory');

            alert('Your Lab Test Booking is successful.');
          } else {
            alert('Your Already have one Booking');
          }
        })
        .catch(error => {
          console.error(error);
          this.setState({loading: false});
        });
    }
  }

  getIndex = index => {
    this.setState({email: this.state.data[index].id});
  };
  _handlePressd = () => {
    if (this.state.myimages.length >= 6) {
      alert('Already added 6 images');
      return;
    }
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};

        const url = GLOBAL.BASE_URL + 'image_attchment_upload_lab';
        const data = new FormData();
        data.append('user_id', GLOBAL.user_id);
        data.append('flag', 1);

        // you can append anyone.
        data.append('image', {
          uri: response.uri,
          type: 'image/jpeg', // or photo.type
          name: 'image.png',
        });
        fetch(url, {
          method: 'post',
          body: data,
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        })
          .then(response => response.json())
          .then(responseJson => {
            //       this.hideLoading()
            this.setState({myimages: responseJson.images});

            this.setState({path: responseJson.path});
          });
      }
    });
  };

  selectedFirstd = item => {
    const url = GLOBAL.BASE_URL + 'delete_images_lab';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: GLOBAL.user_id,
        id: item.id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //                alert(JSON.stringify(responseJson))

        //  this.rajorPay()
        if (responseJson.status == true) {
          this.setState({myimages: responseJson.list_of_images});
        } else {
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  };
  _renderItemsd = ({item, index}) => {
    var uri = `${this.state.path}${item.image}`;

    return (
      <View style={{backgroundColor: 'transparent', margin: 1}}>
        <Image
          style={{width: 120, height: 110, margin: 5}}
          source={{uri: uri}}
        />
        <TouchableOpacity
          style={{position: 'absolute', right: 0}}
          onPress={() => this.selectedFirstd(item)}>
          <Image
            style={{
              width: 20,
              height: 20,
              resizeMode: 'contain',
              transform: [{rotate: '45deg'}],
            }}
            source={require('./add.png')}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    var radio_props_one = [
      {label: 'Male', value: 0},
      {label: 'Female', value: 1},
    ];
    let {phone} = this.state;
    let {dob} = this.state;
    let {address} = this.state;
    let {area} = this.state;
    let {city} = this.state;
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header
          navigation={this.props.navigation}
          headerName={'LAB TEST BOOKING'}
        />

        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
          <View
            style={{
              margin: 10,
              width: window.width - 20,
              backgroundColor: 'white',
              borderRadius: 5,
              borderBottomColor: '#bfbfbf',
              borderBottomWidth: 1,
            }}>
            <TextField
              label="ADD PROBLEM"
              value={phone}
              lineWidth={0}
              activeLineWidth={0}
              fontSize={15}
              multiline={true}
              baseColor={'#000000'}
              onChangeText={phone => this.setState({phone})}
              tintColor={'#800000'}
            />
          </View>

          <View>
            <Text
              style={{
                fontSize: 18,
                color: '#132439',
                fontFamily: 'Konnect-Regular',
                margin: 10,
              }}>
              Add Your Prescription
            </Text>
            {this.state.myimages.length != 0 && (
              <FlatList
                style={{
                  flexGrow: 0,
                  backgroundColor: 'transparent',
                  marginLeft: 10,
                }}
                horizontal={true}
                data={this.state.myimages}
                numColumns={1}
                horizontal={true}
                keyExtractor={(item, index) => index.toString()}
                renderItem={this._renderItemsd}
              />
            )}

            <Button
              style={{
                padding: 9,
                marginTop: 25,
                fontSize: 18,
                borderWidth: 1,
                borderColor: '#800000',
                color: '#800000',
                marginLeft: '3%',
                width: '93%',
                height: 40,
                fontFamily: 'Konnect-Medium',
                borderRadius: 4,
              }}
              styleDisabled={{color: 'red'}}
              onPress={() => this._handlePressd()}>
              ATTACH IMAGES /REPORT
            </Button>
            <Text
              style={{
                fontSize: 18,
                color: '#132439',
                fontFamily: 'Konnect-Regular',
                marginTop: 30,
                marginLeft: 10,
              }}>
              Attach upto 6 images here.
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => this._handlePress()}
            style={{marginTop: '20%'}}>
            <View
              style={{
                backgroundColor: '#800000',
                height: 55,
                borderRadius: 27.5,
                alignSelf: 'center',
                width: 300,
                borderBottomWidth: 0,
                shadowColor: 'black',
                shadowOffset: {width: 0, height: 2},
                shadowOpacity: 0.8,
                shadowRadius: 2,
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  width: '100%',
                  alignSelf: 'center',
                  textAlign: 'center',
                  fontSize: 20,
                  fontFamily: 'Konnect-Medium',
                  color: 'white',
                  padding: 11,
                }}>
                SUBMIT
              </Text>

              <Image
                style={{
                  width: 25,
                  height: 25,
                  resizeMode: 'contain',
                  marginLeft: -50,
                  alignSelf: 'center',
                }}
                source={require('./right.png')}
              />
            </View>
          </TouchableOpacity>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    flex: 1,
    backgroundColor: '#f1f1f1',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
});
