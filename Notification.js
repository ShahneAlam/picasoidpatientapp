import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
  StatusBar,
  Image,
  TouchableOpacity,
  Alert,
  Container,
  Linking,
  TextInput,
  Dimensions,
} from 'react-native';
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Button from 'react-native-button';
import Header from './Header.js';
const window = Dimensions.get('window');
const GLOBAL = require('./Global');
import moment from 'moment';

type Props = {};
class Notification extends Component<Props> {
  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
    };
  };

  constructor(props) {
    super(props);
    const {navigation} = this.props;
    this.state = {
      name: '',
      email: '',
      message: '',
      status: '',
      loading: '',
      userid: '',
      notificationslist: [],
      limitStart: 0,
    };
  }
  _keyExtractor = (item, index) => item.productID;

  renderRowItem = itemData => {
    var dt = moment(itemData.item.added_on).format('DD-MM-YY, hh:mm A');
    return (
      <View
        style={{
          flexDirection: 'row',
          flex: 1,
          backgroundColor: 'white',
          borderRadius: 5,
          width: window.width - 20,
          marginLeft: 10,
          marginRight: 10,
          marginTop: 10,
          marginBottom: 10,
          elevation: 5,
        }}>
        <Image
          style={{width: 30, height: 30, resizeMode: 'contain', margin: 12}}
          source={require('./notification.png')}
        />
        <View style={{flexDirection: 'column', margin: 10, width: '82%'}}>
          <Text
            style={{
              fontSize: 15,
              color: '#800000',
              fontFamily: 'Konnect-Regular',
            }}>
            {itemData.item.title}
          </Text>
          <Text
            style={{
              fontSize: 13,
              marginRight: 10,
              fontFamily: 'Konnect-Regular',
            }}>
            {itemData.item.notification}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
            }}>
            <Image
              style={{width: 18, height: 18, resizeMode: 'contain'}}
              source={require('./clocks.png')}
            />
            <Text
              style={{
                fontSize: 13,
                marginTop: 10,
                marginLeft: 10,
                marginRight: 10,
                color: '#7E7E7E',
              }}>
              {dt}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  loadMoreData = () => {
    const url = GLOBAL.BASE_URL + 'user_notification';
    //      this.showLoading()
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: GLOBAL.user_id,
        limit_from: this.state.limitStart + 25,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //   console.log(JSON.stringify(responseJson))
        //  this.hideLoading()
        if (responseJson.status == true) {
          this.setState({notificationslist: responseJson.list});
        }
      })
      .catch(error => {
        console.error(error);
        //this.hideLoading()
      });
  };

  componentDidMount() {
    this.getReviews();
  }

  getReviews = () => {
    //      this.showLoading();
    const url = GLOBAL.BASE_URL + 'user_notification';
    //      this.showLoading()
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: GLOBAL.user_id,
        limit_from: this.state.limitStart,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        // console.log(JSON.stringify(responseJson))
        //  this.hideLoading()
        if (responseJson.status == true) {
          this.setState({notificationslist: responseJson.list});
        }
      })
      .catch(error => {
        console.error(error);
        //  this.hideLoading()
      });
  };

  render() {
    if (this.state.loading) {
      return (
        <View style={{flex: 1}}>
          <ActivityIndicator style={styles.loading} size={50} color="#E9128B" />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header
          navigation={this.props.navigation}
          headerName={'NOTIFICATIONS'}
        />

        <KeyboardAwareScrollView
          style={styles.container2}
          keyboardShouldPersistTaps="always">
          {this.state.notificationslist.length == 0 && (
            <Text
              style={{
                fontSize: 20,
                margin: 10,
                alignSelf: 'center',
                fontFamily: 'Konnect-Regular',
              }}>
              No new notifications!
            </Text>
          )}

          {this.state.notificationslist.length != 0 && (
            <FlatList
              style={{
                backgroundColor: '#f2f2f2',
                flexGrow: 0,
                marginBottom: 10,
              }}
              data={this.state.notificationslist}
              numColumns={1}
              keyExtractor={(item, index) => index.toString()}
              renderItem={this.renderRowItem}
              onEndReached={() => this.loadMoreData()}
              extraData={this.state}
            />
          )}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#f2f2f2',
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Notification;
