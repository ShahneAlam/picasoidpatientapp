import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  StatusBar,
  Modal,
} from 'react-native';
import Button from 'react-native-button';
const window = Dimensions.get('window');
const GLOBAL = require('./Global');
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

export default class PopupPay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recognized: '',
      started: '',
      modalVisible: this.props.setMode,
    };
    console.log(this.props.setMode);
  }

  componentWillReceiveProps() {
    this.setState({modalVisible: this.props.setMode});
  }

  componentDidMount() {
    this.setState({modalVisible: this.props.setMode});
  }
  setModalVisible = (visible, get) => {
    // if (typeof get !== 'undefined') {
    //            this.setState({text:get.state_name})

    //            // alert(JSON.stringify(get))
    // }
    let asa = !this.state.modalVisible;
    console.log('values' + asa);

    this.setState({modalVisible: asa});
    //        this.prop.setMode=visible
  };

  render() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          //             Alert.alert('Modal has been closed.');
          this.setModalVisible(!this.props.setMode);
        }}>
        <TouchableOpacity
          style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            alignItems: 'center',
          }}
          activeOpacity={1}
          onPressOut={() => {
            this.setModalVisible(false);
          }}>
          <View
            style={{
              width: 300,
              backgroundColor: 'white',
              height: 240,
              borderRadius: 8,
            }}>
            <View
              style={{
                width: '100%',
                backgroundColor: 'white',
                borderRadius: 8,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  backgroundColor: '#800000',
                  height: 60,
                  borderTopLeftRadius: 8,
                  borderTopRightRadius: 8,
                  borderTopLeftWidth: 1,
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  borderTopRightWidth: 1,
                  borderTopRightColor: 'transparent',
                  borderTopLeftColor: 'transparent',
                }}>
                <Text
                  style={{
                    fontSize: 17,
                    fontFamily: 'Konnect-Medium',
                    color: 'white',
                    margin: 10,
                  }}>
                  Activate Interstate Account
                </Text>
                <TouchableOpacity onPress={() => this.setModalVisible(false)}>
                  <Image
                    style={{
                      width: 25,
                      height: 25,
                      resizeMode: 'contain',
                      marginRight: 10,
                    }}
                    source={require('./cross.png')}
                  />
                </TouchableOpacity>
              </View>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: 'Konnect-Medium',
                  color: 'black',
                  marginTop: 15,
                  marginLeft: 15,
                  width: '90%',
                  textAlign: 'left',
                }}>
                By activating interstate account you will be able to place
                appointment booking for other states also by paying a one time
                amount of ₹ {GLOBAL.hInterAmount}/- only.
              </Text>

              <Button
                style={{
                  fontSize: 16,
                  fontFamily: 'Konnect-Medium',
                  color: 'white',
                  alignSelf: 'center',
                  padding: 7,
                }}
                containerStyle={{
                  height: 35,
                  width: '40%',
                  borderRadius: 7,
                  alignSelf: 'center',
                  alignItems: 'center',
                  backgroundColor: '#800000',
                  marginTop: '4%',
                }}
                onPress={() => {
                  GLOBAL.finalType = 1;
                  this.props.navigation.navigate('Ccavenue');
                  this.setModalVisible(false);
                }}>
                Activate
              </Button>
            </View>
          </View>
        </TouchableOpacity>
      </Modal>
    );
  }
}
const styles = StyleSheet.create({
  appBar: {
    backgroundColor: '#800000',
    height: APPBAR_HEIGHT,
  },
});
