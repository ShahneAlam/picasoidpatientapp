import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  StatusBar,
  Modal,
  PermissionsAndroid,
  ToastAndroid,
  Linking,
  Share,
} from 'react-native';
import Button from 'react-native-button';
const window = Dimensions.get('window');
import RNFetchBlob from 'rn-fetch-blob';
var randomString = require('random-string');

import Header from './Header.js';
const GLOBAL = require('./Global');
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};
var path_url = '';
var rpath = '';

export default class UploadPersonal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recognized: '',
      results: [],
      resultspr: [],
      resultslab: [],
      resultsothers: [],
      started: '',
      tone: 0,
      ttwo: 0,
      tthree: 0,
      tfour: 0,
      modalVisible: false,
    };
  }

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
    };
  };
  setModalVisible = visible => {
    //    alert(JSON.stringify(item))
    // if(item!=undefined){
    //     GLOBAL.modelImage=fullPath
    //     this.setState({modalVisible: visible});

    // }
    this.setState({modalVisible: visible});
  };

  deleteImage = (imageId, reportType) => {
    console.log(imageId, reportType);
    const url = GLOBAL.BASE_URL + 'delete_uploadfiles_by_user';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        user_id: GLOBAL.user_id,
        id: imageId,
        type: reportType,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status == true) {
          alert('Report deleted successfully!');

          this.setModalVisible(false);

          this.getData('bills');
          this.getData('prescription');
          this.getData('labreport');
          this.getData('others');

          // if(GLOBAL.reportType == 'bills'){
          //     this.getData(GLOBAL.reportType)

          // }else if(GLOBAL.reportType == 'prescription'){
          //     this.getData(GLOBAL.reportType)

          // }else if(GLOBAL.reportType == 'labreport'){
          //     this.getData(GLOBAL.reportType)

          // }else if(GLOBAL.reportType == 'others'){
          //     this.getData(GLOBAL.reportType)

          // }
        } else {
          this.setState({results: []});
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  };

  actualDownload = item => {
    //  alert(JSON.stringify(item))
    var docname = 'picasoid_doc';
    var rs = randomString({
      length: 3,
      numeric: true,
      letters: false,
      special: false,
    });

    rpath = docname + rs;
    var totalPath = '/' + rpath + '.png';
    //alert(totalPath)
    var url = item;
    this.setState({
      progress: 0,
      loading: true,
    });
    let dirs = RNFetchBlob.fs.dirs;
    RNFetchBlob.config({
      // add this option that makes response data to be stored as a file,
      // this is much more performant.
      path: dirs.DownloadDir + totalPath,
      fileCache: true,
    })
      .fetch('GET', url, {
        //some headers ..
      })
      .progress((received, total) => {
        console.log('progress', received / total);
        this.setState({progress: received / total});
      })
      .then(res => {
        this.setState({
          progress: 100,
          loading: false,
        });
        ToastAndroid.showWithGravity(
          'Your file has been downloaded to downloads folder!',
          ToastAndroid.SHORT,
          ToastAndroid.BOTTOM,
        );
      });
  };

  async downloadFile(item) {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Storage Permission',
          message: 'App needs access to memory to download the file ',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.actualDownload(item);
      } else {
        Alert.alert(
          'Permission Denied!',
          'You need to give storage permission to download the file',
        );
      }
    } catch (err) {
      console.warn(err);
    }
  }

  componentDidMount() {
    //       this.getData('prescription')
  }

  getData = reportType => {
    // "type":bills OR labreport OR others OR prescription,
    GLOBAL.reportType = reportType;
    const url = GLOBAL.BASE_URL + 'list_uploadimages_by_user';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        user_id: GLOBAL.user_id,
        limit_from: '0',
        type: reportType,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //   alert(JSON.stringify(responseJson))

        if (responseJson.status == true) {
          //                    this.setState({tone: 1})
          if (reportType == 'bills') {
            this.setState({results: responseJson.list});
            this.setState({path_url: responseJson.path});
          } else if (reportType == 'prescription') {
            this.setState({resultspr: responseJson.list});
            this.setState({path_urlspr: responseJson.path});
          } else if (reportType == 'labreport') {
            this.setState({resultslab: responseJson.list});
            this.setState({path_urlslab: responseJson.path});
          } else if (reportType == 'others') {
            this.setState({resultsothers: responseJson.list});
            this.setState({path_urlsothers: responseJson.path});
          }
        } else {
          if (reportType == 'bills') {
            this.setState({results: []});
            this.setState({path_url: ''});
          } else if (reportType == 'prescription') {
            this.setState({resultspr: []});
            this.setState({path_urlspr: ''});
          } else if (reportType == 'labreport') {
            this.setState({resultslab: []});
            this.setState({path_urlslab: ''});
          } else if (reportType == 'others') {
            this.setState({resultsothers: []});
            this.setState({path_urlsothers: ''});
          }
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  };

  renderRowItemothers = itemData => {
    //            alert(JSON.stringify(itemData))
    var fullPath = this.state.path_urlsothers + itemData.item.image;
    //            console.log(fullPath)
    return (
      <View
        style={{
          width: '30%',
          margin: 5,
          height: 'auto',
          shadowColor: '#000',
          alignItems: 'center',
          backgroundColor: 'white',
          justifyContent: 'center',
        }}>
        {itemData.item.file_type == 'pdf' && (
          <View>
            <TouchableOpacity
              style={{position: 'absolute', right: 5}}
              onPress={() =>
                this.deleteImage(itemData.item.id, GLOBAL.reportType)
              }>
              <Image
                style={{width: 20, height: 20, resizeMode: 'contain'}}
                source={require('./circle_cross.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{margin: 10}}
              onPress={() => Linking.openURL(fullPath)}>
              <Image
                style={{
                  width: 60,
                  height: 60,
                  resizeMode: 'contain',
                  alignSelf: 'center',
                }}
                source={require('./pdf_im.png')}
              />

              <Text
                style={{
                  fontSize: 11,
                  fontFamily: 'Konnect-Medium',
                  color: 'black',
                  marginTop: 10,
                  width: 80,
                }}>
                {itemData.item.image}
              </Text>
            </TouchableOpacity>
          </View>
        )}
        {itemData.item.file_type != 'pdf' && (
          <TouchableOpacity
            style={{margin: 10}}
            onPress={() => this.openImage1(itemData, fullPath)}>
            <Image
              style={{
                width: 60,
                height: 60,
                resizeMode: 'contain',
                alignSelf: 'center',
              }}
              source={{uri: fullPath}}
            />

            <Text
              style={{
                fontSize: 11,
                fontFamily: 'Konnect-Medium',
                color: 'black',
                marginTop: 10,
                width: 80,
              }}>
              {itemData.item.image}
            </Text>
          </TouchableOpacity>
        )}
      </View>
    );
  };

  renderRowItemlab = itemData => {
    //            alert(JSON.stringify(itemData))
    var fullPath = this.state.path_urlslab + itemData.item.image;
    //            console.log(fullPath)
    return (
      <View
        style={{
          width: '30%',
          margin: 5,
          height: 'auto',
          shadowColor: '#000',
          alignItems: 'center',
          backgroundColor: 'white',
          justifyContent: 'center',
        }}>
        {itemData.item.file_type == 'pdf' && (
          <View>
            <TouchableOpacity
              style={{position: 'absolute', right: 5}}
              onPress={() =>
                this.deleteImage(itemData.item.id, GLOBAL.reportType)
              }>
              <Image
                style={{width: 20, height: 20, resizeMode: 'contain'}}
                source={require('./circle_cross.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{margin: 10}}
              onPress={() => Linking.openURL(fullPath)}>
              <Image
                style={{
                  width: 60,
                  height: 60,
                  resizeMode: 'contain',
                  alignSelf: 'center',
                }}
                source={require('./pdf_im.png')}
              />

              <Text
                style={{
                  fontSize: 11,
                  fontFamily: 'Konnect-Medium',
                  color: 'black',
                  marginTop: 10,
                  width: 80,
                }}>
                {itemData.item.image}
              </Text>
            </TouchableOpacity>
          </View>
        )}
        {itemData.item.file_type != 'pdf' && (
          <TouchableOpacity
            style={{margin: 10}}
            onPress={() => this.openImage1(itemData, fullPath)}>
            <Image
              style={{
                width: 60,
                height: 60,
                resizeMode: 'contain',
                alignSelf: 'center',
              }}
              source={{uri: fullPath}}
            />

            <Text
              style={{
                fontSize: 11,
                fontFamily: 'Konnect-Medium',
                color: 'black',
                marginTop: 10,
                width: 80,
              }}>
              {itemData.item.image}
            </Text>
          </TouchableOpacity>
        )}
      </View>
    );
  };

  renderRowItempr = itemData => {
    //            alert(JSON.stringify(itemData))
    var fullPath = this.state.path_urlspr + itemData.item.image;
    //            console.log(fullPath)
    return (
      <View
        style={{
          width: '30%',
          margin: 5,
          height: 'auto',
          shadowColor: '#000',
          alignItems: 'center',
          backgroundColor: 'white',
          justifyContent: 'center',
        }}>
        {itemData.item.file_type == 'pdf' && (
          <View>
            <TouchableOpacity
              style={{position: 'absolute', right: 5}}
              onPress={() =>
                this.deleteImage(itemData.item.id, GLOBAL.reportType)
              }>
              <Image
                style={{width: 20, height: 20, resizeMode: 'contain'}}
                source={require('./circle_cross.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{margin: 10}}
              onPress={() => Linking.openURL(fullPath)}>
              <Image
                style={{
                  width: 60,
                  height: 60,
                  resizeMode: 'contain',
                  alignSelf: 'center',
                }}
                source={require('./pdf_im.png')}
              />

              <Text
                style={{
                  fontSize: 11,
                  fontFamily: 'Konnect-Medium',
                  color: 'black',
                  marginTop: 10,
                  width: 80,
                }}>
                {itemData.item.image}
              </Text>
            </TouchableOpacity>
          </View>
        )}
        {itemData.item.file_type != 'pdf' && (
          <TouchableOpacity
            style={{margin: 10}}
            onPress={() => this.openImage1(itemData, fullPath)}>
            <Image
              style={{
                width: 60,
                height: 60,
                resizeMode: 'contain',
                alignSelf: 'center',
              }}
              source={{uri: fullPath}}
            />

            <Text
              style={{
                fontSize: 11,
                fontFamily: 'Konnect-Medium',
                color: 'black',
                marginTop: 10,
                width: 80,
              }}>
              {itemData.item.image}
            </Text>
          </TouchableOpacity>
        )}
      </View>
    );
  };

  _fancyShareMessage = item => {
    var a = 'Hey! Chekout my report from PiCaSoid app ' + item;

    Share.share(
      {
        message: a,
        url: item,
      },
      {
        tintColor: 'green',
        dialogTitle: 'Share this report via....',
      },
    ).then(this._showResult);
  };

  shareImage = item => {
    this._fancyShareMessage(item);
  };

  openImage1 = (itemData, fullPath) => {
    //    alert(JSON.stringify(itemData.item))
    GLOBAL.modelImage = fullPath;
    GLOBAL.modelImageTitle = itemData.item.image;
    GLOBAL.modelImageDate = itemData.item.added_on;
    GLOBAL.modelImageId = itemData.item.id;
    this.setModalVisible(true);
  };

  openImage2 = (itemData, fullPath) => {
    //    alert(JSON.stringify(itemData.item))
    GLOBAL.modelImage = fullPath;
    GLOBAL.modelImageTitle = itemData.item.image;
    GLOBAL.modelImageDate = itemData.item.added_on;
    GLOBAL.modelImageId = itemData.item.id;
    this.setModalVisible(true);
  };

  openImage3 = (itemData, fullPath) => {
    //    alert(JSON.stringify(itemData.item))
    GLOBAL.modelImage = fullPath;
    GLOBAL.modelImageTitle = itemData.item.image;
    GLOBAL.modelImageDate = itemData.item.added_on;
    GLOBAL.modelImageId = itemData.item.id;
    this.setModalVisible(true);
  };

  openImage4 = (itemData, fullPath) => {
    //    alert(JSON.stringify(itemData.item))
    GLOBAL.modelImage = fullPath;
    GLOBAL.modelImageTitle = itemData.item.image;
    GLOBAL.modelImageDate = itemData.item.added_on;
    GLOBAL.modelImageId = itemData.item.id;
    this.setModalVisible(true);
  };

  selectedFirst = () => {
    console.log('selected');
  };

  renderRowItem1 = itemData => {
    //            alert(JSON.stringify(itemData))
    var fullPath = this.state.path_url + itemData.item.image;
    //            console.log(fullPath)
    return (
      <View
        style={{
          width: '30%',
          margin: 5,
          height: 'auto',
          shadowColor: '#000',
          alignItems: 'center',
          backgroundColor: 'white',
          justifyContent: 'center',
        }}>
        {itemData.item.file_type == 'pdf' && (
          <View>
            <TouchableOpacity
              style={{position: 'absolute', right: 5}}
              onPress={() =>
                this.deleteImage(itemData.item.id, GLOBAL.reportType)
              }>
              <Image
                style={{width: 20, height: 20, resizeMode: 'contain'}}
                source={require('./circle_cross.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{margin: 10}}
              onPress={() => Linking.openURL(fullPath)}>
              <Image
                style={{
                  width: 60,
                  height: 60,
                  resizeMode: 'contain',
                  alignSelf: 'center',
                }}
                source={require('./pdf_im.png')}
              />

              <Text
                style={{
                  fontSize: 11,
                  fontFamily: 'Konnect-Medium',
                  color: 'black',
                  marginTop: 10,
                  width: 80,
                }}>
                {itemData.item.image}
              </Text>
            </TouchableOpacity>
          </View>
        )}
        {itemData.item.file_type != 'pdf' && (
          <TouchableOpacity
            style={{margin: 10}}
            onPress={() => this.openImage1(itemData, fullPath)}>
            <Image
              style={{
                width: 60,
                height: 60,
                resizeMode: 'contain',
                alignSelf: 'center',
              }}
              source={{uri: fullPath}}
            />

            <Text
              style={{
                fontSize: 11,
                fontFamily: 'Konnect-Medium',
                color: 'black',
                marginTop: 10,
                width: 80,
              }}>
              {itemData.item.image}
            </Text>
          </TouchableOpacity>
        )}
      </View>
    );
  };

  toggleOne = () => {
    this.getData('bills');
    this.setState({tone: !this.state.tone});
  };

  toggleTwo = () => {
    this.getData('prescription');
    this.setState({ttwo: !this.state.ttwo});
  };

  toggleThree = () => {
    this.getData('labreport');
    this.setState({tthree: !this.state.tthree});
  };

  toggleFour = () => {
    this.getData('others');
    this.setState({tfour: !this.state.tfour});
  };

  render() {
    return (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <Header
          navigation={this.props.navigation}
          headerName={'PERSONAL UPLOAD'}
        />
        <ScrollView>
          <Text
            style={{
              fontSize: 18,
              marginTop: 15,
              marginLeft: 15,
              color: 'black',
              fontFamily: 'Konnect-Medium',
            }}>
            Report Type
          </Text>

          <TouchableOpacity
            activeOpacity={0.99}
            onPress={() => this.toggleOne()}>
            <View
              style={{
                margin: 10,
                elevation: 10,
                borderRadius: 6,
                flexDirection: 'row',
                width: '95%',
                height: 40,
                backgroundColor: 'white',
                alignSelf: 'center',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontSize: 15,
                  marginLeft: 15,
                  color: 'black',
                  fontFamily: 'Konnect-Medium',
                  alignSelf: 'center',
                }}>
                Bills
              </Text>
              {this.state.tone == 0 && (
                <Image
                  style={{
                    width: 18,
                    height: 18,
                    resizeMode: 'contain',
                    margin: 10,
                  }}
                  source={require('./expand.png')}
                />
              )}
              {this.state.tone == 1 && (
                <Image
                  style={{
                    width: 18,
                    height: 18,
                    resizeMode: 'contain',
                    margin: 10,
                  }}
                  source={require('./close.png')}
                />
              )}
            </View>
          </TouchableOpacity>
          {this.state.tone == 0 && <View style={{height: 5}} />}
          {this.state.tone == 1 && (
            <View>
              {this.state.results.length == 0 && (
                <Text
                  style={{
                    fontSize: 13,
                    marginTop: 15,
                    color: 'black',
                    fontFamily: 'Konnect-Medium',
                    alignSelf: 'center',
                    textAlign: 'center',
                  }}>
                  No Docs found!
                </Text>
              )}

              {this.state.results.length != 0 && (
                <FlatList
                  style={{marginTop: 5}}
                  data={this.state.results}
                  numColumns={3}
                  keyExtractor={this._keyExtractor}
                  renderItem={this.renderRowItem1}
                  extraData={this.state}
                />
              )}
            </View>
          )}

          <TouchableOpacity
            activeOpacity={0.99}
            onPress={() => this.toggleTwo()}>
            <View
              style={{
                margin: 10,
                elevation: 10,
                borderRadius: 6,
                flexDirection: 'row',
                width: '95%',
                height: 40,
                backgroundColor: 'white',
                alignSelf: 'center',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontSize: 15,
                  marginLeft: 15,
                  color: 'black',
                  fontFamily: 'Konnect-Medium',
                  alignSelf: 'center',
                }}>
                Prescription
              </Text>
              {this.state.ttwo == 0 && (
                <Image
                  style={{
                    width: 18,
                    height: 18,
                    resizeMode: 'contain',
                    margin: 10,
                  }}
                  source={require('./expand.png')}
                />
              )}
              {this.state.ttwo == 1 && (
                <Image
                  style={{
                    width: 18,
                    height: 18,
                    resizeMode: 'contain',
                    margin: 10,
                  }}
                  source={require('./close.png')}
                />
              )}
            </View>
          </TouchableOpacity>

          {this.state.ttwo == 0 && <View style={{height: 5}} />}
          {this.state.ttwo == 1 && (
            <View>
              {this.state.resultspr.length == 0 && (
                <Text
                  style={{
                    fontSize: 13,
                    marginTop: 15,
                    color: 'black',
                    fontFamily: 'Konnect-Medium',
                    alignSelf: 'center',
                    textAlign: 'center',
                  }}>
                  No Docs found!
                </Text>
              )}

              {this.state.resultspr.length != 0 && (
                <FlatList
                  style={{marginTop: 5}}
                  data={this.state.resultspr}
                  numColumns={3}
                  keyExtractor={this._keyExtractor}
                  renderItem={this.renderRowItempr}
                  extraData={this.state}
                />
              )}
            </View>
          )}

          <TouchableOpacity
            activeOpacity={0.99}
            onPress={() => this.toggleThree()}>
            <View
              style={{
                margin: 10,
                elevation: 10,
                borderRadius: 6,
                flexDirection: 'row',
                width: '95%',
                height: 40,
                backgroundColor: 'white',
                alignSelf: 'center',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontSize: 15,
                  marginLeft: 15,
                  color: 'black',
                  fontFamily: 'Konnect-Medium',
                  alignSelf: 'center',
                }}>
                Lab Reports
              </Text>
              {this.state.tthree == 0 && (
                <Image
                  style={{
                    width: 18,
                    height: 18,
                    resizeMode: 'contain',
                    margin: 10,
                  }}
                  source={require('./expand.png')}
                />
              )}
              {this.state.tthree == 1 && (
                <Image
                  style={{
                    width: 18,
                    height: 18,
                    resizeMode: 'contain',
                    margin: 10,
                  }}
                  source={require('./close.png')}
                />
              )}
            </View>
          </TouchableOpacity>

          {this.state.tthree == 0 && <View style={{height: 5}} />}
          {this.state.tthree == 1 && (
            <View>
              {this.state.resultslab.length == 0 && (
                <Text
                  style={{
                    fontSize: 13,
                    marginTop: 15,
                    color: 'black',
                    fontFamily: 'Konnect-Medium',
                    alignSelf: 'center',
                    textAlign: 'center',
                  }}>
                  No Docs found!
                </Text>
              )}

              {this.state.resultslab.length != 0 && (
                <FlatList
                  style={{marginTop: 5}}
                  data={this.state.resultslab}
                  numColumns={3}
                  keyExtractor={this._keyExtractor}
                  renderItem={this.renderRowItemlab}
                  extraData={this.state}
                />
              )}
            </View>
          )}

          <TouchableOpacity
            activeOpacity={0.99}
            onPress={() => this.toggleFour()}>
            <View
              style={{
                margin: 10,
                elevation: 10,
                borderRadius: 6,
                flexDirection: 'row',
                width: '95%',
                height: 40,
                backgroundColor: 'white',
                alignSelf: 'center',
                justifyContent: 'space-between',
              }}>
              <Text
                style={{
                  fontSize: 15,
                  marginLeft: 15,
                  color: 'black',
                  fontFamily: 'Konnect-Medium',
                  alignSelf: 'center',
                }}>
                Others
              </Text>
              {this.state.tfour == 0 && (
                <Image
                  style={{
                    width: 18,
                    height: 18,
                    resizeMode: 'contain',
                    margin: 10,
                  }}
                  source={require('./expand.png')}
                />
              )}
              {this.state.tfour == 1 && (
                <Image
                  style={{
                    width: 18,
                    height: 18,
                    resizeMode: 'contain',
                    margin: 10,
                  }}
                  source={require('./close.png')}
                />
              )}
            </View>
          </TouchableOpacity>

          {this.state.tfour == 0 && <View style={{height: 5}} />}
          {this.state.tfour == 1 && (
            <View>
              {this.state.resultsothers.length == 0 && (
                <Text
                  style={{
                    fontSize: 13,
                    marginTop: 15,
                    color: 'black',
                    fontFamily: 'Konnect-Medium',
                    alignSelf: 'center',
                    textAlign: 'center',
                    marginBottom: 15,
                  }}>
                  No Docs found!
                </Text>
              )}

              {this.state.resultsothers.length != 0 && (
                <FlatList
                  style={{marginTop: 5}}
                  data={this.state.resultsothers}
                  numColumns={3}
                  keyExtractor={this._keyExtractor}
                  renderItem={this.renderRowItemothers}
                  extraData={this.state}
                />
              )}
            </View>
          )}
        </ScrollView>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            //             Alert.alert('Modal has been closed.');
            this.setModalVisible(!this.state.modalVisible);
          }}>
          <TouchableOpacity
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              backgroundColor: 'rgba(0, 0, 0, 0.5)',
              alignItems: 'center',
            }}
            activeOpacity={1}
            onPressOut={() => {
              this.setModalVisible(false);
            }}>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                backgroundColor: 'rgba(0, 0, 0, 0.5)',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: 300,
                  backgroundColor: 'white',
                  height: 400,
                }}>
                <View style={{width: '100%'}}>
                  <View style={{flexDirection: 'column'}}>
                    <Image
                      style={{width: '100%', height: '80%'}}
                      source={{uri: GLOBAL.modelImage}}
                    />

                    <View
                      style={{
                        width: '100%',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <Text
                        style={{
                          color: 'black',
                          fontFamily: 'Konnect-Medium',
                          fontSize: 15,
                          width: '30%',
                        }}
                        numberOfLines={1}>
                        {GLOBAL.modelImageTitle}
                      </Text>
                      <Text
                        style={{
                          color: 'black',
                          fontFamily: 'Konnect-Medium',
                          fontSize: 15,
                        }}>
                        {GLOBAL.modelImageDate}
                      </Text>
                    </View>
                    <View
                      style={{
                        width: '100%',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                      }}>
                      <TouchableOpacity
                        onPress={() => this.downloadFile(GLOBAL.modelImage)}>
                        <View
                          style={{
                            height: 40,
                            width: 80,
                            flexDirection: 'row',
                            backgroundColor: '#800000',
                            margin: 10,
                            alignItems: 'center',
                            borderRadius: 8,
                          }}>
                          <Image
                            style={{
                              width: 15,
                              height: 15,
                              resizeMode: 'contain',
                              margin: 5,
                            }}
                            source={require('./im_download.png')}
                          />
                          <Text
                            style={{
                              color: 'white',
                              fontFamily: 'Konnect-Medium',
                              fontSize: 11,
                            }}>
                            Downlod
                          </Text>
                        </View>
                      </TouchableOpacity>

                      <TouchableOpacity
                        onPress={() =>
                          this.deleteImage(
                            GLOBAL.modelImageId,
                            GLOBAL.reportType,
                          )
                        }>
                        <View
                          style={{
                            height: 40,
                            width: 80,
                            flexDirection: 'row',
                            backgroundColor: '#800000',
                            margin: 10,
                            alignItems: 'center',
                            borderRadius: 8,
                            padding: 6,
                          }}>
                          <Image
                            style={{
                              width: 15,
                              height: 15,
                              resizeMode: 'contain',
                              margin: 5,
                            }}
                            source={require('./im_delete.png')}
                          />
                          <Text
                            style={{
                              color: 'white',
                              fontFamily: 'Konnect-Medium',
                              fontSize: 11,
                            }}>
                            Delete
                          </Text>
                        </View>
                      </TouchableOpacity>

                      <TouchableOpacity
                        onPress={() => this.shareImage(GLOBAL.modelImage)}>
                        <View
                          style={{
                            height: 40,
                            width: 80,
                            flexDirection: 'row',
                            backgroundColor: '#800000',
                            margin: 10,
                            alignItems: 'center',
                            borderRadius: 8,
                            padding: 6,
                          }}>
                          <Image
                            style={{
                              width: 15,
                              height: 15,
                              resizeMode: 'contain',
                              margin: 5,
                            }}
                            source={require('./im_share.png')}
                          />
                          <Text
                            style={{
                              color: 'white',
                              fontFamily: 'Konnect-Medium',
                              fontSize: 11,
                            }}>
                            Share
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </TouchableOpacity>
        </Modal>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  appBar: {
    backgroundColor: '#800000',
  },
});
