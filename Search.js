import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  Modal,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Button from 'react-native-button';
const window = Dimensions.get('window');
import store from 'react-native-simple-store';
import {DialogComponent, DialogTitle} from 'react-native-dialog-component';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import Header from './Header.js';
const GLOBAL = require('./Global');
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};
var dict = {};
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      recognized: '',
      started: '',
      text: '',
      department: [],
      speciality: [],
      hospital: [],
      price: [],
      type: '',
      results: [],
      value: 0,
    };
  }

  componentWillUnmount() {}

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
    };
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  _handleStateChange = state => {};

  componentDidMount() {
    this.props.navigation.addListener('willFocus', this._handleStateChange);
  }

  submits = () => {
    if (this.state.value == 0) {
      GLOBAL.myonlinetype = 'online';
      this.props.navigation.navigate('OnlineBooking');
    } else {
      GLOBAL.myonlinetype = 'offline';
      GLOBAL.onlinetype = 'normal';
      this.props.navigation.navigate('BookingAppointmentDetail');
    }
  };

  login = (s, item) => {
    console.log(JSON.stringify(item));
    dict = item;

    GLOBAL.appointmentArray = item;
    GLOBAL.speciality = s;
    GLOBAL.pica_booking_charge_online = item.booking_charge_for_online;
    GLOBAL.pica_booking_charge_offline = item.booking_charge_for_offline;

    if (item.online_consult == '3' && item.normal_appointment == '1') {
      this.dialogComponent.show();
    } else if (item.online_consult == '3') {
      GLOBAL.type = '4';
      GLOBAL.price = item.online_consult_video_price;
      GLOBAL.type = '4';
      //            this.dialogComponent.show()

      this.props.navigation.navigate('OnlineBooking');
    } else if (item.normal_appointment == '1') {
      GLOBAL.price = item.normal_appointment_price;
      GLOBAL.type = '5';
      GLOBAL.onlinetype = 'normal';
      //   this.dialogComponent.show()

      this.props.navigation.navigate('BookingAppointmentDetail');
    } else if (item.online_consult == '0' && item.normal_appointment == '0') {
      alert('Currently this doctor is not in service.');
    } else if (item.online_consult == '0' && item.normal_appointment == '1') {
      GLOBAL.price = item.normal_appointment_price;
      GLOBAL.type = '5';
      GLOBAL.onlinetype = 'normal';
      //   this.dialogComponent.show()

      this.props.navigation.navigate('BookingAppointmentDetail');
    }
  };

  selectedFirst = (item, speciality) => {
    //  alert(JSON.stringify(item))
    if (this.state.type == 'doctor_result') {
      GLOBAL.speciality = speciality;
      GLOBAL.appointmentArray = item;
      this.props.navigation.navigate('DoctorDetail');
    } else {
      GLOBAL.searchSpeciality = item.specialty_name;
      this.props.navigation.navigate('SearchSpeciality');
    }
  };

  _renderItems = ({item, index}) => {
    var speciality = item.speciality_detail_array;
    //         console.log('kik' +item.like)

    return (
      <TouchableOpacity onPress={() => this.selectedFirst(item, speciality)}>
        <View
          style={{
            flex: 1,
            marginLeft: 5,
            width: window.width - 10,
            backgroundColor: 'white',
            marginTop: 10,
            marginBottom: 10,
            borderRadius: 10,
          }}>
          <View style={{flexDirection: 'row', width: '100%'}}>
            <View>
              <Image
                style={{width: 60, height: 60, borderRadius: 30, margin: 10}}
                source={{uri: item.image}}
              />
              <View
                style={{
                  backgroundColor: '#800000',
                  borderRadius: 4,
                  width: 40,
                  height: 20,
                  marginTop: 2,
                  flexDirection: 'row',
                  justifyItems: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                }}>
                <Image
                  style={{
                    width: 8,
                    height: 8,
                    marginLeft: 4,
                    resizeMode: 'contain',
                  }}
                  source={require('./star.png')}
                />

                <Text
                  style={{
                    marginLeft: 5,
                    fontSize: 8,
                    marginTop: 3,
                    color: 'white',
                    fontFamily: 'Konnect-Medium',
                  }}>
                  {item.ratting}
                </Text>
              </View>
              {item.doctor_avail_status == 1 && (
                <Text
                  style={{
                    fontSize: 9,
                    color: '#3DBA56',
                    fontFamily: 'Konnect-Medium',
                    width: 50,
                    textAlign: 'center',
                    alignSelf: 'center',
                  }}>
                  Online
                </Text>
              )}
              {item.doctor_avail_status != 1 && (
                <Text
                  style={{
                    fontSize: 9,
                    color: 'red',
                    fontFamily: 'Konnect-Medium',
                    width: 50,
                    textAlign: 'center',
                    alignSelf: 'center',
                  }}>
                  Offline
                </Text>
              )}
            </View>

            <View>
              <View style={{flexDirection: 'row', width: '100%'}}>
                <Text
                  style={{
                    marginLeft: 5,
                    fontSize: 15,
                    color: 'black',
                    fontFamily: 'Konnect-Medium',
                    width: '80%',
                    marginTop: 18,
                  }}>
                  {item.name}
                </Text>
              </View>

              <View style={{flexDirection: 'row'}}>
                <Text
                  style={{
                    marginLeft: 5,
                    fontSize: 12,
                    color: 'grey',
                    height: 'auto',
                    fontFamily: 'Konnect-Medium',
                    width: window.width - 100,
                  }}>
                  {speciality}
                </Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Image
                  style={{width: 20, height: 20, resizeMode: 'contain'}}
                  source={require('./location.png')}
                />

                <Text
                  style={{
                    marginLeft: 5,
                    width: window.width - 150,
                    height: 'auto',
                    fontSize: 12,
                    color: '#8F8F8F',
                    fontFamily: 'Konnect-Medium',
                  }}>
                  Hospital/Clinic: {item.lat_long_address}
                </Text>
              </View>

              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View>
                  <Text
                    style={{
                      fontSize: 12,
                      color: 'black',
                      fontFamily: 'Konnect-Medium',
                    }}>
                    Experience
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#3A3A3A',
                      fontFamily: 'Konnect-Medium',
                      textAlign: 'center',
                    }}>
                    {item.experience} Years
                  </Text>
                </View>

                <View>
                  <Text
                    style={{
                      fontSize: 12,
                      color: 'black',
                      fontFamily: 'Konnect-Medium',
                    }}>
                    Likes
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#3A3A3A',
                      fontFamily: 'Konnect-Medium',
                      textAlign: 'center',
                    }}>
                    {item.like}
                  </Text>
                </View>

                <View style={{marginRight: 50}}>
                  <Text
                    style={{
                      fontSize: 12,
                      color: 'black',
                      fontFamily: 'Konnect-Medium',
                    }}>
                    Reviews
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      color: '#3A3A3A',
                      fontFamily: 'Konnect-Medium',
                      textAlign: 'center',
                    }}>
                    {item.total_review}
                  </Text>
                </View>
              </View>
              {item.online_consult == '0' && item.normal_appointment == '1' && (
                <Text
                  style={{
                    fontSize: 12,
                    color: '#800000',
                    fontFamily: 'Konnect-Medium',
                  }}>
                  Consult for ₹ {item.normal_appointment_price}/- onwards
                </Text>
              )}

              {item.online_consult != '3' && item.online_consult == '1' && (
                <Text
                  style={{
                    fontSize: 12,
                    color: '#800000',
                    fontFamily: 'Konnect-Medium',
                  }}>
                  Consult for ₹ {item.online_consult_chat_price}/- onwards
                </Text>
              )}
              {item.online_consult != '3' && item.online_consult == '2' && (
                <Text
                  style={{
                    fontSize: 12,
                    color: '#800000',
                    fontFamily: 'Konnect-Medium',
                  }}>
                  Consult for ₹ {item.online_consult_video_price}/- onwards
                </Text>
              )}
              {item.online_consult == '3' && (
                <Text
                  style={{
                    fontSize: 12,
                    color: '#800000',
                    fontFamily: 'Konnect-Medium',
                  }}>
                  Consult for ₹ {item.online_consult_video_price}/- onwards
                </Text>
              )}

              <Text
                style={{
                  fontSize: 12,
                  color: '#800000',
                  fontFamily: 'Konnect-Medium',
                }}>
                Online Booking Charge is ₹ {item.booking_charge_for_online}/-
                only
              </Text>

              <Text
                style={{
                  fontSize: 12,
                  color: '#800000',
                  fontFamily: 'Konnect-Medium',
                }}>
                Offline Booking Charge is ₹ {item.booking_charge_for_offline}/-
                only
              </Text>
            </View>
          </View>
          <Button
            style={{
              padding: 6,
              marginTop: 5,
              fontSize: 15,
              color: 'white',
              backgroundColor: '#800000',
              marginLeft: '55%',
              width: '20%',
              height: 34,
              fontFamily: 'Konnect-Medium',
              borderRadius: 12,
              marginBottom: 20,
            }}
            styleDisabled={{color: 'red'}}
            onPress={() => this.login(speciality, item)}>
            Consult
          </Button>
        </View>
      </TouchableOpacity>
    );
  };

  SearchFilterFunction(text) {
    const url = GLOBAL.BASE_URL + 'serach_doctor';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        patient_id: GLOBAL.user_id,
        lat: GLOBAL.lat,
        long: GLOBAL.long,
        search_keyword: text,
        state: GLOBAL.myStatefrom,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //                console.log(JSON.stringify(responseJson.booking_price_for_online))
        if (responseJson.result == true) {
          GLOBAL.hDoctorBookingAmountOnline =
            responseJson.booking_price_for_online;
          GLOBAL.hDoctorBookingAmountOffline =
            responseJson.booking_price_for_offline;
          this.setState({
            type: responseJson.type,

            results: responseJson.doctor_list_s,
          });
          results.sort(function (a, b) {
            return a.localeCompare(b); //using String.prototype.localCompare()
          });
        } else {
          this.setState({results: []});
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  }

  render() {
    var radio_props = [
      {label: 'Online Consultation', value: 0},
      {label: 'Offline Consultation', value: 1},
    ];

    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header
          navigation={this.props.navigation}
          headerName={'SEARCH DOCTORS'}
        />

        <View
          style={{
            margin: 10,
            width: window.width - 20,
            height: 50,
            borderRadius: 20,
            flexDirection: 'row',
            backgroundColor: 'white',
          }}>
          <Image
            style={{
              width: 18,
              height: 18,
              alignSelf: 'center',
              resizeMode: 'contain',
              marginLeft: 13,
            }}
            source={require('./search.png')}
          />

          <TextInput
            style={{marginLeft: 10, width: window.width - 100}}
            placeholderTextColor="rgba(0, 0, 0, 0.4)"
            onChangeText={text => this.SearchFilterFunction(text)}
            value={this.state.height}
            autoFocus={true}
            placeholder={'Search Doctor By Name/Speciality'}
          />
        </View>

        <FlatList
          style={{flexGrow: 0, margin: 8, height: window.height - 130}}
          data={this.state.results}
          numColumns={1}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItems}
          extraData={this.state}
        />

        <DialogComponent
          dialogStyle={{backgroundColor: 'transparent'}}
          dialogTitle={<DialogTitle title="Dialog Title" />}
          dismissOnTouchOutside={true}
          dismissOnHardwareBackPress={true}
          ref={dialogComponent => {
            this.dialogComponent = dialogComponent;
          }}>
          <View
            style={{
              width: window.width - 30,
              alignSelf: 'center',
              backgroundColor: 'transparent',
              flexDirection: 'column',
            }}>
            <View
              style={{
                backgroundColor: '#6d0000',
                width: '100%',
                height: 50,
                justifyContent: 'space-between',
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  margin: 15,
                  color: 'white',
                  fontFamily: 'Konnect-Regular',
                  fontSize: 17,
                }}>
                Choose Booking Type
              </Text>
              <TouchableOpacity onPress={() => this.dialogComponent.dismiss()}>
                <Image
                  style={{
                    width: 25,
                    height: 25,
                    resizeMode: 'contain',
                    margin: 15,
                  }}
                  source={require('./cross.png')}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                width: window.width - 30,
                backgroundColor: 'white',
                height: 140,
              }}>
              <View
                style={{
                  marginTop: 20,
                  width: '100%',
                  marginLeft: 20,
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <RadioForm
                  radio_props={radio_props}
                  initial={0}
                  buttonColor={'#800000'}
                  buttonOuterColor={'#800000'}
                  selectedButtonColor={'#800000'}
                  animation={false}
                  buttonSize={12}
                  labelColor={'black'}
                  buttonStyle={{marginTop: 20}}
                  buttonWrapStyle={{marginTop: 20}}
                  labelStyle={{
                    fontSize: 16,
                    fontFamily: 'Konnect-Regular',
                    width: '90%',
                    marginLeft: 10,
                    paddingBottom: 10,
                  }}
                  onPress={value => {
                    this.setState({value: value});
                  }}
                />
              </View>

              <Button
                style={{
                  padding: 6,
                  marginTop: 5,
                  width: 100,
                  fontSize: 14,
                  color: 'white',
                  backgroundColor: '#800000',
                  alignSelf: 'center',
                  height: 30,
                  fontFamily: 'Konnect-Regular',
                  borderRadius: 4,
                }}
                styleDisabled={{color: 'red'}}
                onPress={() => this.submits()}>
                SUBMIT
              </Button>
            </View>
          </View>
        </DialogComponent>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    flex: 1,
    backgroundColor: '#f1f1f1',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
});
