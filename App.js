import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Alert, StatusBar} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import 'react-native-gesture-handler';
import AppNavigator from './Navigator';
// import NotifService from './NotifService';
const GLOBAL = require('./Global');
import appConfig from './app.json';
import PushNotification from 'react-native-push-notification';

export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      senderId: appConfig.senderID,
      gotNotif: 0,
    };
  }

  pushNotificationMessage = ({title, message}) =>
    PushNotification.localNotification({
      title,
      message,
    });
  componentDidMount() {
    PushNotification.configure({
      onRegister: ({token}) => {
        AsyncStorage.setItem('token', token);
        //   GLOBAL.firebaseToken = token.token;
        GLOBAL.firebaseToken = token;
        console.log('TOKEN:', token);
      },

      onNotification: notification => {
        console.log('notification');
        //  console.log(JSON.stringify(notification, null, 2));
        pushNotificationMessage(notification);
        // const {data = {}} = notification;
        // const {type = ''} = data;
      },
      onAction: function (notification) {
        console.log('ACTION:', notification.action);
        console.log('NOTIFICATION:', notification);
        // process the action
      },
      onRegistrationError: function (err) {
        console.error(err.message, err);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      senderID: '599081959923',
      popInitialNotification: true,
      requestPermissions: true,
    });

    PushNotification.localNotificationSchedule({
      //... You can use all the options from localNotifications
      message: 'My Notification Message', // (required)
      date: new Date(Date.now() + 60 * 1000), // in 60 secs
      allowWhileIdle: false, // (optional) set notification to work while on doze, default: false
    });
  }

  render() {
    //    StatusBar.setBarStyle('light-content', true);
    return (
      <AppNavigator>
        <View style={{flex: 1}}>
          {this.state.gotNotif == 1 && (
            <View>
              <Text>title</Text>
              <Text>body</Text>
            </View>
          )}

          <AppNavigator />
        </View>
      </AppNavigator>
    );
  }
  // onRegister(token) {
  //   AsyncStorage.setItem('token', token.token);
  //   GLOBAL.firebaseToken = token.token;
  //   console.log('TOKEN:', token);
  //   this.setState({registerToken: token.token, fcmRegistered: true});
  // }

  // onNotif(notif) {
  //   console.log(notif);
  //   // Alert.alert(notif.title, notif.message+notif.foreground);
  //   this.setState({gotNotif: 1});
  // }

  // handlePerm(perms) {
  //   Alert.alert('Permissions', JSON.stringify(perms));
  // }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    borderWidth: 1,
    borderColor: '#000000',
    margin: 5,
    padding: 5,
    width: '70%',
    backgroundColor: '#DDDDDD',
    borderRadius: 5,
  },
  textField: {
    borderWidth: 1,
    borderColor: '#AAAAAA',
    margin: 5,
    padding: 5,
    width: '70%',
  },
  spacer: {
    height: 10,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
});
