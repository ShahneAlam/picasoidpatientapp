import React, {Component} from 'react';
import {NavigationActions, StackActions} from 'react-navigation';
import PropTypes from 'prop-types';
import {
  ScrollView,
  Text,
  View,
  Linking,
  StyleSheet,
  Modal,
  Image,
  TouchableOpacity,
  Alert,
  Share,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {DrawerActions} from 'react-navigation-drawer';
import Button from 'react-native-button';
import * as Animatable from 'react-native-animatable';

const GLOBAL = require('./Global');

class Drawer extends React.Component {
  constructor(props) {
    super(props);
    const {navigation} = this.props;
    this.state = {
      my: 'sdf',
      modalVisible: false,
      expandList: true,
      pic: '',
    };
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  _fancyShareMessage = () => {
    var a =
      'Checkout PiCaSoid app ' +
      '\n' +
      'Download the app from here:' +
      '\n' +
      'Android: ' +
      GLOBAL.androidappUrl +
      '\n' +
      'iOS: ' +
      GLOBAL.iosappUrl;

    Share.share(
      {
        message: a,
      },
      {
        tintColor: 'green',
        dialogTitle: 'Share this app via....',
      },
    ).then(this._showResult);
  };

  componentDidMount() {
    var value = AsyncStorage.getItem('name');
    value.then(e => {
      GLOBAL.name = e;

      this.setState({my: GLOBAL.name});
    });
    this.props.navigation.addListener('willFocus', this._handleStateChange);
  }

  _handleStateChange = state => {
    this.getData();
    this.getInterPrice();
  };

  getInterPrice = () => {
    const url = GLOBAL.BASE_URL + 'settings';
    fetch(url, {
      method: 'GET',
    })
      .then(response => response.json())
      .then(responseJson => {
        // alert(JSON.stringify(responseJson.inter_state_amount))
        if (responseJson.status == true) {
          GLOBAL.hInterAmount = responseJson.inter_state_amount;
          GLOBAL.androidappUrl = responseJson.play_store_url;
          GLOBAL.iosappUrl = responseJson.app_url;
        } else {
          //                    alert('No Data Found')
        }
      })
      .catch(error => {
        console.error(error);
      });
  };
  getData = () => {
    const url = GLOBAL.BASE_URL + 'get_profile';

    //        alert(GLOBAL.user_id)
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: GLOBAL.user_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //                alert(JSON.stringify(responseJson))
        if (responseJson.status == true) {
          GLOBAL.myname = responseJson.user_detail.name;
          GLOBAL.myemail = responseJson.user_detail.email;
          GLOBAL.pica_id = responseJson.user_detail.user_unqui_id_prefix;
          GLOBAL.mypimage = responseJson.user_detail.image;
          GLOBAL.myStatefrom = responseJson.user_detail.state;
          //                    GLOBAL.myStatefrom= 'Rajasthan'

          GLOBAL.myPaymentStatus = responseJson.user_detail.app_payment_status;
          this.setState({pic: responseJson.user_detail.user_unqui_id_prefix});
        } else {
          alert('No Data Found');
        }
      })
      .catch(error => {
        console.error(error);
      });
  };

  expandList = visible => {
    this.setState({expandList: !this.state.expandList});
  };

  _YesLogout = () => {
    //        const url = GLOBAL.BASE_URL +  'logout'
    // //      this.showLoading()
    //       fetch(url, {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify({
    //     user_id : GLOBAL.userid,
    //   }),
    // }).then((response) => response.json())
    //     .then((responseJson) => {

    // //    alert(JSON.stringify(responseJson))
    //   //     this.hideLoading()
    //        if (responseJson.status == true) {
    AsyncStorage.removeItem('userID');

    this.props.navigation.dispatch(
      StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: 'Login',
            params: {someParams: 'parameters goes here...'},
          }),
        ],
      }),
    );
    this.props.navigation.closeDrawer();
    //  this.props.navigation.dispatch(DrawerActions.closeDrawer());

    //    }else {
    //        alert('Something Went Wrong.')
    //    }
    // })
    // .catch((error) => {
    //   console.error(error);
    // });
  };

  openMembers = () => {
    GLOBAL.typelist = '1';
    this.props.navigation.navigate('ListMember');
  };
  navigateToScreen1 = route => () => {
    Alert.alert(
      'Logout!',
      'Are you sure you want to Logout?',
      [{text: 'Cancel'}, {text: 'Yes', onPress: () => this._YesLogout()}],
      {cancelable: false},
    );
  };

  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
  };

  immunize = () => {
    const url = GLOBAL.BASE_URL + 'settings';
    fetch(url, {
      method: 'GET',
    })
      .then(response => response.json())
      .then(responseJson => {
        //                alert(JSON.stringify(responseJson.immunization_reminder))
        if (responseJson.status == true) {
          //                    console.log(responseJson.immunization_reminder +'?'+GLOBAL.pica_id)
          Linking.openURL(
            responseJson.immunization_reminder +
              '?picasoid_id=' +
              GLOBAL.pica_id,
          );
        } else {
          alert('No Data Found');
        }
      })
      .catch(error => {
        console.error(error);
      });
  };

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#800000'}}>
        <ScrollView>
          <View style={{backgroundColor: 'white'}}>
            <View style={styles.headertop}>
              <View
                style={{
                  marginTop: 20,
                  marginLeft: 20,
                  flexDirection: 'column',
                }}>
                <Image
                  style={{
                    width: 70,
                    height: 70,
                    borderRadius: 35,
                    marginLeft: 5,
                  }}
                  source={{uri: GLOBAL.mypimage}}
                />
                <View style={{flexDirection: 'column', marginTop: 5}}>
                  <Text
                    style={{
                      marginTop: 10,
                      color: 'white',
                      marginLeft: 10,
                      fontSize: 17,
                      height: 'auto',
                      fontFamily: 'Konnect-Regular',
                    }}>
                    {GLOBAL.myname}
                  </Text>
                  <Text style={[styles.drawerText, {color: 'white'}]}>
                    {GLOBAL.myemail}
                  </Text>
                  <Text style={[styles.drawerText, {color: 'white'}]}>
                    PiCaSoid Id : {GLOBAL.pica_id}
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('EditProfile')}>
                  <View
                    style={{
                      marginTop: 15,
                      padding: 5,
                      height: 32,
                      overflow: 'hidden',
                      borderRadius: 15,
                      marginLeft: 10,
                      borderWidth: 2,
                      width: 120,
                      marginBottom: 10,
                      borderColor: 'white',
                      backgroundColor: '#800000',
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 12,
                        color: 'white',
                        fontFamily: 'Konnect-Medium',
                        alignSelf: 'center',
                      }}>
                      Edit Profile
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.menuItem}>
              <Image
                style={styles.drawericon}
                source={require('./drawer/d_home.png')}
              />
              <Text
                style={styles.drawerTexts}
                onPress={() => this.props.navigation.toggleDrawer()}>
                Home
              </Text>
            </View>

            <View style={styles.menuItem}>
              <Image
                style={styles.drawericon}
                source={require('./drawer/d_book.png')}
              />
              <Text
                style={styles.drawerTexts}
                onPress={this.navigateToScreen('Appointment')}>
                My Bookings
              </Text>
            </View>

            <View style={styles.menuItem}>
              <Image
                style={styles.drawericon}
                source={require('./drawer/d_history.png')}
              />
              <Text
                style={styles.drawerTexts}
                onPress={() => this.props.navigation.navigate('LabHistory')}>
                Lab History
              </Text>
            </View>

            <View style={styles.menuItem}>
              <Image
                style={styles.drawericon}
                source={require('./drawer/d_document.png')}
              />
              <Text
                style={styles.drawerTexts}
                onPress={() => {
                  this.props.navigation.navigate('MyDocument');
                }}>
                My Documents
              </Text>
            </View>

            <View style={styles.menuItem}>
              <Image
                style={styles.drawericon}
                source={require('./drawer/d_member.png')}
              />
              <Text
                style={styles.drawerTexts}
                onPress={() => this.openMembers()}>
                List Members
              </Text>
            </View>

            <View style={styles.menuItem}>
              <Image
                style={styles.drawericon}
                source={require('./drawer/d_about.png')}
              />
              <Text
                style={styles.drawerTexts}
                onPress={() =>
                  Linking.openURL(
                    'https://www.picasoid.co.in/nehealthcard.aspx',
                  )
                }>
                NE Health Card
              </Text>
            </View>

            <View style={styles.menuItem}>
              <Image
                style={styles.drawericon}
                source={require('./drawer/d_reminder.png')}
              />
              <Text style={styles.drawerTexts} onPress={() => this.immunize()}>
                Immunization Reminder
              </Text>
            </View>

            <View style={styles.menuItem}>
              <Image
                style={styles.drawericon}
                source={require('./drawer/d_support.png')}
              />
              <Text
                style={styles.drawerTexts}
                onPress={() => this.props.navigation.navigate('Support')}>
                Treatment Enquiry
              </Text>
            </View>

            <View style={styles.menuItem}>
              <Image
                style={styles.drawericon}
                source={require('./drawer/d_share.png')}
              />
              <Text
                style={styles.drawerTexts}
                onPress={() => this._fancyShareMessage()}>
                Share with Friends
              </Text>
            </View>

            {/* 
                        <View style={styles.menuItem}>

                            <Image style={styles.drawericon}
                                   source={require('./drawer/d_tc.png')} />
                            <Text style = {styles.drawerTexts}
                                  onPress={()=>Linking.openURL('https://www.picasoid.co.in/privacypolicy.aspx')}>
                                Terms & Conditions
                            </Text>
                        </View> */}

            <View style={styles.menuItem}>
              <Image
                style={styles.drawericon}
                source={require('./drawer/d_lists.png')}
              />
              <Text
                style={styles.drawerTexts}
                onPress={() =>
                  Linking.openURL(
                    'https://www.picasoid.co.in/privacypolicy.aspx',
                  )
                }>
                Privacy Policy
              </Text>
            </View>

            <View style={styles.menuItem}>
              <Image
                style={styles.drawericon}
                source={require('./drawer/d_about.png')}
              />
              <Text
                style={styles.drawerTexts}
                onPress={() =>
                  Linking.openURL(
                    'https://www.picasoid.co.in/online-doctor-consultation.aspx',
                  )
                }>
                About Us
              </Text>
            </View>

            <View style={styles.menuItem}>
              <Image
                style={styles.drawericon}
                source={require('./drawer/d_logout.png')}
              />
              <Text
                style={styles.drawerTexts}
                onPress={this.navigateToScreen1('Login')}>
                Logout
              </Text>
            </View>
            <Modal
              animationType="fade"
              transparent={true}
              backdropOpacity={1}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                //         Alert.alert('Modal has been closed.');
                this.setModalVisible(false);
              }}>
              <TouchableOpacity
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  backgroundColor: 'rgba(0, 0, 0, 0.5)',
                  alignItems: 'center',
                  borderRadius: 8,
                }}
                activeOpacity={1}
                onPressOut={() => {
                  this.setModalVisible(false);
                }}>
                <View
                  style={{
                    width: 300,
                    backgroundColor: 'white',
                    height: 240,
                    borderRadius: 8,
                  }}>
                  <View
                    style={{
                      width: '100%',
                      backgroundColor: 'white',
                      borderRadius: 8,
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        width: '100%',
                        backgroundColor: '#800000',
                        height: 60,
                        borderTopLeftRadius: 8,
                        borderTopRightRadius: 8,
                        borderTopLeftWidth: 1,
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        borderTopRightWidth: 1,
                        borderTopRightColor: 'transparent',
                        borderTopLeftColor: 'transparent',
                      }}>
                      <Text
                        style={{
                          fontSize: 17,
                          color: 'white',
                          fontFamily: 'Konnect-Medium',
                          margin: 10,
                        }}>
                        Activate Interstate Account
                      </Text>
                      <TouchableOpacity
                        onPress={() => this.setModalVisible(false)}>
                        <Image
                          style={{
                            width: 25,
                            height: 25,
                            resizeMode: 'contain',
                            marginRight: 10,
                          }}
                          source={require('./cross.png')}
                        />
                      </TouchableOpacity>
                    </View>

                    <Text
                      style={{
                        fontSize: 16,
                        fontFamily: 'Konnect-Medium',
                        color: 'black',
                        marginTop: 15,
                        marginLeft: 15,
                        width: '90%',
                        textAlign: 'left',
                      }}>
                      By activating interstate account you will be able to place
                      appointment booking for other states also by paying a one
                      time amount of ₹ {GLOBAL.hInterAmount}/- only.
                    </Text>

                    <Button
                      style={{
                        fontSize: 16,
                        fontFamily: 'Konnect-Medium',
                        color: 'white',
                        alignSelf: 'center',
                        padding: 7,
                      }}
                      containerStyle={{
                        height: 35,
                        width: '40%',
                        borderRadius: 7,
                        alignSelf: 'center',
                        alignItems: 'center',
                        backgroundColor: '#800000',
                        marginTop: '5%',
                      }}
                      onPress={() => {
                        GLOBAL.finalType = 1;
                        this.props.navigation.navigate('Ccavenue');
                        this.setModalVisible(false);
                      }}>
                      Activate
                    </Button>
                  </View>
                </View>
              </TouchableOpacity>
            </Modal>
          </View>
        </ScrollView>
      </View>
    );
  }
}

Drawer.propTypes = {
  navigation: PropTypes.object,
};

const styles = StyleSheet.create({
  drawerText: {
    marginTop: 2,
    color: 'white',
    marginLeft: 10,
    fontSize: 13,
    fontFamily: 'Konnect-Medium',
  },
  headertop: {
    width: 300,
    height: 220,
    backgroundColor: '#800000',
    flexDirection: 'column',
  },
  menuItem: {
    padding: 10,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
  },
  drawericon: {
    borderLeftWidth: 1,
    width: 20,
    height: 20,
    marginLeft: 8,
    marginTop: 3,
    resizeMode: 'contain',
  },
  drawericons: {
    width: 20,
    height: 20,
    marginLeft: 8,
    marginTop: 3,
  },
  drawerTexts: {
    width: 'auto',
    height: 22,
    marginLeft: 45,
    marginTop: -18,
    color: 'black',
    fontFamily: 'Konnect-Regular',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,
    top: window.height / 2,
    opacity: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
});

export default Drawer;
