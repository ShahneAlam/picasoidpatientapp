import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Image,
  Text,
  Platform,
  FlatList,
  ActivityIndicator,
  Dimensions,
  StatusBar,
  TouchableOpacity,
  TextInput,
  Linking,
} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Button from 'react-native-button';
const {width, height} = Dimensions.get('window');
const window = Dimensions.get('window');
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import {Marker} from 'react-native-maps';
import store from 'react-native-simple-store';
//import { Dropdown } from 'react-native-material-dropdown-v2-fixed';
import {Dropdown} from 'react-native-material-dropdown';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import Carousel, {Pagination} from 'react-native-snap-carousel';

const SCREEN_HEIGHT = height;
const SCREEN_WIDTH = width;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;

const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const GLOBAL = require('./Global');
type Props = {};
const FirstRoute = () => (
  <ScrollView style={styles.container}>
    <View style={{flexDirection: 'column'}}>
      <View style={{flexDirection: 'row', margin: 10}}>
        <Image
          style={{width: 20, height: 20, resizeMode: 'contain', marginTop: 3}}
          source={require('./dplus.png')}
        />
        <View style={{flexDirection: 'column', marginLeft: 30}}>
          <Text style={{fontSize: 15, color: 'black'}}>Experience</Text>
          <Text style={{fontSize: 15, color: 'grey'}}>10 yrs</Text>
        </View>
        <Image
          style={{
            width: 20,
            height: 20,
            resizeMode: 'contain',
            marginTop: 3,
            marginLeft: window.width / 6,
          }}
          source={require('./loc.png')}
        />
        <View style={{flexDirection: 'column', marginLeft: 30}}>
          <Text style={{fontSize: 15, color: 'black'}}>Distance</Text>
          <Text style={{fontSize: 15, color: 'grey'}}>10 Km</Text>
        </View>
      </View>
    </View>
    <View
      style={{
        width: window.width - 20,
        height: 1,
        backgroundColor: '#bfbfbf',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
      }}></View>

    <View style={{flexDirection: 'row', margin: 10}}>
      <Image
        style={{width: 20, height: 20, resizeMode: 'contain', marginTop: 3}}
        source={require('./dre.png')}
      />
      <View style={{flexDirection: 'column', marginLeft: 30}}>
        <Text style={{fontSize: 15, color: 'black'}}>Review</Text>
        <Text style={{fontSize: 15, color: 'grey'}}>10 </Text>
      </View>
      <Image
        style={{
          width: 20,
          height: 20,
          resizeMode: 'contain',
          marginTop: 3,
          marginLeft: window.width / 6,
        }}
        source={require('./recommended.png')}
      />
      <View style={{flexDirection: 'column', marginLeft: 30}}>
        <Text style={{fontSize: 15, color: 'black'}}>Recommendtions</Text>
        <Text style={{fontSize: 15, color: 'grey'}}>168</Text>
      </View>
    </View>

    <View
      style={{
        width: window.width - 20,
        height: 1,
        backgroundColor: '#bfbfbf',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
      }}></View>

    <View style={{flexDirection: 'row', margin: 10}}>
      <Image
        style={{width: 20, height: 20, resizeMode: 'contain', marginTop: 3}}
        source={require('./dhouse.png')}
      />
      <View style={{flexDirection: 'column', marginLeft: 30}}>
        <Text style={{fontSize: 15, color: 'black'}}>Medeor Hospital</Text>
        <Text style={{fontSize: 15, color: 'grey'}}>
          Chaudhary Chandu Singh Marg, Block A2 Building
        </Text>
        <Text style={{fontSize: 15, color: 'grey'}}>
          Landmark: Green Field School
        </Text>
        <Image
          style={{width: 280, height: 150, resizeMode: 'cover', margin: 5}}
          source={require('./dloc.png')}
        />
      </View>
    </View>

    <View style={{flexDirection: 'row', margin: 10}}>
      <Image
        style={{width: 20, height: 20, resizeMode: 'contain', marginTop: 3}}
        source={require('./dtime.png')}
      />
      <View style={{flexDirection: 'column', marginLeft: 30}}>
        <Text style={{fontSize: 15, color: '#8BC60D'}}>AVAILABLE TODAY</Text>
        <Text style={{fontSize: 15, color: 'grey'}}>10:30 AM - 05:00 PM</Text>
        <Text style={{fontSize: 13, color: 'blue'}}>ALL TIMINGS</Text>
      </View>
    </View>

    <View style={{flexDirection: 'row', margin: 10}}>
      <Image
        style={{width: 20, height: 20, resizeMode: 'contain', marginTop: 3}}
        source={require('./dtick.png')}
      />
      <View style={{flexDirection: 'column', marginLeft: 30}}>
        <Text style={{fontSize: 15, color: '#010101'}}>SERVICES</Text>
        <Text
          style={{fontSize: 12, color: 'grey', marginTop: 5, marginLeft: 5}}>
          -Aesthetics Dentistry
        </Text>
        <Text
          style={{fontSize: 12, color: 'grey', marginTop: 5, marginLeft: 5}}>
          -Laser Bleaching
        </Text>
        <Text
          style={{fontSize: 12, color: 'grey', marginTop: 5, marginLeft: 5}}>
          -Office Bleaching
        </Text>
        <Text style={{fontSize: 13, color: 'red', marginTop: 5}}>
          ALL SERVICES
        </Text>
      </View>
    </View>

    <View style={{flexDirection: 'row', margin: 10}}>
      <Image
        style={{width: 20, height: 20, resizeMode: 'contain', marginTop: 3}}
        source={require('./dtick.png')}
      />
      <View style={{flexDirection: 'column', marginLeft: 30}}>
        <Text style={{fontSize: 15, color: '#010101'}}>ALSO PRACTICES AT</Text>
        <Text
          style={{fontSize: 12, color: 'grey', marginTop: 5, marginLeft: 5}}>
          Noida Family Dentistry & TCC
        </Text>
        <Text
          style={{fontSize: 12, color: 'grey', marginTop: 5, marginLeft: 5}}>
          Block-D House No. 127-A ( Landmark : The millenium School, Block-D
          park )
        </Text>
      </View>
    </View>
  </ScrollView>
);
const SecondRoute = () => (
  <View style={[styles.scene, {backgroundColor: '#673ab7'}]} />
);

export default class HospitalDetail extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
      animations: {
        setRoot: {
          waitForRender: false,
        },
      },
    };
  };
  constructor(props) {
    super(props);
    this.state = {
      position: 1,
      interval: null,
      password: '',
      gallery: [],
      timings: [],
      name: '',
      speciality: [],
      speciality_name: '',
      speciality_id: '',
      value: -1,
      atleast: 0,
      isDone: 0,
      showExtra: 0,
      results: [],
      marker: [1],
      hospital_website: '',
      hospital_distance: '',
      activeSlide: 0,
      index: 0,
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },

      routes: [
        {key: 'first', title: 'Details'},
        {key: 'second', title: 'Rating'},
      ],
    };
  }

  getRespone = res => {
    var rece = res.specialty;
    const transformed = rece.map(({id, title}) => ({label: title, value: id}));

    this.setState({speciality: transformed});
  };

  getSpeciality = () => {
    //         var self=this;
    // store.get('specialty')
    //     .then((res) =>
    //             self.getRespone(res)

    //         // this.setState({speciality:res.specialty})
    //     )
    console.log(GLOBAL.hids);
    const url = GLOBAL.BASE_URL + 'view_all_specialty_hospital';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        hospital_id: GLOBAL.hids,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(JSON.stringify(responseJson));

        if (responseJson.status == true) {
          var rece = responseJson.specialty;
          const transformed = rece.map(({id, title}) => ({
            label: title,
            value: id,
          }));
          this.setState({speciality: transformed});
          this.setState({showExtra: 1});
        } else {
          this.setState({speciality: []});
          this.setState({showExtra: 0});
        }
      })
      .catch(error => {
        console.error(error);
        //                this.hideLoading()
      });
  };

  componentDidMount() {
    //  alert(JSON.stringify(this.state.distance));
    // console.log(JSON.stringify(responseJson.name));
    this.getSpeciality();
    this.getHospitalDetails();
  }

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  getHospitalDetails() {
    //        this.showLoading()
    const url = GLOBAL.BASE_URL + 'hospital_detail';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        id: GLOBAL.hids,
        lat: GLOBAL.lat,
        long: GLOBAL.long,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        // alert(JSON.stringify(responseJson));

        //              this.hideLoading()
        if (responseJson.status == true) {
          this.setState({
            results: responseJson,
            name: responseJson.name,
            image: responseJson.image,
            gallery: responseJson.gallery_a,
            hospital_state: responseJson.hospital_state,
            hospital_website: responseJson.hospital_website,
            hospital_distance: responseJson.distance,
          });
          var lat = parseFloat(responseJson.latitude);
          var long = parseFloat(responseJson.longitude);
          var initialRegion = {
            latitude: lat,
            longitude: long,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          };
          console.log('sadasdsdsd' + JSON.stringify(responseJson));
          this.setState({initialPosition: initialRegion});
          this.mapView.animateToRegion(initialRegion, 1);
          this.setState({timings: responseJson.time_array});

          // for (var i = 0 ; i< responseJson.time_array.length;i++){
          //     if (responseJson.time_array[i].flag == 0){
          //         var a  =  ` ${responseJson.time_array[i].start1} - ${responseJson.time_array[i].end1}`;

          //         this.setState({timings:a})
          //     }
          // }
        }
      })
      .catch(error => {
        console.error(error);
        //           this.hideLoading()
      });
  }

  renderTimes = ({item, index}) => {
    // console.log(item);
    return (
      <View
        style={{
          backgroundColor: 'white',
          color: 'white',
          flexDirection: 'row',
          margin: 5,
          flex: 1,
          height: 50,
          alignItems: 'center',
          borderBottomWidth: 1,
          borderBottomColor: '#e9eaeb',
          width: window.width / 1.4,
        }}>
        {item.flag == 0 && (
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 15,
                color: 'grey',
                fontFamily: 'Konnect-Medium',
                marginLeft: 10,
              }}>
              {item.day.toUpperCase()}
            </Text>
            <View style={{flexDirection: 'column', marginRight: 10}}>
              <Text
                style={{
                  fontSize: 15,
                  color: 'grey',
                  fontFamily: 'Konnect-Regular',
                }}>
                {item.start1} - {item.end1}
              </Text>
            </View>
          </View>
        )}

        {item.flag == 1 && (
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 15,
                color: 'green',
                fontFamily: 'Konnect-Medium',
                marginLeft: 10,
              }}>
              {item.day.toUpperCase()}
            </Text>
            <View style={{flexDirection: 'column', marginRight: 10}}>
              <Text
                style={{
                  fontSize: 15,
                  color: 'green',
                  fontFamily: 'Konnect-Regular',
                }}>
                {item.start1} - {item.end1}
              </Text>
            </View>
          </View>
        )}
      </View>
    );
  };

  getDoctorsBySpeciality = () => {
    console.log(
      '-->>' +
        JSON.stringify({
          patient_id: GLOBAL.user_id,
          lat: GLOBAL.lat,
          long: GLOBAL.long,
          search_keyword: this.state.speciality_name,
          doctor_condition: 'online',
          state: this.state.hospital_state,
          hospital_id: GLOBAL.hids,
        }),
    );
    const url = GLOBAL.BASE_URL + 'search_doctor_by_spciality_hospital';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        patient_id: GLOBAL.user_id,
        lat: GLOBAL.lat,
        long: GLOBAL.long,
        search_keyword: this.state.speciality_name,
        doctor_condition: 'online',
        state: this.state.hospital_state,
        hospital_id: GLOBAL.hids,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        // console.log(JSON.stringify(responseJson))

        if (responseJson.status == true) {
          var rece = responseJson.doctor_list_s;
          //                    const transformed = rece.map(({ id, name }) => ({ label: name, value: id }));

          this.setState({doctorlist: rece});
        } else {
          alert('No doctors found for the selected department!');
          this.setState({doctorlist: []});
          this.setState({isDone: 0});
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  };

  _handlePressViewDoc = () => {
    this.props.navigation.navigate('DoctorDetail');
  };

  getIndex = index => {
    this.setState({
      speciality_id: this.state.speciality[index].value,
      speciality_name: this.state.speciality[index].label,
    });
    //        this.setState({atleast : 1})
    //        alert(this.state.relation)
    this.getDoctorsBySpeciality();
  };

  getIndexs = index => {
    //        console.log(this.state.doctorlist)
    this.setState({
      doc_name: this.state.doctorlist[index].value,
      doc_id: this.state.doctorlist[index].label,
    });

    // GLOBAL.speciality = speciality
    // GLOBAL.appointmentArray = item

    // this.setState({isDone : 1})
    //        alert(this.state.relation)
  };

  selectDoctor = (item, indexs) => {
    //alert(JSON.stringify(item))
    var a = this.state.doctorlist;
    for (var i = 0; i < this.state.doctorlist.length; i++) {
      this.state.doctorlist[i].is_selected = 0;
    }
    var index = a[indexs];
    if (index.is_selected == 0) {
      index.is_selected = 1;
      this.setState({doc_id: item.id});
    } else {
      index.is_selected = 0;
    }
    this.state.doctorlist[indexs] = index;
    this.setState({doctorlist: this.state.doctorlist});
    this.setState({isDone: 1});
    GLOBAL.speciality = item.speciality_detail_array;
    GLOBAL.appointmentArray = item;
  };

  _renderItemsDoclist = ({item, index}) => {
    //            console.log(JSON.stringify(item))
    return (
      <TouchableOpacity
        style={{width: '100%', height: 60}}
        onPress={() => this.selectDoctor(item, index)}>
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            height: '100%',
            alignItems: 'center',
          }}>
          {item.is_selected == 0 && (
            <Image
              style={{
                width: 25,
                height: 25,
                resizeMode: 'contain',
                marginTop: 8,
                marginLeft: 10,
              }}
              source={require('./uncheck.png')}
            />
          )}
          {item.is_selected != 0 && (
            <Image
              style={{
                width: 25,
                height: 25,
                resizeMode: 'contain',
                marginTop: 8,
                marginLeft: 10,
              }}
              source={require('./check.png')}
            />
          )}

          <Image
            style={{
              width: 40,
              height: 40,
              borderRadius: 20,
              marginLeft: 10,
              marginTop: 5,
            }}
            source={{uri: item.image}}
          />

          <Text
            style={{
              fontSize: 16,
              color: 'black',
              fontFamily: 'Konnect-Regular',
              marginLeft: 10,
              marginTop: 13,
            }}>
            {item.name}
          </Text>
        </View>
        <View
          style={{
            backgroundColor: '#e1e1e1',
            width: '100%',
            height: 1,
            marginBottom: 10,
          }}></View>
      </TouchableOpacity>
    );
  };

  _renderBanners = ({item, index}) => {
    //        console.log(JSON.stringify(item))
    if (this.state.loading) {
      return (
        <View style={{width: window.width, height: 250}}>
          <ActivityIndicator
            style={{alignSelf: 'center'}}
            size="large"
            color="#800000"
          />
        </View>
      );
    }

    return (
      <View style={{width: window.width, height: 250}}>
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.3)',
            position: 'absolute',
            top: 0,
            zIndex: 1,
          }}></View>
        <Image
          style={{width: '100%', height: '100%', resizeMode: 'cover'}}
          source={{uri: item}}
        />
      </View>
    );
  };

  get pagination() {
    const {entries, activeSlide} = this.state;
    return (
      <Pagination
        dotsLength={this.state.gallery.length}
        activeDotIndex={activeSlide}
        containerStyle={{
          alignSelf: 'center',
          backgroundColor: 'transparent',
          marginTop: -45,
        }}
        dotStyle={{
          width: 20,
          height: 6,
          borderRadius: 5,
          marginHorizontal: -5,
          backgroundColor: '#800000',
        }}
        inactiveDotStyle={{
          backgroundColor: 'black',
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  }

  renderTabBar(props) {
    return (
      <TabBar
        style={{
          backgroundColor: '#FFFFFF',
          elevation: 0,
          borderColor: 'rgba(0,0,0,0.5)',
          borderBottomWidth: 2.5,
          height: 50,
        }}
        labelStyle={{
          color: 'rgba(0,0,0,0.5)',
          fontSize: 18,
          fontWeight: 'bold',
        }}
        {...props}
        indicatorStyle={{backgroundColor: '#800000', height: 2.5}}
      />
    );
  }
  _handlePress = () => {
    Linking.openURL(`tel:${this.state.results.number}`);
  };
  componentWillUnmount() {}

  getDirections = () => {
    //  alert(JSON.stringify(response.distance));
    //    console.log(this.state.initialPosition.latitude)
    Linking.openURL(
      'http://maps.google.com/maps?daddr=' +
        this.state.initialPosition.latitude +
        ',' +
        this.state.initialPosition.longitude,
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.state.gallery.length == 0 && (
          <Image
            style={{width: window.width, height: 250, resizeMode: 'cover'}}
            source={require('./treat.png')}
          />
        )}

        {this.state.gallery.length != 0 && (
          <Carousel
            ref={c => {
              this._carousel = c;
            }}
            data={this.state.gallery}
            renderItem={this._renderBanners}
            sliderWidth={window.width}
            itemWidth={window.width}
            containerCustomStyle={{
              flexGrow: 0,
            }}
            layout={'default'}
            layoutCardOffset={18}
            onSnapToItem={index => this.setState({activeSlide: index})}
          />
        )}

        {this.pagination}
        <View
          style={{
            marginLeft: 20,
            flexDirection: 'row',
            position: 'absolute',
            top: 30,
          }}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
            <Image
              style={{
                width: 20,
                height: 20,
                resizeMode: 'contain',
                marginTop: 6,
              }}
              source={require('./back.png')}
            />
          </TouchableOpacity>

          <Text
            style={{
              marginLeft: 20,
              fontSize: 22,
              width: '88%',
              color: 'white',
              fontFamily: 'Konnect-Medium',
            }}>
            {this.state.name}
          </Text>
        </View>
        <ScrollView style={[styles.container, {marginTop: -10}]}>
          <View
            style={{
              flexDirection: 'row',
              marginLeft: 10,
              marginRight: 10,
              marginTop: 5,
            }}>
            <Image
              style={{
                width: 20,
                height: 20,
                resizeMode: 'contain',
                marginTop: 3,
              }}
              source={require('./dtime.png')}
            />
            <View style={{flexDirection: 'column', marginLeft: 30}}>
              <Text
                style={{
                  fontSize: 15,
                  color: '#8BC60D',
                  fontFamily: 'Konnect-Medium',
                }}>
                AVAILABLE TIMINGS
              </Text>
              {this.state.timings.length == 0 && (
                <Text
                  style={{
                    fontSize: 15,
                    color: 'black',
                    marginTop: 5,
                    fontFamily: 'Konnect-Regular',
                  }}>
                  No time slots available!
                </Text>
              )}

              {this.state.timings.length != 0 && (
                <FlatList
                  style={{
                    elevation: 5,
                    backgroundColor: 'white',
                    marginTop: 5,
                    borderRadius: 5,
                  }}
                  data={this.state.timings}
                  renderItem={this.renderTimes}
                  keyExtractor={(item, index) => index.toString()}
                />
              )}
            </View>
          </View>

          <View style={{flexDirection: 'row', margin: 10}}>
            <Image
              style={{
                width: 20,
                height: 20,
                resizeMode: 'contain',
                marginTop: 3,
              }}
              source={require('./dhouse.png')}
            />
            <View
              style={{flexDirection: 'column', marginLeft: 30, width: '95%'}}>
              <Text
                style={{
                  fontSize: 15,
                  color: 'black',
                  width: '90%',
                  fontFamily: 'Konnect-Medium',
                }}>
                {this.state.results.name}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  color: 'grey',
                  marginBottom: 10,
                  fontFamily: 'Konnect-Regular',
                  width: '90%',
                }}
                numberOfLines={2}
                multiline={true}>
                {this.state.results.lat_long_address}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  color: 'black',
                  fontFamily: 'Konnect-Medium',
                  marginBottom: 10,
                }}
                onPress={() =>
                  Linking.openURL(`http://${this.state.hospital_website}`)
                }>
                Website :
                <Text
                  style={{
                    fontSize: 15,
                    color: 'blue',
                    fontFamily: 'Konnect-Regular',
                    marginBottom: 10,
                  }}>
                  {' ' + this.state.hospital_website}
                </Text>
              </Text>

              <Text
                style={{
                  fontSize: 15,
                  color: 'black',
                  fontFamily: 'Konnect-Medium',
                  marginBottom: 10,
                }}
                onPress={() => Linking.openURL(this.state.hospital_website)}>
                Distance :
                <Text
                  style={{
                    fontSize: 15,
                    color: 'grey',
                    fontFamily: 'Konnect-Regular',
                    marginBottom: 10,
                  }}>
                  {' ' + this.state.hospital_distance} Km
                </Text>
              </Text>

              <MapView
                clusterColor="#77869E"
                clusterTextColor="white"
                clusterBorderColor="#77869E"
                clusterBorderWidth={4}
                showsUserLocation={true}
                showsMyLocationButton={false}
                pitchEnabled={true}
                rotateEnabled={true}
                zoomEnabled={true}
                scrollEnabled={true}
                onMapReady={this._onMapReady}
                ref={ref => (this.mapView = ref)}
                region={this.state.initialPosition}
                style={{
                  width: 300,
                  height: 250,
                  marginBottom: this.state.marginBottom,
                }}>
                <Marker
                  coordinate={this.state.initialPosition}
                  title={this.state.name}
                  description={this.state.results.lat_long_address}
                />
              </MapView>
            </View>
          </View>
          <Button
            style={{
              padding: 7,
              marginTop: '2%',
              fontSize: 15,
              color: 'white',
              alignSelf: 'center',
              backgroundColor: '#800000',
              width: '40%',
              fontFamily: 'Konnect-Medium',
              borderRadius: 4,
            }}
            styleDisabled={{color: 'black', backgroundColor: 'grey'}}
            onPress={() => this.getDirections()}>
            GET DIRECTIONS
          </Button>

          <View style={{flexDirection: 'row', margin: 10}}>
            <Image
              style={{
                width: 20,
                height: 20,
                resizeMode: 'contain',
                marginTop: 3,
              }}
              source={require('./dtick.png')}
            />
            <View style={{flexDirection: 'column', marginLeft: 30}}>
              <Text
                style={{
                  fontSize: 15,
                  color: '#010101',
                  fontFamily: 'Konnect-Medium',
                }}>
                SERVICES
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  color: 'grey',
                  marginTop: 5,
                  marginLeft: 5,
                  fontFamily: 'Konnect-Regular',
                }}>
                -{this.state.results.services}
              </Text>
            </View>
          </View>

          <Button
            style={{
              padding: 8,
              marginTop: 8,
              fontSize: 20,
              color: 'white',
              backgroundColor: '#800000',
              marginLeft: '5%',
              width: '90%',
              height: 40,
              fontFamily: 'Konnect-Medium',
              borderRadius: 4,
            }}
            styleDisabled={{color: 'red'}}
            onPress={() => this._handlePress()}>
            CALL NOW
          </Button>

          {this.state.showExtra == 1 && (
            <View>
              <Text
                style={{
                  fontSize: 15,
                  color: '#010101',
                  marginLeft: 20,
                  marginTop: 20,
                  fontFamily: 'Konnect-Medium',
                }}>
                View Doctors By Department
              </Text>

              <View
                style={{
                  width: '90%',
                  alignItems: 'center',
                  alignSelf: 'center',
                  marginBottom: 20,
                }}>
                <Dropdown
                  containerStyle={{
                    width: '100%',
                    height: 50,
                    marginTop: 10,
                    alignSelf: 'center',
                    marginBottom: 30,
                  }}
                  fontSize={14}
                  fontColor={'#000000'}
                  labelFontSize={13}
                  dropdownPosition={1}
                  onChangeText={(value, index) => this.getIndex(index)}
                  label={'Select Department'}
                  data={this.state.speciality}
                />
              </View>

              <FlatList
                style={{
                  flexGrow: 0,
                  marginLeft: 8,
                  marginRight: 8,
                  marginTop: -10,
                  marginBottom: 10,
                }}
                data={this.state.doctorlist}
                numColumns={1}
                keyExtractor={(item, index) => index.toString()}
                renderItem={this._renderItemsDoclist}
                extraData={this.state}
              />

              {this.state.isDone == 0 && (
                <Button
                  style={{
                    padding: 8,
                    marginTop: 8,
                    fontSize: 20,
                    color: 'white',
                    backgroundColor: 'grey',
                    marginLeft: '5%',
                    width: '90%',
                    height: 40,
                    fontFamily: 'Konnect-Medium',
                    borderRadius: 4,
                    marginBottom: 20,
                  }}
                  styleDisabled={{color: 'white'}}
                  disabled={true}>
                  VIEW DOCTOR DETAILS
                </Button>
              )}

              {this.state.isDone == 1 && (
                <Button
                  style={{
                    padding: 8,
                    marginTop: 8,
                    fontSize: 20,
                    color: 'white',
                    backgroundColor: '#800000',
                    marginLeft: '5%',
                    width: '90%',
                    height: 40,
                    fontFamily: 'Konnect-Medium',
                    borderRadius: 4,
                    marginBottom: 20,
                  }}
                  styleDisabled={{color: 'red'}}
                  onPress={() => this._handlePressViewDoc()}>
                  VIEW DOCTOR DETAILS
                </Button>
              )}
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f7f7f7',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    flex: 1,
    backgroundColor: '#000000',
  },
});
