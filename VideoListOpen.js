import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Alert,
  TouchableOpacity,
  TextInput,
  Image,
  ImageBackground,
  Linking,
  FlatList,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import Header from './Header.js';
import React, {Component} from 'react';
import Button from 'react-native-button';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
import {WebView} from 'react-native-webview';
import getYouTubeID from 'get-youtube-id';

class VideoListOpen extends React.Component {
  state = {
    v_details: GLOBAL.vid,
  };

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
    };
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  componentDidMount() {}

  render() {
    var yeah = this.state.v_details;

    var videoid = getYouTubeID(this.state.v_details.youtube_url, {
      fuzzy: false,
    });

    var video_url = `https://www.youtube.com/embed/${videoid}?controls=1&showinfo=0`;
    console.log(video_url);

    return (
      <View style={{flex: 1, backgroundColor: 'black'}}>
        <Header navigation={this.props.navigation} headerName={'VIDEO'} />

        <ScrollView>
          <View style={{flex: 1}}>
            <WebView
              style={{
                height: window.height / 1.3,
                width: '100%',
                backgroundColor: 'black',
              }}
              source={{uri: video_url}}
            />

            <Text
              style={{
                fontSize: 16,
                margin: 10,
                fontFamily: 'Konnect-Medium',
                color: 'white',
                textAlign: 'left',
                width: '95%',
              }}>
              {yeah.title}
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default VideoListOpen;
