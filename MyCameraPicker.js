import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Image,
    Alert,
    Text,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
} from 'react-native';
const GLOBAL = require('./Global');
import { RNCamera } from 'react-native-camera';
const window = Dimensions.get('window');
import Button from 'react-native-button';

export default class MyCameraPicker extends Component {
    state = {

    };

    static navigationOptions = ({ navigation }) => {
        return {
           header: () => null,
    }
}

    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }

    componentDidMount(){
        //   this._handlePressLogin()
    }

    takePicture = async() => {
    if (this.camera) {
      const options = { quality: 0.5, base64: false , width: 500, height:500};
      const data = await this.camera.takePictureAsync(options);
      console.log(JSON.stringify(data));
      GLOBAL.cameraImage = data.uri;

      this.aftergoBack(data.uri)
    }
  };

    aftergoBack(cameraUri){
    const { navigation } = this.props;
    navigation.goBack();
    navigation.state.params.finalUploadCamera(cameraUri);
    }

    render() {

        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}

                                       size="large" color='#800000' />
                </View>
            )
        }
        return (

            <View style={{ flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',}}>
             <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={{ flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',}}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />
        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center', }}>
          <TouchableOpacity onPress={()=>this.takePicture()} style={{ flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,}}>
            <Text style={{ fontSize: 14,fontFamily:'Konnect-Regular',color:'#800000', }}> SNAP </Text>
          </TouchableOpacity>
        </View>
        </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor :'#f1f1f1',
    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,
        top: window.height/2,
        opacity: 0.5,
        justifyContent: 'center',
        alignItems: 'center'
    },
   
})