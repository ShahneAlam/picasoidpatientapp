import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Alert,
    TouchableOpacity,
    TextInput,
    Image,
    ImageBackground,
    Linking,
    FlatList,
    Dimensions,
    ActivityIndicator,
} from 'react-native';
import Header from './Header.js';
import React, {Component} from 'react';
import Button from 'react-native-button';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');


class LabHistoryDetail extends React.Component {
    state = {
        name :'',
        email:'',
        phone :'',
        company :'',
        response :[],
        responses :[],
        loading:false,results:[],
        visible:false,a_details:'',
    };


    static navigationOptions = ({ navigation }) => {
        return {
            header: () => null,
        }
    }

    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }


    componentDidMount(){



    }


    renderItem=({item}) => {
//        console.log(JSON.stringify(item))
return(

    <TouchableOpacity style={{height:190,width:150,borderRadius:6,justifyContent:'center', margin:5}} activeOpacity={0.99}
    onPress={()=>Linking.openURL(item)}>
      <Image source={{uri: item}}
        style={{width:'100%',height:'100%',alignSelf:'center'}}/>
    </TouchableOpacity>


 );
}

_keyExtractor=(item, index)=>item.key;


    confirmCancel=()=>{
        this.showLoading()
        const url = GLOBAL.BASE_URL + 'cancel_appointment'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },


            body: JSON.stringify({
                "booking_id": GLOBAL.labdetail.booking_id,


            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                this.hideLoading()
//                alert(JSON.stringify(responseJson))


                if (responseJson.status == true) {
                    alert('Appointment cancelled successfully!')
//                    this.loadAppointments()


                } else {
                    alert('Something went wrong!')
                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });

    }

    onPressCancel=(item, index)=>{
        Alert.alert(
            'Cancel Appointment',
            'Are you sure you want to cancel this appointment?',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel'
                },
                {
                    text: 'Confirm',
                    onPress: () => this.confirmCancel(item,index)
                }
            ],
            {
                cancelable: false
            }
        );
        return true;
    }




    clickResc=()=>{

        this.props.navigation.navigate('AppointmentResc')
    }

    renderItems=({item}) => {


        return(
    <TouchableOpacity style={{height:190,width:150,borderRadius:6,justifyContent:'center', margin:5}} activeOpacity={0.99}
    onPress={()=>Linking.openURL(item)}>
      <Image source={{uri: item}}
        style={{width:'100%',height:'100%',alignSelf:'center'}}/>
    </TouchableOpacity>



        );
    }


    render() {
        var yeah =  GLOBAL.labdetail
//        console.log(JSON.stringify(yeah))
        return(
        <View>
        <Header navigation={this.props.navigation}
                headerName={'LAB BOOKING DETAILS'}/>
            <ScrollView>
                <View style={{width : Dimensions.get('window').width,height: Dimensions.get('window').height, flexDirection:'column'}}>

    <View style={{backgroundColor:'white',color :'white',flexDirection:'column' , margin: 10, height:'auto',borderRadius :6,width : Dimensions.get('window').width-20, shadowColor: '#D3D3D3',
    shadowOffset: { width: 0, height: 1 },shadowOpacity: 0.6,shadowRadius: 2,elevation: 5}}>

      <View style={{flexDirection:'row',marginTop:'3%',marginLeft:15}}>
        <Image source={{uri: yeah.lab_image}}
           style={{width:70,height:70,borderRadius:35}}/>

      <View style={{flexDirection:'column',marginLeft:10,marginTop:5}}>
      <Text style={{fontSize:17,fontFamily:'Konnect-Medium',color:'#000000'}}>{yeah.lab_name}</Text>
      <View style={{flexDirection:'row',alignItems:'center',marginTop:10}}>
      <Text style={{fontSize:14,fontFamily:'Konnect-Medium',color:'#0000004D'}}>Booking Id:- </Text>
      <Text style={{fontSize:14,fontFamily:'Konnect-Medium',color:'#0000004D'}}>#{yeah.booking_id}</Text>
      </View>


      <View style={{flexDirection:'row',alignItems:'center',marginTop:10}}>
      <Text style={{fontSize:16,fontFamily:'Konnect-Medium',color:'#000000'}}>Status:- </Text>
      {yeah.status == '1' && (
      <View>
      <Text style={{fontSize:16,fontFamily:'Konnect-Medium',color:'#00ff00'}}>Completed</Text>
      <View style={{flexDirection:'row',alignItems:'center',marginTop:10}}>
      <Text style={{fontSize:14,fontFamily:'Konnect-Medium',color:'#0000004D'}}>Completed Date:- </Text>
      <Text style={{fontSize:14,fontFamily:'Konnect-Medium',color:'#0000004D'}}>{yeah.complete_date}</Text>
      </View>
      </View>
      )}
      {yeah.status == '0' && (
     <Text style={{fontSize:16,fontFamily:'Konnect-Medium',color:'red'}}>Pending</Text>
 
      )}
      </View>
      </View>

      <Text style={{fontSize:14,fontFamily:'Konnect-Medium',color:'#0000004D',position:'absolute', right:10, top:5}}>{yeah.booking_date}</Text>


  </View>

  <Text style={{fontSize:18,fontFamily:'Konnect-Medium',color:'#000000',marginTop:'10%',marginLeft:15}}>User Upload's</Text>

        <FlatList 
          data={yeah.images}
          extraData={this.state}
          horizontal={true}
          keyExtractor={this._keyExtractor}
          renderItem={this.renderItem}
           />

  <Text style={{fontSize:18,fontFamily:'Konnect-Medium',color:'#000000',marginTop:'2%',marginLeft:15}}>Lab Upload's</Text>
{yeah.resolve_images.length == 0 && (
      <Text style={{fontSize:14,fontFamily:'Konnect-Medium',color:'#0000004D',marginLeft:15, marginTop:10, marginBottom:10}}>No Report uploaded by Lab</Text>

    )}

    {yeah.resolve_images.length!=0 && (

                    <FlatList
                        data={yeah.resolve_images}
                        keyExtractor={(item, index) => index.toString() }
                        renderItem={this.renderItems}
                    />

        )}

                </View>
    </View>

            </ScrollView>
          </View>

        );
    }
}

export default LabHistoryDetail;