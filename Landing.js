import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
  Platform,
  ImageBackground,
} from 'react-native';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
import Button from 'react-native-button';
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class Landing extends Component {
  state = {
    name: '',
    email: '',
    phone: '',
    company: '',
    loading: false,
    visible: false,

    selected: false,
    data: [],
    results: [],
  };

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
      animations: {
        setRoot: {
          waitForRender: false,
        },
      },
    };
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  componentDidMount() {}

  login = () => {
    this.props.navigation.replace('Login');
  };
  loginx = () => {
    this.props.navigation.replace('Register');
    //this.props.navigation.navigate('Register');
  };
  check = () => {
    this.setState({isSecure: !this.state.isSecure});
  };

  getIndex = index => {
    this.setState({email: this.state.data[index].id});
  };
  render() {
    let {phone} = this.state;
    let {email} = this.state;
    let {name} = this.state;
    let {company} = this.state;
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <ImageBackground
          source={require('./background.png')}
          style={{width: '100%', height: '100%'}}>
          <Image
            style={{
              width: 300,
              height: 140,
              alignSelf: 'center',
              marginTop: '20%',
              resizeMode: 'contain',
            }}
            source={require('./picalogo.png')}
          />

          <Text
            style={{
              textAlign: 'center',
              width: '100%',
              color: 'black',
              fontFamily: 'Konnect-Regular',
              fontSize: 20,
              marginTop: 40,
              alignSelf: 'center',
            }}>
            Getting Started
          </Text>

          <Text
            style={{
              textAlign: 'center',
              width: '100%',
              color: '#9ba4ad',
              fontFamily: 'Konnect-Regular',
              fontSize: 16,
              marginTop: 8,
              alignSelf: 'center',
            }}>
            Making healthcare information accessible very easily.
          </Text>
          <Button
            style={{
              padding: 14,
              marginTop: 45,
              fontSize: 16,
              borderWidth: 1,
              borderColor: '#9ba4ad',
              color: '#9ba4ad',
              backgroundColor: 'transparent',
              marginLeft: '5%',
              width: '90%',
              height: 45,
              fontFamily: 'Konnect-Regular',
              borderRadius: 4,
            }}
            styleDisabled={{color: 'red'}}
            onPress={() => this.login()}>
            SIGN IN
          </Button>

          <Button
            style={{
              padding: 14,
              marginTop: 30,
              fontSize: 16,
              borderWidth: 1,
              borderColor: '#9ba4ad',
              color: '#9ba4ad',
              backgroundColor: 'transparent',
              marginLeft: '5%',
              width: '90%',
              height: 45,
              fontFamily: 'Konnect-Regular',
              borderRadius: 4,
            }}
            styleDisabled={{color: 'red'}}
            onPress={() => this.loginx()}>
            SIGN UP
          </Button>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  account: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 17,
    justifyContent: 'center',
    color: '#262628',
    fontFamily: 'Konnect-Regular',
  },
});
