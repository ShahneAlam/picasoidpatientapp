import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
const window = Dimensions.get('window');
import Button from 'react-native-button';
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};
const GLOBAL = require('./Global');
let customDatesStyles = [];

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class DoctorVisit extends Component {
  state = {
    name: '',
    email: '',
    phone: '',
    company: '',
    loading: false,
    visible: false,

    selected: false,
    data: [],
    images: [
      {
        title: 'Planned Visit',
        image: require('./male.png'),
        selected: '',
        images: require('./males.png'),
        price: ' INR 100 ',
      },
      {
        title: 'Emergency',
        image: require('./female.png'),
        selected: '',
        images: require('./females.png'),
        price: ' INR 100 ',
      },
    ],
  };
  myCallbackFunction = res => {
    this.hideLoading();
    this.setState({data: res.role});
    this.setState({loading: false});
  };
  myCallbackFunctions = res => {
    this.hideLoading();
    GLOBAL.mobile = this.state.phone;
    if (res.status == 200) {
      GLOBAL.which = '2';

      GLOBAL.userID = res.user_id.toString();
      GLOBAL.name = res.name;
      GLOBAL.mobile = res.mobile;
      AsyncStorage.setItem('mobile', res.mobile);
      AsyncStorage.setItem('userID', res.user_id);
      AsyncStorage.setItem('username', res.name);

      this.props.navigation.navigate('Otp');
    } else if (res.status == 201) {
      this.setState({visible: true});
    } else {
      alert(stringsoflanguages.unable);
    }
  };
  static navigationOptions = ({navigation}) => {
    return {
      //   header: () => null,
      title: 'SERVICES',
      headerTitleStyle: {
        textAlign: 'center',
        alignSelf: 'center',
        color: 'black',
      },
      headerStyle: {
        backgroundColor: 'white',
      },
      headerTintColor: '#800000',
      animations: {
        setRoot: {
          waitForRender: false,
        },
      },
    };
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  componentDidMount() {
    GLOBAL.module = '2';
  }

  login = () => {
    this.props.navigation.navigate('NurseTime');
  };

  check = () => {
    this.setState({isSecure: !this.state.isSecure});
  };

  selectedFirst = indexs => {
    if (indexs == 0) {
      this.props.navigation.navigate('DoctorVisitDetail');
    } else {
      this.props.navigation.navigate('Emergency');
    }
  };

  _renderItems = ({item, index}) => {
    return (
      <TouchableOpacity onPress={() => this.selectedFirst(index)}>
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
            marginLeft: 5,
            width: window.width - 10,
            backgroundColor: 'white',
            justifyContent: 'space-around',
            marginTop: 10,
            marginBottom: 10,
            height: 50,
          }}>
          <Text
            style={{
              marginLeft: 5,
              marginTop: 10,
              fontSize: 20,
              color: '#1F1F1F',
              height: 'auto',
              fontFamily: 'Poppins-Regular',
              width: window.width - 80,
            }}>
            {item.title}
          </Text>

          <Image
            style={{
              width: 22,
              height: 22,
              alignSelf: 'flex-end',
              marginRight: 34,
              marginBottom: 15,
            }}
            source={require('./service-doorstep_03.png')}
          />
        </View>
      </TouchableOpacity>
    );
  };
  render() {
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <FlatList
          style={{flexGrow: 0, margin: 8}}
          data={this.state.images}
          numColumns={1}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItems}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    backgroundColor: '#f1f1f1',
    height: window.height,
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
});
