import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  Modal,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Button from 'react-native-button';
const window = Dimensions.get('window');
import Header from './Header.js';
const GLOBAL = require('./Global');
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class ListMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      recognized: '',
      started: '',
      text: '',
      results: [],
    };
  }

  componentWillUnmount() {}

  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state;
    return {
      // headerRight:  <TouchableOpacity onPress={() =>params.handleSave()
      // }>
      //     <Text style={{color :'#800000',fontFamily:'Konnect-Regular',fontSize: 16,marginRight:10}} >

      //         ADD
      //     </Text>
      // </TouchableOpacity>,
      header: () => null,
    };
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  _handleStateChange = state => {
    this.getMembers();
  };

  getMembers = () => {
    const url = GLOBAL.BASE_URL + 'list_member';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: GLOBAL.user_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //console.log(JSON.stringify(responseJson))

        if (responseJson.status == true) {
          this.setState({results: responseJson.member_list});
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  };
  _saveDetails = () => {
    this.props.navigation.navigate('AddMember');
  };

  componentWillMount() {
    this.getMembers();
  }

  componentDidMount() {
    this.props.navigation.addListener('willFocus', this._handleStateChange);
    this.props.navigation.setParams({handleSave: this._saveDetails});
  }
  _handlePress() {
    console.log('Pressed!');

    if (this.state.atleast == 0) {
      alert('Please select atleast 1 member');
    } else {
      this.props.navigation.navigate('AddMember');
    }
  }

  login = (s, item) => {
    GLOBAL.appointmentArray = item;
    GLOBAL.speciality = s;

    this.props.navigation.navigate('BookingAppointmentDetail');
  };

  check = () => {
    this.setState({isSecure: !this.state.isSecure});
  };

  selectedFirst = item => {
    if (GLOBAL.typelist == '') {
      GLOBAL.mymember = item;
      GLOBAL.showMemDetails = 1;
      this.props.navigation.goBack();
      GLOBAL.typelist = '';
      if (GLOBAL.typelists == '') {
        //          alert('adsa')
        GLOBAL.mymembers = item;
        GLOBAL.showMemDetailss = 1;
        this.props.navigation.goBack();
        GLOBAL.typelists = '';
      }
    } else {
      GLOBAL.memDetails = item;
      this.props.navigation.navigate('ShowMember');
    }
  };

  getIndex = index => {
    this.setState({email: this.state.data[index].id});
  };

  _renderItems = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => this.selectedFirst(item)}
        activeOpacity={0.99}>
        <View
          style={{
            flex: 1,
            marginLeft: 5,
            width: window.width - 10,
            backgroundColor: 'white',
            marginTop: 10,
            marginBottom: 10,
            borderRadius: 10,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Image
            style={{
              width: 50,
              height: 50,
              marginLeft: 10,
              borderRadius: 25,
              borderColor: '#800000',
              borderWidth: 2,
            }}
            source={require('./user.png')}
          />

          <View style={{flexDirection: 'column', width: '80%', margin: 10}}>
            <Text
              style={{
                marginLeft: 5,
                fontSize: 18,
                color: '#3A3A3A',
                fontFamily: 'Konnect-Medium',
              }}>
              {item.member_name}
            </Text>

            <Text
              style={{
                marginLeft: 5,
                fontSize: 13,
                color: '#555755',
                fontFamily: 'Konnect-Medium',
              }}>
              {item.email}
            </Text>
            <Text
              style={{
                marginLeft: 5,
                fontSize: 13,
                color: '#555755',
                fontFamily: 'Konnect-Medium',
              }}>
              {item.member_mobile}
            </Text>
            <Text
              style={{
                marginLeft: 5,
                fontSize: 13,
                color: '#555755',
                fontFamily: 'Konnect-Medium',
              }}>
              {item.relation}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  render() {
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header navigation={this.props.navigation} headerName={'LIST MEMBER'} />

        {this.state.results.length == 0 && (
          <Text
            style={{
              fontSize: 13,
              marginTop: 15,
              color: 'black',
              fontFamily: 'Konnect-Medium',
              alignSelf: 'center',
              textAlign: 'center',
            }}>
            No Members added yet!{`\n`}Click `Add Member` button to add members
          </Text>
        )}

        {this.state.results.length != 0 && (
          <FlatList
            style={{flexGrow: 0, height: window.height - 160}}
            data={this.state.results}
            numColumns={1}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this._renderItems}
            extraData={this.state}
          />
        )}
        <Button
          style={{
            padding: 7,
            marginTop: 18,
            fontSize: 20,
            color: 'white',
            backgroundColor: '#800000',
            marginLeft: '5%',
            width: '90%',
            height: 40,
            fontFamily: 'Konnect-Medium',
            borderRadius: 4,
          }}
          styleDisabled={{color: 'red'}}
          onPress={() => this._handlePress()}>
          ADD MEMBER
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    flex: 1,
    backgroundColor: '#f1f1f1',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
});
