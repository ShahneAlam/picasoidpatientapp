import React, {PureComponent} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  FlatList,
  Linking,
  Modal,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Platform,
  BackHandler,
  ImageBackground,
} from 'react-native';
import store from 'react-native-simple-store';
import {
  Dialog,
  DialogContent,
  DialogComponent,
  DialogTitle,
} from 'react-native-dialog-component';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 200;
import requestCameraAndAudioPermission from './requestCameraAndAudioPermission.js';
import {withNavigationFocus} from 'react-navigation';
import Geolocation from '@react-native-community/geolocation';
import Carousel from 'react-native-banner-carousel';
import {Header} from 'react-navigation';
import Button from 'react-native-button';
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import moment from 'moment';
import {debounce} from 'throttle-debounce';
// import { Thumbnail } from 'react-native-thumbnail-video';

class Home extends PureComponent {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      phone: '',
      company: '',
      loading: false,
      visible: false,
      visibleConsult: true,
      searchText: '',
      banner: [],
      booking_type: 'chat',
      value: 0,
      modalVisible: false,
      speciality: [],
      resultstates: [],
      videos_g: [],
      articles: [],
      startConsultation: 0,

      moviesList: [
        {
          back: require('./patients.png'),
          title: 'Doctor Consultation',
          image: require('./patient.png'),
        },
        {
          back: require('./labs.png'),
          title: 'Lab Test Booking',
          image: require('./lab.png'),
        },

        {
          back: require('./ambulances.png'),
          title: 'Ambulance Booking',
          image: require('./ambulance.png'),
        },

        {
          back: require('./healthcares.png'),
          title: 'Healthcare Centres',
          image: require('./healthcare.png'),
        },

        {
          back: require('./reports.png'),
          title: 'Medical Report',
          image: require('./report.png'),
        },

        {
          back: require('./enquiry.png'),
          title: 'Treatment Enquiry',
          image: require('./enquiries.png'),
        },
      ],

      selected: false,
      data: [],
      results: [],
    };
  }
  renderPage(image, index) {
    return (
      <View key={index}>
        <Image
          style={{
            width: BannerWidth,
            height: BannerHeight,
            resizeMode: 'stretch',
          }}
          source={{uri: image}}
        />
      </View>
    );
  }

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
      animations: {
        setRoot: {
          waitForRender: false,
        },
      },
    };
  };

  joinConsult = () => {
    console.log(GLOBAL.startConsulBid);

    if (GLOBAL.startConsultation == '1') {
      const url = GLOBAL.BASE_URL + 'start_status_online_consult';

      fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },

        body: JSON.stringify({
          booking_id: GLOBAL.startConsulBid,
          what: '2',
          from: 'patient',
        }),
      })
        .then(response => response.json())
        .then(responseJson => {
          //                    GLOBAL.bookingid = item.chat_g_id;

          if (responseJson.status == true) {
            if (GLOBAL.startConsuldocBookType == 'chat') {
              this.props.navigation.navigate('Chat');
            } else {
              //                        GLOBAL.bookingid = item.chat_g_id;
              this.props.navigation.navigate('VideoCall', {
                channelName: GLOBAL.startConsulId,
                onCancel: message => {
                  this.setState({
                    visible: true,
                    message,
                  });
                },
              });
            }
          } else {
          }
        })
        .catch(error => {
          console.error(error);
          //this.hideLoading()
        });
    } else {
      if (GLOBAL.startConsuldocBookType == 'chat') {
        this.props.navigation.navigate('Chat');
      } else {
        //                        GLOBAL.bookingid = item.chat_g_id;
        this.props.navigation.navigate('VideoCall', {
          channelName: GLOBAL.startConsulId,
          onCancel: message => {
            this.setState({
              visible: true,
              message,
            });
          },
        });
      }
    }
  };

  printChange(e) {
    //    e.persist();
    debounce(500, () => {
      // console.log('value :: ','asds');
      // console.log('which :: ', 'asdas');
      // call ajax

      this.getLoadHome();
    })();
  }

  getLoadHome = () => {
    //        console.log('Mahakal')
    const url = GLOBAL.BASE_URL + 'home_patient';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        type: 'home_patient',
        user_id: GLOBAL.user_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //  console.log(JSON.stringify(responseJson))
        // alert(JSON.stringify(responseJson))
        if (responseJson.status == true) {
          //  store.update('responseJson',responseJson)
          //   this.props.navigation.replace('Home')
          GLOBAL.ambulance = responseJson.ambulence_numer;
          GLOBAL.emer = responseJson.emergency_number;

          // GLOBAL.startConsultation = responseJson.is_chat_or_video_start
          // GLOBAL.startConsulBid = responseJson.booking_id
          // GLOBAL.startConsulId = responseJson.chat_g_id
          // GLOBAL.startConsuldocName = responseJson.doctor_name
          // GLOBAL.startConsuldocBookType = responseJson.booking_type
          // GLOBAL.startConsuldocId = responseJson.doctor_id

          this.getRespone(responseJson);
          // this.setState({startConsultation : responseJson.is_chat_or_video_start,
          //     booking_type:responseJson.booking_type
          // })
          // this.setState({startConsulBid : responseJson.booking_id})
          // this.setState({startConsulId : responseJson.chat_g_id})

          // this.setState({booking_type:responseJson.booking_type })
          if (this.state.startConsultation == 0) {
          } else {
            this.dialogComponents.show();
          }
          this.dynamicHome();
        }
      })
      .catch(error => {
        console.error(error);
        // this.hideLoading()
      });
  };

  dynamicHome = () => {
    const url = GLOBAL.BASE_URL + 'home_patient_dynamics';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: GLOBAL.user_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        // console.log(JSON.stringify(responseJson))
        if (responseJson.status == true) {
          GLOBAL.startConsuldocBookType = responseJson.booking_type;
          GLOBAL.startConsultation = responseJson.is_chat_or_video_start;
          GLOBAL.startConsulBid = responseJson.booking_id;
          GLOBAL.startConsulId = responseJson.chat_g_id;
          GLOBAL.startConsuldocName = responseJson.doctor_name;
          GLOBAL.startConsuldocId = responseJson.doctor_id;

          // this.setState({startConsultation : responseJson.is_chat_or_video_start,
          //     booking_type:responseJson.booking_type
          // })
          // this.setState({startConsulBid : responseJson.booking_id})
          // this.setState({startConsulId : responseJson.chat_g_id})

          // this.setState({booking_type:responseJson.booking_type })
          if (GLOBAL.startConsultation == 0) {
          } else {
            this.dialogComponents.show();
          }
          // recursive call

          this.dynamicHome();
        }
      })
      .catch(error => {
        console.error(error);
        // this.hideLoading()
      });
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  getRespone = res => {
    // console.log(JSON.stringify(res.articles))
    this.setState({
      speciality: res.specialty,
      banner: res.banners,
      videos_g: res.videos_l,
      articles: res.articles,
    });
  };

  handleBackButton = () => {
    if (this.props.isFocused) {
      Alert.alert(
        'Confirm Exit',
        'Are you sure you want to exit?',
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'Yes',
            onPress: () => BackHandler.exitApp(),
          },
        ],
        {
          cancelable: false,
        },
      );
      return true;
    }
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  UNSAFE_componentWillMount() {
    Geolocation.getCurrentPosition(info => {
      console.log(info);
      GLOBAL.lat = info.coords.latitude;
      GLOBAL.long = info.coords.longitude;
      if (Platform.OS === 'android') {
        //Request required permissions from Android
        requestCameraAndAudioPermission().then(_ => {
          console.log('requested!');
        });
      }
    });
  }

  componentDidMount() {
    // this._interval = setInterval(() => {
    //     alert('sadsds')
    //   }, 5000);
    GLOBAL.date = moment();
    this.props.navigation.addListener('willFocus', this._handleStateChange);

    // var self=this;
    // store.get('responseJson')
    //     .then((res) =>
    //      self.getRespone(res)

    //        // this.setState({speciality:res.specialty})
    //     )
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  _handleStateChange = state => {
    this.printChange();
    this.getStates();
    this.getData();
  };

  getData = () => {
    const url = GLOBAL.BASE_URL + 'get_profile';

    //        alert(GLOBAL.user_id)
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: GLOBAL.user_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //                console.log(JSON.stringify(responseJson))
        if (responseJson.status == true) {
          var getgender = responseJson.user_detail.gender;
          GLOBAL.payGender = responseJson.user_detail.gender;
          if (getgender == 'Male') {
            GLOBAL.mygenderedit = 0;
          } else if (getgender == 'Female') {
            GLOBAL.mygenderedit = 1;
          } else if (getgender == 'Transgender') {
            GLOBAL.mygenderedit = 2;
          }
          var getmarital = responseJson.user_detail.maritial_status;
          //                 console.log(getmarital +'value')
          if (getmarital == 'Single') {
            GLOBAL.mymaritaledit = 0;
          } else {
            GLOBAL.mymaritaledit = 1;
          }

          GLOBAL.myname = responseJson.user_detail.name;
          GLOBAL.myDobfrom = responseJson.user_detail.dob;
          GLOBAL.myAddfrom = responseJson.user_detail.address;
          GLOBAL.myCityfrom = responseJson.user_detail.city;
          GLOBAL.myemail = responseJson.user_detail.email;
          GLOBAL.pica_id = responseJson.user_detail.user_unqui_id_prefix;
          GLOBAL.mypimage = responseJson.user_detail.image;
          GLOBAL.myStatefrom = responseJson.user_detail.state;
          //               GLOBAL.myStatefrom= 'Rajasthan'

          GLOBAL.myPaymentStatus = responseJson.user_detail.app_payment_status;
          //                    GLOBAL.myPaymentStatus = 0

          GLOBAL.mymobile = responseJson.user_detail.mobile;
          // this.setState({pic :responseJson.user_detail.user_unqui_id_prefix})
        } else {
          alert('No Data Found');
        }
      })
      .catch(error => {
        console.error(error);
      });
  };

  setModalVisible = (visible, get) => {
    if (typeof get !== 'undefined') {
      this.setState({text: get.state_name});
      Linking.openURL(`tel:${get.emergency_number}`);
      //  alert(JSON.stringify(get))
    }

    this.setState({modalVisible: visible});
  };

  _renderItemsstates = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => this.setModalVisible(!this.state.modalVisible, item)}>
        <Text
          style={{fontSize: 16, color: 'black', fontFamily: 'Konnect-Regular'}}>
          {item.state_name}
        </Text>
        <View
          style={{
            backgroundColor: '#e1e1e1',
            width: '100%',
            height: 1,
            marginTop: 10,
            marginBottom: 10,
          }}></View>
      </TouchableOpacity>
    );
  };

  login = () => {
    if (GLOBAL.is_typeDoctor == 'service') {
      if (this.state.value == 0) {
        GLOBAL.myonlinetype = 'online';
        this.props.navigation.navigate('BookingAppointment');
      } else {
        GLOBAL.myonlinetype = 'offline';
        GLOBAL.onlinetype = 'normal';
        this.props.navigation.navigate('OfflineBooking');
      }
    } else {
      if (this.state.value == 0) {
        GLOBAL.myonlinetype = 'online';
        this.props.navigation.navigate('SearchSpeciality');
      } else {
        GLOBAL.myonlinetype = 'offline';
        GLOBAL.onlinetype = 'normal';
        this.props.navigation.navigate('SearchSpeciality');
      }
    }
  };

  selectedFirstsd = item => {
    //        alert('SearchSpeciality')
    GLOBAL.searchSpeciality = item;
    GLOBAL.is_typeDoctor = 'speciality';
    this.dialogComponent.show();
    //        this.props.navigation.navigate('SearchSpeciality')
  };
  renderRowItem2 = itemData => {
    var index = itemData.index;

    return (
      <TouchableOpacity
        onPress={() => this.selectedFirstsd(itemData.item.title)}>
        <View
          style={{
            width: window.width / 3 - 8,
            margin: 3,
            height: 100,
            borderRadius: 10,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
          }}>
          {index % 3 == 0 && (
            <Image
              source={require('./firsts.png')}
              style={{
                width: window.width / 3 - 12,
                height: 100,
                borderRadius: 10,
              }}
            />
          )}

          {index % 3 == 1 && (
            <Image
              source={require('./seconds.png')}
              style={{
                width: window.width / 3 - 12,
                height: 100,
                borderRadius: 10,
              }}
            />
          )}

          {index % 3 == 2 && (
            <Image
              source={require('./thirds.png')}
              style={{
                width: window.width / 3 - 12,
                height: 100,
                borderRadius: 10,
              }}
            />
          )}

          <Image
            source={{uri: itemData.item.image}}
            style={{
              width: 45,
              height: 45,
              marginTop: -80,
              marginLeft: 10,
              resizeMode: 'contain',
            }}
          />

          <Text
            style={{
              fontSize: 11,
              margin: 5,
              marginLeft: 10,
              fontFamily: 'Konnect-Regular',
              color: 'white',
            }}>
            {itemData.item.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  article = itemData => {
    GLOBAL.aid = itemData.item;
    this.props.navigation.navigate('ArticleDescription');
  };

  renderRowItem3 = itemData => {
    return (
      <TouchableOpacity onPress={() => this.article(itemData)}>
        <View
          style={{
            width: window.width / 2.02 - 8,
            margin: 3,
            borderRadius: 10,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
          }}>
          <Image
            source={{uri: itemData.item.image}}
            style={{
              width: window.width / 2.02 - 12,
              height: 100,
              borderRadius: 10,
            }}
          />
          <Text
            style={{
              fontSize: 13,
              margin: 5,
              fontFamily: 'Konnect-Regular',
              color: 'black',
              height: 33,
              textAlign: 'center',
            }}
            numberOfLines={2}>
            {itemData.item.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  videoopen = (item, index) => {
    GLOBAL.vid = item;
    this.props.navigation.navigate('ViewVideo');
  };

  renderRowItem4 = ({item, index}) => {
    return (
      <TouchableOpacity onPress={() => this.videoopen(item, index)}>
        <View
          style={{
            width: window.width / 2.02 - 8,
            margin: 3,
            borderRadius: 10,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
          }}>
          <Image
            source={{uri: item.image}}
            style={{
              width: window.width / 2.02 - 12,
              height: 100,
              borderRadius: 10,
            }}
          />
          <Text
            style={{
              fontSize: 13,
              margin: 5,
              fontFamily: 'Konnect-Regular',
              color: 'black',
              height: 33,
              textAlign: 'center',
            }}
            numberOfLines={2}>
            {item.title}
          </Text>
        </View>
        <Image
          style={{
            width: 80,
            height: 60,
            resizeMode: 'contain',
            position: 'absolute',
            alignSelf: 'center',
            top: '15%',
          }}
          source={require('./ic_play.png')}
        />
      </TouchableOpacity>
    );
  };

  getStates = () => {
    const url = GLOBAL.BASE_URL + 'state_list';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        key: 'state',
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //                    alert(JSON.stringify(responseJson.list))
        if (responseJson.status == true) {
          //                        console.log('yes')
          //                         var rece = responseJson.list
          //                         const transformed = rece.map(({ id, state_name }) => ({ label: state_name, value: id }));
          //                         console.log(transformed)
          this.setState({resultstates: responseJson.list});
          // //                        arrayholder = responseJson.list
        } else {
          this.setState({resultstates: []});
        }
      })
      .catch(error => {
        console.error(error);
        //  this.hideLoading()
      });
  };

  selectedFirst = index => {
    if (index == 0) {
      GLOBAL.is_typeDoctor = 'service';
      this.dialogComponent.show();
    } else if (index == 3) {
      this.props.navigation.navigate('HospitalList');
    } else if (index == 1) {
      this.props.navigation.navigate('Labtest');
    } else if (index == 2) {
      this.setModalVisible(true);
      //            Linking.openURL(`tel:${GLOBAL.ambulance}`)
    } else if (index == 4) {
      this.props.navigation.navigate('Upload');
    } else if (index == 5) {
      this.props.navigation.navigate('Support');
    }
  };

  renderRowItem1 = itemData => {
    return (
      <TouchableOpacity onPress={() => this.selectedFirst(itemData.index)}>
        <View
          style={{
            width: window.width / 3 - 8,
            margin: 3,
            height: 100,
            borderRadius: 10,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
          }}>
          <Image
            source={itemData.item.image}
            style={{
              width: window.width / 3 - 12,
              height: 100,
              borderRadius: 10,
            }}
          />
          <Image
            source={itemData.item.back}
            style={{
              width: 45,
              height: 45,
              marginTop: -80,
              marginLeft: 10,
              resizeMode: 'contain',
            }}
          />

          <Text
            style={{
              fontSize: 11,
              margin: 5,
              marginLeft: 10,
              fontFamily: 'Konnect-Regular',
              color: 'white',
            }}>
            {itemData.item.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  setSearchText(e) {
    this.setState({
      searchText: e.target.value,
    });
  }

  cancelConsult = () => {
    this.dialogComponents.dismiss();
  };

  render() {
    var radio_props = [
      {label: 'Online Consultation', value: 0},
      {label: 'Offline Consultation', value: 1},
    ];

    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View>
        <StatusBar backgroundColor="#6d0000" />

        <View style={{flex: 1, backgroundColor: 'white'}} />
        <View
          style={{
            backgroundColor: '#800000',
            height: 54,
            width: '100%',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            style={{width: '76%'}}
            //onPress={() => this.props.navigation.openDrawer}
            onPress={() => this.props.navigation.openDrawer()}
            activeOpacity={0.99}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={{margin: 15, height: 25, width: 30}}
                source={require('./drawer.png')}
              />

              <Image
                style={{marginTop: 14, height: 27, width: 27, marginLeft: -7}}
                source={require('./homelogo.png')}
              />

              <Text
                style={{
                  fontSize: 17,
                  fontFamily: 'Konnect-Medium',
                  color: 'white',
                  marginTop: 17,
                  marginLeft: 8,
                }}>
                Home
              </Text>
            </View>
          </TouchableOpacity>

          <View style={{flexDirection: 'row', marginTop: 2}}>
            <TouchableOpacity
              onPress={() => Linking.openURL(`tel:${GLOBAL.emer}`)}>
              <Image
                style={{margin: 15, height: 20, width: 20}}
                source={require('./call.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Notification')}>
              <Image
                style={{marginTop: 14, height: 20, width: 20, marginLeft: -2}}
                source={require('./bell.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
        <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
          <Carousel
            autoplay
            autoplayTimeout={5000}
            loop
            showsPageIndicator={false}
            index={0}
            pageSize={BannerWidth}>
            {this.state.banner.map((image, index) =>
              this.renderPage(image, index),
            )}
          </Carousel>
          <TouchableOpacity
            style={{
              width: '80%',
              alignSelf: 'center',
              height: 40,
              marginTop: -20,
            }}
            activeOpacity={0.99}
            onPress={() => this.props.navigation.navigate('Search')}>
            <View
              style={{
                width: '100%',
                alignSelf: 'center',
                height: 40,
                borderRadius: 22,
                flexDirection: 'row',
                backgroundColor: 'white',
                borderColor: '#800000',
                borderWidth: 1,
                shadowColor: '#000',
                shadowOffset: {width: 0, height: 2},
                shadowOpacity: 0.8,
                shadowRadius: 2,
              }}>
              <Image
                style={{
                  height: 20,
                  width: 20,
                  marginLeft: 20,
                  marginTop: 10,
                  resizeMode: 'contain',
                }}
                source={require('./homesearch.png')}
              />
              <TextInput
                style={{width: '69%', marginLeft: 10, height: 40}}
                value={this.state.searchText}
                onChange={this.setSearchText.bind(this)}
                editable={false}
                placeholder="Search for doctor, speciality, etc"
              />
            </View>
          </TouchableOpacity>

          <Text
            style={{
              color: 'black',
              fontSize: 20,
              fontFamily: 'Konnect-Regular',
              marginLeft: 15,
              marginTop: 15,
            }}>
            Services
          </Text>

          <View
            style={{
              marginLeft: 15,
              backgroundColor: '#800000',
              height: 2,
              width: 25,
            }}></View>

          <FlatList
            style={{marginTop: 10, marginLeft: 5, width: window.width - 10}}
            data={this.state.moviesList}
            numColumns={3}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderRowItem1}
            extraData={this.state}
          />

          <View style={{flexDirection: 'row', width: '100%'}}>
            <Text
              style={{
                color: 'black',
                fontSize: 20,
                fontFamily: 'Konnect-Regular',
                marginLeft: 15,
                marginTop: 10,
                width: '70%',
              }}>
              Specialities
            </Text>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Speciality')}>
              <Text
                style={{
                  color: '#800000',
                  fontSize: 14,
                  fontFamily: 'Konnect-Regular',
                  marginLeft: 15,
                  marginTop: 15,
                  alignSelf: 'flex-end',
                  marginRight: 20,
                }}>
                View All
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              marginLeft: 15,
              backgroundColor: '#800000',
              height: 2,
              width: 25,
            }}></View>

          <FlatList
            style={{marginTop: 10, marginLeft: 5, width: window.width - 10}}
            data={this.state.speciality}
            horizontal={true}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderRowItem2}
            extraData={this.state}
          />

          <View style={{flexDirection: 'row', width: '100%'}}>
            <Text
              style={{
                color: 'black',
                fontSize: 20,
                fontFamily: 'Konnect-Regular',
                marginLeft: 15,
                marginTop: 10,
                width: '70%',
              }}>
              Videos
            </Text>

            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('VideoListing')}
              //    onPress={() => this.props.navigation.navigate('ViewVideo')}
            >
              <Text
                style={{
                  color: '#800000',
                  fontSize: 14,
                  fontFamily: 'Konnect-Regular',
                  marginLeft: 15,
                  marginTop: 15,
                  alignSelf: 'flex-end',
                  marginRight: 20,
                }}>
                View All
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              marginLeft: 15,
              backgroundColor: '#800000',
              height: 2,
              width: 25,
            }}></View>

          <FlatList
            style={{
              marginTop: 10,
              marginLeft: 5,
              width: window.width - 10,
              marginBottom: 5,
            }}
            data={this.state.videos_g}
            horizontal={true}
            // numColumns={3}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderRowItem4}
            extraData={this.state}
          />

          <View style={{flexDirection: 'row', width: '100%'}}>
            <Text
              style={{
                color: 'black',
                fontSize: 20,
                fontFamily: 'Konnect-Regular',
                marginLeft: 15,
                marginTop: 10,
                width: '70%',
              }}>
              Articles & Announcements
            </Text>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('ArticlesAndAnnouncementsList')
              }>
              <Text
                style={{
                  color: '#800000',
                  fontSize: 14,
                  fontFamily: 'Konnect-Regular',
                  marginLeft: 15,
                  marginTop: 15,
                  alignSelf: 'flex-end',
                  marginRight: 20,
                }}>
                View All
              </Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              marginLeft: 15,
              backgroundColor: '#800000',
              height: 2,
              width: 25,
            }}></View>

          <FlatList
            style={{
              marginTop: 10,
              marginLeft: 5,
              width: window.width - 10,
              marginBottom: 5,
            }}
            data={this.state.articles}
            horizontal={true}
            // numColumns={3}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderRowItem3}
            extraData={this.state}
          />

          <View style={{marginTop: 50}}></View>
        </KeyboardAwareScrollView>

        <DialogComponent
          dialogStyle={{backgroundColor: 'transparent'}}
          dialogTitle={<DialogTitle title="Dialog Title" />}
          dismissOnTouchOutside={true}
          dismissOnHardwareBackPress={true}
          ref={dialogComponent => {
            this.dialogComponent = dialogComponent;
          }}>
          <View
            style={{
              width: window.width - 30,
              alignSelf: 'center',
              backgroundColor: 'transparent',
              flexDirection: 'column',
            }}>
            <View
              style={{
                backgroundColor: '#6d0000',
                width: '100%',
                height: 50,
                justifyContent: 'space-between',
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  margin: 15,
                  color: 'white',
                  fontFamily: 'Konnect-Regular',
                  fontSize: 17,
                }}>
                Choose Booking Type
              </Text>
              <TouchableOpacity onPress={() => this.dialogComponent.dismiss()}>
                <Image
                  style={{
                    width: 25,
                    height: 25,
                    resizeMode: 'contain',
                    margin: 15,
                  }}
                  source={require('./cross.png')}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                width: window.width - 30,
                backgroundColor: 'white',
                height: 140,
              }}>
              <View
                style={{
                  marginTop: 20,
                  width: '100%',
                  marginLeft: 20,
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <RadioForm
                  radio_props={radio_props}
                  initial={0}
                  buttonColor={'#800000'}
                  buttonOuterColor={'#800000'}
                  selectedButtonColor={'#800000'}
                  animation={false}
                  buttonSize={12}
                  labelColor={'black'}
                  buttonStyle={{marginTop: 20}}
                  buttonWrapStyle={{marginTop: 20}}
                  labelStyle={{
                    fontSize: 16,
                    fontFamily: 'Konnect-Regular',
                    width: '90%',
                    marginLeft: 10,
                    paddingBottom: 10,
                  }}
                  onPress={value => {
                    this.setState({value: value});
                  }}
                />
              </View>

              <Button
                style={{
                  padding: 6,
                  marginTop: 5,
                  width: 100,
                  fontSize: 14,
                  color: 'white',
                  backgroundColor: '#800000',
                  alignSelf: 'center',
                  height: 30,
                  fontFamily: 'Konnect-Regular',
                  borderRadius: 4,
                }}
                styleDisabled={{color: 'red'}}
                onPress={() => this.login()}>
                SUBMIT
              </Button>
            </View>
          </View>
        </DialogComponent>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            //             Alert.alert('Modal has been closed.');
            this.setModalVisible(!this.state.modalVisible);
          }}>
          <TouchableOpacity
            style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              backgroundColor: 'rgba(0, 0, 0, 0.5)',
              alignItems: 'center',
              borderRadius: 8,
            }}
            activeOpacity={1}
            onPressOut={() => {
              this.setModalVisible(false);
            }}>
            <View
              style={{
                width: 300,
                backgroundColor: 'white',
                height: 240,
                borderRadius: 8,
              }}>
              <View
                style={{
                  width: '100%',
                  backgroundColor: 'white',
                  borderRadius: 8,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    backgroundColor: '#800000',
                    height: 60,
                    borderTopLeftRadius: 8,
                    borderTopRightRadius: 8,
                    borderTopLeftWidth: 1,
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    borderTopRightWidth: 1,
                    borderTopRightColor: 'transparent',
                    borderTopLeftColor: 'transparent',
                  }}>
                  <Text
                    style={{
                      fontSize: 17,
                      color: 'white',
                      fontFamily: 'Konnect-Regular',
                      margin: 10,
                    }}>
                    Select State
                  </Text>
                  <TouchableOpacity onPress={() => this.setModalVisible(false)}>
                    <Image
                      style={{
                        width: 25,
                        height: 25,
                        resizeMode: 'contain',
                        marginRight: 10,
                      }}
                      source={require('./cross.png')}
                    />
                  </TouchableOpacity>
                </View>
                <FlatList
                  style={{flexGrow: 0, margin: 8, marginBottom: 50}}
                  data={this.state.resultstates}
                  numColumns={1}
                  extraData={this.state}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={this._renderItemsstates}
                />
              </View>
            </View>
          </TouchableOpacity>
        </Modal>

        <DialogComponent
          dialogStyle={{backgroundColor: 'transparent'}}
          dismissOnTouchOutside={false}
          dismissOnHardwareBackPress={false}
          ref={dialogComponents => {
            this.dialogComponents = dialogComponents;
          }}>
          <View
            style={{
              width: '80%',
              alignSelf: 'center',
              backgroundColor: 'white',
              height: 150,
              borderRadius: 12,
            }}>
            {this.state.booking_type == 'chat' && (
              <Text
                style={{
                  color: 'black',
                  fontSize: 15,
                  alignSelf: 'center',
                  marginTop: 10,
                  padding: 10,
                  fontFamily: 'Konnect-Medium',
                }}>
                Your Chat Session has started. Please Join to get Benefit of
                this Service.
              </Text>
            )}

            {this.state.booking_type != 'chat' && (
              <Text
                style={{
                  color: 'black',
                  fontSize: 15,
                  alignSelf: 'center',
                  marginTop: 10,
                  padding: 10,
                  fontFamily: 'Konnect-Medium',
                }}>
                Your Video Session has started. Please Join to get Benefit for
                this Service.
              </Text>
            )}

            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                alignSelf: 'center',
                alignItems: 'center',
                justifyContent: 'space-between',
              }}>
              <Button
                style={{
                  padding: 6,
                  marginTop: 7,
                  fontSize: 20,
                  color: 'white',
                  backgroundColor: '#800000',
                  marginLeft: 30,
                  width: 100,
                  height: 40,
                  fontFamily: 'Konnect-Medium',
                  borderRadius: 4,
                }}
                styleDisabled={{color: 'red'}}
                onPress={() => this.joinConsult()}>
                JOIN
              </Button>

              <Button
                style={{
                  padding: 6,
                  marginTop: 7,
                  fontSize: 20,
                  color: 'white',
                  backgroundColor: '#800000',
                  marginRight: 30,
                  width: 100,
                  height: 40,
                  fontFamily: 'Konnect-Medium',
                  borderRadius: 4,
                }}
                styleDisabled={{color: 'red'}}
                onPress={() => this.cancelConsult()}>
                CANCEL
              </Button>
            </View>
          </View>
        </DialogComponent>
      </View>
    );
  }
}
export default withNavigationFocus(Home);

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,
    top: window.height / 2,
    opacity: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
