import React, {PureComponent} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  FlatList,
  Linking,
  Modal,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Platform,
  BackHandler,
  ImageBackground,
} from 'react-native';
import store from 'react-native-simple-store';
import {
  Dialog,
  DialogContent,
  DialogComponent,
  DialogTitle,
} from 'react-native-dialog-component';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 200;
import requestCameraAndAudioPermission from './requestCameraAndAudioPermission.js';
import {withNavigationFocus} from 'react-navigation';
import Geolocation from '@react-native-community/geolocation';
import Carousel from 'react-native-banner-carousel';
import Header from './Header.js';
import Button from 'react-native-button';
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import moment from 'moment';
import {debounce} from 'throttle-debounce';

class VideoListing extends React.Component {
  state = {
    name: '',
    email: '',
    phone: '',
    company: '',
    loading: false,
    visible: false,
    visibleConsult: true,
    searchText: '',
    banner: [],
    booking_type: 'chat',
    value: 0,
    modalVisible: false,
    speciality: [],
    resultstates: [],
    videos_g: [],
    articles: [],
    startConsultation: 0,
  };

  printChange(e) {
    //    e.persist();
    debounce(500, () => {
      // console.log('value :: ','asds');
      // console.log('which :: ', 'asdas');
      // call ajax

      this.getLoadHome();
    })();
  }

  getLoadHome = () => {
    //        console.log('Mahakal')
    const url = GLOBAL.BASE_URL + 'home_patient';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        type: 'home_patient',
        user_id: GLOBAL.user_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //  console.log(JSON.stringify(responseJson))
        // alert(JSON.stringify(responseJson))
        if (responseJson.status == true) {
          //  store.update('responseJson',responseJson)
          //   this.props.navigation.replace('Home')
          GLOBAL.ambulance = responseJson.ambulence_numer;
          GLOBAL.emer = responseJson.emergency_number;

          // GLOBAL.startConsultation = responseJson.is_chat_or_video_start
          // GLOBAL.startConsulBid = responseJson.booking_id
          // GLOBAL.startConsulId = responseJson.chat_g_id
          // GLOBAL.startConsuldocName = responseJson.doctor_name
          // GLOBAL.startConsuldocBookType = responseJson.booking_type
          // GLOBAL.startConsuldocId = responseJson.doctor_id

          this.getRespone(responseJson);
          // this.setState({startConsultation : responseJson.is_chat_or_video_start,
          //     booking_type:responseJson.booking_type
          // })
          // this.setState({startConsulBid : responseJson.booking_id})
          // this.setState({startConsulId : responseJson.chat_g_id})

          // this.setState({booking_type:responseJson.booking_type })
          if (this.state.startConsultation == 0) {
          } else {
            this.dialogComponents.show();
          }
          this.dynamicHome();
        }
      })
      .catch(error => {
        console.error(error);
        // this.hideLoading()
      });
  };

  dynamicHome = () => {
    const url = GLOBAL.BASE_URL + 'home_patient_dynamics';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: GLOBAL.user_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        // console.log(JSON.stringify(responseJson))
        if (responseJson.status == true) {
          GLOBAL.startConsuldocBookType = responseJson.booking_type;
          GLOBAL.startConsultation = responseJson.is_chat_or_video_start;
          GLOBAL.startConsulBid = responseJson.booking_id;
          GLOBAL.startConsulId = responseJson.chat_g_id;
          GLOBAL.startConsuldocName = responseJson.doctor_name;
          GLOBAL.startConsuldocId = responseJson.doctor_id;

          // this.setState({startConsultation : responseJson.is_chat_or_video_start,
          //     booking_type:responseJson.booking_type
          // })
          // this.setState({startConsulBid : responseJson.booking_id})
          // this.setState({startConsulId : responseJson.chat_g_id})

          // this.setState({booking_type:responseJson.booking_type })
          if (GLOBAL.startConsultation == 0) {
          } else {
            this.dialogComponents.show();
          }
          // recursive call

          this.dynamicHome();
        }
      })
      .catch(error => {
        console.error(error);
        // this.hideLoading()
      });
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  getRespone = res => {
    // console.log(JSON.stringify(res.articles))
    this.setState({
      speciality: res.specialty,
      banner: res.banners,
      videos_g: res.videos_l,
      articles: res.articles,
    });
  };

  handleBackButton = () => {
    if (this.props.isFocused) {
      Alert.alert(
        'Confirm Exit',
        'Are you sure you want to exit?',
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'Yes',
            onPress: () => BackHandler.exitApp(),
          },
        ],
        {
          cancelable: false,
        },
      );
      return true;
    }
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  // UNSAFE_componentWillMount() {
  //   Geolocation.getCurrentPosition(info => {
  //     console.log(info);
  //     GLOBAL.lat = info.coords.latitude;
  //     GLOBAL.long = info.coords.longitude;
  //     if (Platform.OS === 'android') {
  //       //Request required permissions from Android
  //       requestCameraAndAudioPermission().then(_ => {
  //         console.log('requested!');
  //       });
  //     }
  //   });
  // }

  componentDidMount() {
    // this._interval = setInterval(() => {
    //     alert('sadsds')
    //   }, 5000);
    GLOBAL.date = moment();
    this.props.navigation.addListener('willFocus', this._handleStateChange);

    // var self=this;
    // store.get('responseJson')
    //     .then((res) =>
    //      self.getRespone(res)

    //        // this.setState({speciality:res.specialty})
    //     )
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  _handleStateChange = state => {
    this.printChange();
    this.getStates();
    this.getData();
  };

  getData = () => {
    const url = GLOBAL.BASE_URL + 'get_profile';

    //        alert(GLOBAL.user_id)
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: GLOBAL.user_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //                console.log(JSON.stringify(responseJson))
        if (responseJson.status == true) {
          var getgender = responseJson.user_detail.gender;
          GLOBAL.payGender = responseJson.user_detail.gender;
          if (getgender == 'Male') {
            GLOBAL.mygenderedit = 0;
          } else if (getgender == 'Female') {
            GLOBAL.mygenderedit = 1;
          } else if (getgender == 'Transgender') {
            GLOBAL.mygenderedit = 2;
          }
          var getmarital = responseJson.user_detail.maritial_status;
          //                 console.log(getmarital +'value')
          if (getmarital == 'Single') {
            GLOBAL.mymaritaledit = 0;
          } else {
            GLOBAL.mymaritaledit = 1;
          }

          GLOBAL.myname = responseJson.user_detail.name;
          GLOBAL.myDobfrom = responseJson.user_detail.dob;
          GLOBAL.myAddfrom = responseJson.user_detail.address;
          GLOBAL.myCityfrom = responseJson.user_detail.city;
          GLOBAL.myemail = responseJson.user_detail.email;
          GLOBAL.pica_id = responseJson.user_detail.user_unqui_id_prefix;
          GLOBAL.mypimage = responseJson.user_detail.image;
          GLOBAL.myStatefrom = responseJson.user_detail.state;
          //               GLOBAL.myStatefrom= 'Rajasthan'

          GLOBAL.myPaymentStatus = responseJson.user_detail.app_payment_status;
          //                    GLOBAL.myPaymentStatus = 0

          GLOBAL.mymobile = responseJson.user_detail.mobile;
          // this.setState({pic :responseJson.user_detail.user_unqui_id_prefix})
        } else {
          alert('No Data Found');
        }
      })
      .catch(error => {
        console.error(error);
      });
  };

  getStates = () => {
    const url = GLOBAL.BASE_URL + 'state_list';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        key: 'state',
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //                    alert(JSON.stringify(responseJson.list))
        if (responseJson.status == true) {
          //                        console.log('yes')
          //                         var rece = responseJson.list
          //                         const transformed = rece.map(({ id, state_name }) => ({ label: state_name, value: id }));
          //                         console.log(transformed)
          this.setState({resultstates: responseJson.list});
          // //                        arrayholder = responseJson.list
        } else {
          this.setState({resultstates: []});
        }
      })
      .catch(error => {
        console.error(error);
        //  this.hideLoading()
      });
  };

  videoopen = (item, index) => {
    GLOBAL.vid = item;
    this.props.navigation.navigate('VideoListOpen');
  };

  renderRowItem4 = ({item, index}) => {
    return (
      <TouchableOpacity onPress={() => this.videoopen(item, index)}>
        <View
          style={{
            marginTop: 10,
            marginBottom: 10,
            flexDirection: 'column',
            width: '95%',
            borderRadius: 10,
            borderWidth: 1,
            borderColor: '#00000050',
            alignSelf: 'center',
            //  backgroundColor: 'yellow',
          }}>
          <ImageBackground
            imageStyle={{borderTopLeftRadius: 10, borderTopRightRadius: 10}}
            source={{uri: item.image}}
            style={{
              width: '100%',
              height: 200,
              resizeMode: 'contain',
            }}>
            <View
              style={{
                width: '100%',
                height: '100%',
                justifyContent: 'center',
                alignSelf: 'center',
              }}>
              <Image
                style={{
                  width: 80,
                  height: 60,

                  resizeMode: 'contain',
                  alignSelf: 'center',
                }}
                source={require('./ic_play.png')}
              />
            </View>
          </ImageBackground>

          <Text
            style={{
              marginVertical: 10,
              marginHorizontal: 10,
              fontSize: 13,
              fontFamily: 'Konnect-Regular',
              color: 'black',
              lineHeight: 18,
              fontWeight: '600',
            }}
            numberOfLines={2}>
            {item.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View>
        <Header navigation={this.props.navigation} headerName={'ALL VIDEO'} />
        <FlatList
          style={{
            marginBottom: 60,
            width: window.width,
          }}
          data={this.state.videos_g}
          horizontal={false}
          // numColumns={3}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderRowItem4}
          extraData={this.state}
        />
      </View>
    );
  }
}

export default VideoListing;

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    backgroundColor: 'white',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
});

// import React, {Component} from 'react';
// import {
//   StyleSheet,
//   Text,
//   TextInput,
//   View,
//   Image,
//   Alert,
//   FlatList,
//   Dimensions,
//   TouchableOpacity,
//   ActivityIndicator,
//   SafeAreaView,
//   AsyncStorage,
//   Platform,
//   ImageBackground,
// } from 'react-native';
// import store from 'react-native-simple-store';
// import {DialogComponent, DialogTitle} from 'react-native-dialog-component';
// import Header from './Header.js';
// import RadioForm, {
//   RadioButton,
//   RadioButtonInput,
//   RadioButtonLabel,
// } from 'react-native-simple-radio-button';
// const GLOBAL = require('./Global');
// const window = Dimensions.get('window');
// const BannerWidth = Dimensions.get('window').width;
// const BannerHeight = 200;
// const images = [];
// import Carousel from 'react-native-banner-carousel';
// import Button from 'react-native-button';
// import {TextField} from 'react-native-material-textfield-plus';
// type Props = {};

// import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

// export default class VideoListing extends Component {
//   state = {
//     videos_g: [],
//   };

//   static navigationOptions = ({navigation}) => {
//     return {
//       header: () => null,
//       animations: {
//         setRoot: {
//           waitForRender: false,
//         },
//       },
//     };
//   };

//   componentDidMount() {
//     // var self = this;
//     // store.get('specialty').then(
//     //   res => self.getRespone(res),
//     //   // this.setState({speciality:res.specialty})
//     // );
//   }

//   // login = () => {
//   //   if (this.state.value == 0) {
//   //     GLOBAL.myonlinetype = 'online';
//   //     this.props.navigation.navigate('SearchSpeciality');
//   //   } else {
//   //     GLOBAL.myonlinetype = 'offline';
//   //     GLOBAL.onlinetype = 'normal';
//   //     this.props.navigation.navigate('SearchSpeciality');
//   //   }
//   // };

//   // selectedFirstsd = item => {
//   //   this.dialogComponent.show();
//   //   GLOBAL.searchSpeciality = item;
//   //   //        this.props.navigation.navigate('SearchSpeciality')
//   // };

//   // renderRowItem2 = itemData => {
//   //   var index = itemData.index;

//   //   return (
//   //     <TouchableOpacity
//   //       onPress={() => this.selectedFirstsd(itemData.item.title)}>
//   //       <View
//   //         style={{
//   //           width: window.width / 2 - 12,
//   //           margin: 4,
//   //           height: 125,
//   //           borderRadius: 30,
//   //           shadowColor: '#000',
//   //           shadowOffset: {
//   //             width: 0,
//   //             height: 2,
//   //           },
//   //           shadowOpacity: 0.25,
//   //           shadowRadius: 3.84,
//   //         }}>
//   //         {index % 9 == 0 && (
//   //           <Image
//   //             source={require('./firsts.png')}
//   //             style={{
//   //               width: window.width / 2 - 12,
//   //               height: 125,
//   //               borderRadius: 22,
//   //             }}
//   //           />
//   //         )}

//   //         {index % 9 == 1 && (
//   //           <Image
//   //             source={require('./seconds.png')}
//   //             style={{
//   //               width: window.width / 2 - 12,
//   //               height: 125,
//   //               borderRadius: 22,
//   //             }}
//   //           />
//   //         )}

//   //         {index % 9 == 2 && (
//   //           <Image
//   //             source={require('./thirds.png')}
//   //             style={{
//   //               width: window.width / 2 - 12,
//   //               height: 125,
//   //               borderRadius: 22,
//   //             }}
//   //           />
//   //         )}

//   //         {index % 9 == 3 && (
//   //           <Image
//   //             source={require('./patient.png')}
//   //             style={{
//   //               width: window.width / 2 - 12,
//   //               height: 125,
//   //               borderRadius: 22,
//   //             }}
//   //           />
//   //         )}

//   //         {index % 9 == 4 && (
//   //           <Image
//   //             source={require('./doctor.png')}
//   //             style={{
//   //               width: window.width / 2 - 12,
//   //               height: 125,
//   //               borderRadius: 22,
//   //             }}
//   //           />
//   //         )}

//   //         {index % 9 == 5 && (
//   //           <Image
//   //             source={require('./lab.png')}
//   //             style={{
//   //               width: window.width / 2 - 12,
//   //               height: 125,
//   //               borderRadius: 22,
//   //             }}
//   //           />
//   //         )}

//   //         {index % 9 == 6 && (
//   //           <Image
//   //             source={require('./ambulance.png')}
//   //             style={{
//   //               width: window.width / 2 - 12,
//   //               height: 125,
//   //               borderRadius: 22,
//   //             }}
//   //           />
//   //         )}
//   //         {index % 9 == 7 && (
//   //           <Image
//   //             source={require('./healthcare.png')}
//   //             style={{
//   //               width: window.width / 2 - 12,
//   //               height: 125,
//   //               borderRadius: 22,
//   //             }}
//   //           />
//   //         )}
//   //         {index % 9 == 8 && (
//   //           <Image
//   //             source={require('./report.png')}
//   //             style={{
//   //               width: window.width / 2 - 12,
//   //               height: 125,
//   //               borderRadius: 22,
//   //             }}
//   //           />
//   //         )}

//   //         <Image
//   //           source={{uri: itemData.item.image}}
//   //           style={{
//   //             width: 45,
//   //             height: 45,
//   //             marginTop: -80,
//   //             marginLeft: 5,
//   //             resizeMode: 'contain',
//   //           }}
//   //         />

//   //         <Text
//   //           style={{
//   //             fontSize: 11,
//   //             margin: 5,
//   //             marginLeft: 10,
//   //             fontFamily: 'Konnect-Regular',
//   //             color: 'white',
//   //           }}>
//   //           {itemData.item.title}
//   //         </Text>
//   //       </View>
//   //     </TouchableOpacity>
//   //   );
//   // };

//   // renderRowItem3 = itemData => {
//   //   return (
//   //     <View
//   //       style={{
//   //         width: window.width / 2.5 - 8,
//   //         margin: 4,
//   //         borderRadius: 30,
//   //         shadowColor: '#000',
//   //         shadowOffset: {
//   //           width: 0,
//   //           height: 2,
//   //         },
//   //         shadowOpacity: 0.25,
//   //         shadowRadius: 3.84,
//   //       }}>
//   //       <Image
//   //         source={{uri: itemData.item.image}}
//   //         style={{
//   //           width: window.width / 2.5 - 12,
//   //           height: 100,
//   //           borderRadius: 22,
//   //         }}
//   //       />
//   //       <Text
//   //         style={{
//   //           fontSize: 13,
//   //           margin: 5,
//   //           fontFamily: 'Konnect-Regular',
//   //           color: 'black',
//   //         }}>
//   //         {itemData.item.title}
//   //       </Text>
//   //     </View>
//   //   );
//   // };

//   // selectedFirst = () => {
//   //   this.dialogComponent.show();
//   // };

//   // renderRowItem1 = itemData => {
//   //   return (
//   //     <TouchableOpacity onPress={() => this.selectedFirst(itemData.index)}>
//   //       <View
//   //         style={{
//   //           width: window.width / 3 - 8,
//   //           margin: 4,
//   //           height: 100,
//   //           borderRadius: 30,
//   //           shadowColor: '#000',
//   //           shadowOffset: {
//   //             width: 0,
//   //             height: 2,
//   //           },
//   //           shadowOpacity: 0.25,
//   //           shadowRadius: 3.84,
//   //         }}>
//   //         <Image
//   //           source={itemData.item.image}
//   //           style={{
//   //             width: window.width / 3 - 12,
//   //             height: 100,
//   //             borderRadius: 22,
//   //           }}
//   //         />
//   //         <Image
//   //           source={itemData.item.back}
//   //           style={{
//   //             width: 45,
//   //             height: 45,
//   //             marginTop: -80,
//   //             marginLeft: 5,
//   //             resizeMode: 'contain',
//   //           }}
//   //         />

//   //         <Text
//   //           style={{
//   //             fontSize: 11,
//   //             margin: 5,
//   //             fontFamily: 'Konnect-Regular',
//   //             color: 'white',
//   //           }}>
//   //           {itemData.item.title}
//   //         </Text>
//   //       </View>
//   //     </TouchableOpacity>
//   //   );
//   // };

//   // check = () => {
//   //   this.setState({isSecure: !this.state.isSecure});
//   // };

//   // getIndex = index => {
//   //   this.setState({email: this.state.data[index].id});
//   // };
//   // setSearchText(e) {
//   //   this.setState({
//   //     searchText: e.target.value,
//   //   });
//   // }

//   videoopen = (item, index) => {
//     GLOBAL.vid = item;
//     //  this.props.navigation.navigate('ViewVideo');
//   };

//   renderRowItem4 = ({item, index}) => {
//     return (
//       <TouchableOpacity onPress={() => this.videoopen(item, index)}>
//         <View
//           style={{
//             width: window.width / 2.02 - 8,
//             margin: 3,
//             borderRadius: 10,
//             shadowColor: '#000',
//             shadowOffset: {
//               width: 0,
//               height: 2,
//             },
//             shadowOpacity: 0.25,
//             shadowRadius: 3.84,
//           }}>
//           <Image
//             source={{uri: item.image}}
//             style={{
//               width: window.width / 2.02 - 12,
//               height: 100,
//               borderRadius: 10,
//             }}
//           />
//           <Text
//             style={{
//               fontSize: 13,
//               margin: 5,
//               fontFamily: 'Konnect-Regular',
//               color: 'black',
//               height: 33,
//               textAlign: 'center',
//             }}
//             numberOfLines={2}>
//             {item.title}
//           </Text>
//         </View>
//         <Image
//           style={{
//             width: 80,
//             height: 60,
//             resizeMode: 'contain',
//             position: 'absolute',
//             alignSelf: 'center',
//             top: '15%',
//           }}
//           source={require('./ic_play.png')}
//         />
//       </TouchableOpacity>
//     );
//   };

//   render() {
//     // var radio_props = [
//     //   {label: 'Online Consultation', value: 0},
//     //   {label: 'Offline Consultation', value: 1},
//     // ];

//     // if (this.state.loading) {
//     //   return (
//     //     <View style={styles.container}>
//     //       <ActivityIndicator
//     //         style={styles.loading}
//     //         size="large"
//     //         color="#800000"
//     //       />
//     //     </View>
//     //   );
//     // }
//     return (
//       <View>
//         <Header navigation={this.props.navigation} headerName={'ALL VIDEO'} />
//         <FlatList
//           style={{
//             marginTop: 10,
//             marginLeft: 5,
//             width: window.width - 10,
//             marginBottom: 5,
//           }}
//           data={this.state.videos_g}
//           horizontal={false}
//           // numColumns={3}
//           keyExtractor={(item, index) => index.toString()}
//           renderItem={this.renderRowItem4}
//           extraData={this.state}
//         />
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   wrapper: {},
//   container: {
//     backgroundColor: 'white',
//   },
//   loading: {
//     position: 'absolute',
//     left: window.width / 2 - 30,

//     top: window.height / 2,

//     opacity: 0.5,

//     justifyContent: 'center',
//     alignItems: 'center',
//   },
// });
