import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
  Platform,
  ImageBackground,
} from 'react-native';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
import Button from 'react-native-button';

import {TextField} from 'react-native-material-textfield-plus';
//import {Dropdown} from 'react-native-material-dropdown-v2-fixed';
import {Dropdown} from 'react-native-material-dropdown';
type Props = {};

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class ZZZZ extends Component {
  state = {
    name: '',
    email: '',
    phone: '',
    company: '',
    loading: false,
    visible: false,
    elevationone: 0,
    elevationtwo: 0,
    selected: false,
    data: [],
    results: [],
    data: [
      {
        value: 'Banana',
      },
      {
        value: 'Mango',
      },
      {
        value: 'Pear',
      },
    ],
  };

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
      animations: {
        setRoot: {
          waitForRender: false,
        },
      },
    };
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  componentDidMount() {
    var valuesf = AsyncStorage.getItem('token');
    valuesf.then(f => {
      GLOBAL.firebaseToken = f;
    });
    // alert(GLOBAL.firebaseToken)
  }

  render() {
    let {phone} = this.state;
    let {email} = this.state;
    let {name} = this.state;
    let {company} = this.state;
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
          <ImageBackground
            source={require('./background.png')}
            style={{width: '100%', height: '100%'}}>
            <Image
              style={{
                width: 300,
                height: 140,
                alignSelf: 'center',
                marginTop: '10%',
                resizeMode: 'contain',
              }}
              source={require('./picalogo.png')}
            />
            {/*                            <View style = {{backgroundColor:'#c6c6c6',height:1,marginLeft:'5%',width:'90%',marginTop:0}}>

                            </View>*/}
            <TouchableOpacity
              style={{alignSelf: 'flex-end', marginRight: 38, marginTop: 20}}
              //onPress={() => this.props.navigation.navigate('Forgot')}
              onPress={() => this.props.navigation.replace('ZZZZ')}>
              <Text style={styles.createaccount}>
                <Text style={styles.account}>Forgot Password?</Text>
              </Text>
            </TouchableOpacity>

            <View
              style={{
                width: '100%',
                backgroundColor: '#000',
                justifyContent: 'center',
                alignSelf: 'center',
              }}>
              {/* <Dropdown
                icon="chevron-down"
                iconColor="#E1E1E1"
                label="Favorite Fruit"
                data={this.state.data}
              /> */}

              {/* <Dropdown
                containerStyle={{
                  width: '100%',
                  alignSelf: 'center',
                  height: 70,
                }}
                icon={require('./dropdownimg.png')}
                labelFontSize={18}
                label="Schedule Time"
                underlineColor={{borderBottomColor: 'white'}}
                data={this.state.data}
              /> */}
            </View>
          </ImageBackground>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  account: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 17,
    justifyContent: 'center',
    color: '#c6c6c6',
    fontFamily: 'Konnect-Regular',
  },
  createaccount: {
    marginLeft: 5,
    fontSize: 17,
    textAlign: 'center',
    color: '#800000',
  },
  createaccounts: {
    marginLeft: 5,
    fontSize: 17,
    textAlign: 'center',
    marginTop: 30,
    color: '#800000',
    textDecorationLine: 'underline',
    fontFamily: 'Konnect-Regular',
  },
});

// import {
//   SafeAreaView,
//   Platform,
//   StyleSheet,
//   ScrollView,
//   View,
//   Text,
//   StatusBar,
//   Alert,
//   TouchableOpacity,
//   TextInput,
//   Image,
//   ImageBackground,
//   Linking,
//   FlatList,
//   Dimensions,
//   AsyncStorage,
//   PermissionsAndroid,
//   NativeModules,
//   BackHandler,
// } from 'react-native';

// import React, {Component} from 'react';
// import {SafeAreaProvider} from 'react-native-safe-area-context';
// import {Dropdown} from 'react-native-material-dropdown-v2-fixed';

// class ZZZZ extends React.Component {
//   state = {
//     data: [
//       {
//         value: '1 Week',
//       },
//       {
//         value: '1 Month',
//       },
//       {
//         value: 'Other',
//       },
//     ],
//   };
//   render() {
//     return (
//       <SafeAreaProvider style={{backgroundColor: 'white'}}>
//         <StatusBar backgroundColor="#F97012" />

//         <View
//           style={{
//             width: '100%',
//             height: 60,
//             elevation: 2,
//             backgroundColor: '#F97012',
//             justifyContent: 'center',
//           }}>
//           <View
//             style={{
//               width: '90%',
//               alignSelf: 'center',
//               flexDirection: 'row',
//               alignItems: 'center',
//             }}>
//             <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
//               <Image
//                 style={{height: 22, width: 12, resizeMode: 'contain'}}
//                 // source={require('./backwhite.png')}
//               />
//             </TouchableOpacity>

//             <Text
//               style={{
//                 fontFamily: 'Avenir',
//                 fontSize: 20,
//                 fontFamily: 'Avenir',
//                 color: 'white',
//                 marginLeft: 20,
//               }}>
//               Profile
//             </Text>
//           </View>
//         </View>

//         <View
//           style={{
//             width: '100%',
//             justifyContent: 'center',
//             alignSelf: 'center',
//           }}>
//           <Dropdown
//             containerStyle={{
//               width: '100%',
//               alignSelf: 'center',
//               height: 70,
//             }}
//             icon={require('./dropdownimg.png')}
//             labelFontSize={18}
//             label="Schedule Time"
//             underlineColor={{borderBottomColor: 'white'}}
//             data={this.state.data}
//           />
//         </View>
//       </SafeAreaProvider>
//     );
//   }
// }

// export default ZZZZ;
