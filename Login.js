import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
  Platform,
  ImageBackground,
} from 'react-native';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
import AsyncStorage from '@react-native-async-storage/async-storage';
import Button from 'react-native-button';
//var DeviceInfo = require('react-native-device-info');
import DeviceInfo from 'react-native-device-info';
import PasswordInputText from 'react-native-hide-show-password-input';

import {TextField} from 'react-native-material-textfield-plus';
type Props = {};

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class Login extends Component {
  state = {
    name: '',
    email: '',
    phone: '',
    company: '',
    loading: false,
    visible: false,
    elevationone: 0,
    elevationtwo: 0,
    selected: false,
    data: [],
    results: [],
  };

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
      animations: {
        setRoot: {
          waitForRender: false,
        },
      },
    };
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  componentDidMount() {
    // alert(JSON.stringify(GLOBAL.firebaseToken));
    var valuesf = AsyncStorage.getItem('token');
    valuesf.then(f => {
      GLOBAL.firebaseToken = f;
    });
    //alert(GLOBAL.firebaseToken);
  }

  login = () => {
    if (this.state.phone == '') {
      alert('Please Enter Username');
    } else if (this.state.company == '') {
      alert('Please Enter Password');
    } else {
      this.showLoading();
      const url = GLOBAL.BASE_URL + 'Signin';
      this.showLoading();

      console.log(
        JSON.stringify({
          phone: this.state.phone,
          password: this.state.company,
          deviceID: DeviceInfo.getUniqueId(),
          deviceType: Platform.OS,
          deviceToken: GLOBAL.firebaseToken,
          model_name: '',
          carrier_name: '',
          device_country: '',
          device_memory: '',
          has_notch: '',
          manufacture: '',
          ip_address: '',
        }),
      );
      fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          phone: this.state.phone,
          password: this.state.company,
          deviceID: DeviceInfo.getUniqueId(),
          deviceType: Platform.OS,
          deviceToken: GLOBAL.firebaseToken,
          model_name: '',
          carrier_name: '',
          device_country: '',
          device_memory: '',
          has_notch: '',
          manufacture: '',
          ip_address: '',
        }),
      })
        .then(response => response.json())
        .then(responseJson => {
          //                    alert(JSON.stringify(responseJson.user_detail))

          this.hideLoading();
          if (responseJson.status == true) {
            this.setState({results: responseJson.user_detail});
            GLOBAL.user_id = this.state.results.user_id;
            AsyncStorage.setItem('userID', this.state.results.user_id);
            AsyncStorage.setItem('image', this.state.results.image);
            AsyncStorage.setItem('name', this.state.results.name);
            AsyncStorage.setItem('email', this.state.results.email);
            AsyncStorage.setItem('mobile', this.state.results.mobile);
            this.props.navigation.replace('DrawerNavigator');
          } else {
            alert('Invalid Credentials!');
          }
        })
        .catch(error => {
          console.error(error);
        });
    }
  };

  check = () => {
    this.setState({isSecure: !this.state.isSecure});
  };

  onBlur = () => {
    this.setState({elevationone: 0});
  };

  onFocus = () => {
    this.setState({elevationone: 15});
  };

  onBlurs = () => {
    this.setState({elevationtwo: 0});
  };

  onFocuss = () => {
    this.setState({elevationtwo: 15});
  };

  render() {
    let {phone} = this.state;
    let {email} = this.state;
    let {name} = this.state;
    let {company} = this.state;
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView keyboardShouldPersistTaps="handled">
          <ImageBackground
            source={require('./background.png')}
            style={{width: '100%', height: '100%'}}>
            <Image
              style={{
                width: 300,
                height: 140,
                alignSelf: 'center',
                marginTop: '10%',
                resizeMode: 'contain',
              }}
              source={require('./picalogo.png')}
            />

            <Text
              style={{
                marginLeft: 30,
                textAlign: 'left',
                width: '100%',
                color: 'black',
                fontFamily: 'Konnect-Medium',
                fontSize: 40,
                marginTop: 40,
              }}>
              Login
            </Text>

            <Text
              style={{
                marginLeft: 30,
                textAlign: 'left',
                width: '90%',
                color: '#c6c6c6',
                fontFamily: 'Konnect-Regular',
                fontSize: 19,
                marginTop: 5,
              }}>
              Please sign in to continue with your personal PiCaSoid Health
              Account
            </Text>

            <View
              style={{
                marginLeft: '5%',
                width: '89%',
                marginTop: '8%',
                flexDirection: 'row',
                elevation: this.state.elevationone,
                backgroundColor: 'white',
                borderRadius: 5,
                borderWidth: 1,
                borderColor: 'transparent',
              }}>
              <Image
                style={{
                  width: 25,
                  height: 25,
                  resizeMode: 'contain',
                  marginTop: 30,
                  marginLeft: 10,
                }}
                source={require('./username.png')}
              />

              <View style={{width: '85%', marginLeft: '2%'}}>
                <TextField
                  label="USERNAME (PiCaSoid ID / Mobile / Email)"
                  value={phone}
                  fontSize={15}
                  onBlur={() => this.onBlur()}
                  onFocus={() => this.onFocus()}
                  lineWidth={1}
                  activeLineWidth={1}
                  onChangeText={phone => this.setState({phone})}
                  tintColor={'#800000'}
                />
              </View>
            </View>

            <View
              style={{
                marginLeft: '5%',
                width: '89%',
                marginTop: '3%',
                flexDirection: 'row',
                elevation: this.state.elevationtwo,
                backgroundColor: 'white',
                borderRadius: 5,
                borderWidth: 1,
                borderColor: 'transparent',
              }}>
              <Image
                style={{
                  width: 25,
                  height: 25,
                  resizeMode: 'contain',
                  marginTop: 30,
                  marginLeft: 10,
                }}
                source={require('./password.png')}
              />

              <View style={{width: '85%', marginLeft: '2%'}}>
                <PasswordInputText
                  getRef={input => (this.input = input)}
                  label="PASSWORD"
                  value={company}
                  secureTextEntry={true}
                  onChangeText={company => this.setState({company})}
                  tintColor={'#800000'}
                  onBlur={() => this.onBlurs()}
                  onFocus={() => this.onFocuss()}
                />

                <Button title="Clear" onPress={() => this.input.clear()} />

                {/* 
                <TextField
                  label="PASSWORD"
                  value={company}
                  fontSize={15}
                  onBlur={() => this.onBlurs()}
                  onFocus={() => this.onFocuss()}
                  lineWidth={1}
                  activeLineWidth={1}
                  secureTextEntry={true}
                  onChangeText={company => this.setState({company})}
                  tintColor={'#800000'}
                /> */}
              </View>
            </View>

            {/*                            <View style = {{backgroundColor:'#c6c6c6',height:1,marginLeft:'5%',width:'90%',marginTop:0}}>

                            </View>*/}

            <TouchableOpacity
              style={{alignSelf: 'flex-end', marginRight: 38, marginTop: 20}}
              onPress={() => this.props.navigation.navigate('Forgot')}
              // onPress={() => this.props.navigation.replace('ZZZZ')}
            >
              <Text style={styles.createaccount}>
                <Text style={styles.account}>Forgot Password?</Text>
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.login()}
              style={{marginTop: 40}}>
              <View
                style={{
                  backgroundColor: '#800000',
                  height: 55,
                  borderRadius: 27.5,
                  alignSelf: 'center',
                  width: 300,
                  borderBottomWidth: 0,
                  shadowColor: 'black',
                  shadowOffset: {width: 0, height: 2},
                  shadowOpacity: 0.8,
                  shadowRadius: 2,
                  flexDirection: 'row',
                }}>
                <Text
                  style={{
                    width: '100%',
                    alignSelf: 'center',
                    textAlign: 'center',
                    fontSize: 20,
                    fontFamily: 'Konnect-Medium',
                    color: 'white',
                    padding: 11,
                  }}>
                  SUBMIT
                </Text>

                <Image
                  style={{
                    width: 25,
                    height: 25,
                    resizeMode: 'contain',
                    marginLeft: -50,
                    alignSelf: 'center',
                  }}
                  source={require('./right.png')}
                />
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={{marginTop: 30}}
              onPress={() => this.props.navigation.replace('Register')}>
              <Text style={styles.createaccount}>
                <Text style={styles.account}>Don't have an account?</Text>
                <Text style={styles.createaccounts}>&nbsp;Sign Up</Text>
              </Text>
            </TouchableOpacity>
          </ImageBackground>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  account: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 17,
    justifyContent: 'center',
    color: '#c6c6c6',
    fontFamily: 'Konnect-Regular',
  },
  createaccount: {
    marginLeft: 5,
    fontSize: 17,
    textAlign: 'center',
    color: '#800000',
  },
  createaccounts: {
    marginLeft: 5,
    fontSize: 17,
    textAlign: 'center',
    marginTop: 30,
    color: '#800000',
    textDecorationLine: 'underline',
    fontFamily: 'Konnect-Regular',
  },
});
