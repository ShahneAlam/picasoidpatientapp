import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  Modal,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Button from 'react-native-button';
import Header from './Header.js';
const window = Dimensions.get('window');
import store from 'react-native-simple-store';
var arrayholder = [];
const GLOBAL = require('./Global');
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};
const {width, height} = Dimensions.get('window');
import Geolocation from '@react-native-community/geolocation';

import MapView, {PROVIDER_GOOGLE} from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import {Marker} from 'react-native-maps';

const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let customDatesStyles = [];
import SlidingUpPanel from 'rn-sliding-up-panel';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Global from './Global';

export default class Labtest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      recognized: '',
      started: '',
      text: '',
      department: [],
      speciality: [],
      hospital: [],
      price: [],
      results: [],
      markers: [],
      marginBottom: 1,
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0,
        visiblity: false,
      },
    };
  }

  componentWillUnmount() {}

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
    };
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  _onMapReady = () => this.setState({marginBottom: 0});

  _handleStateChange = state => {
    Geolocation.getCurrentPosition(
      position => {
        var lat = parseFloat(position.coords.latitude);
        var long = parseFloat(position.coords.longitude);

        var initialRegion = {
          latitude: lat,
          longitude: long,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        };

        this.setState({initialPosition: initialRegion});
      },
      //  (error) => alert(JSON.stringify(error)),
      {enableHighAccuracy: false, timeout: 20000},
    );

    const url = GLOBAL.BASE_URL + 'lab_list';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        user_id: GLOBAL.user_id,
        lat: GLOBAL.lat,
        long: GLOBAL.long,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status == true) {
          this.setState({results: responseJson.list});

          //   alert(JSON.stringify(this.state.results));
          var rece = responseJson.list;
          // var allmarkers = responseJson.list.length
          // var markarray = [allmarkers]

          this.setState({markers: responseJson.list});
          //                       alert(this.state.markarray)

          // const transformed = rece.map(({ id, state_name }) => ({ label: state_name, value: id }));
          // console.log(transformed)

          arrayholder = responseJson.list;
        } else {
          this.setState({results: []});
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  };

  componentDidMount() {
    // alert(JSON.stringify(GLOBAL.user_id));
    // alert(JSON.stringify(this.state.results));
    this.props.navigation.addListener('willFocus', this._handleStateChange);
  }

  selectedFirst = itemData => {
    GLOBAL.labid = itemData.item.id;
    //
    // GLOBAL.lab = itemData.item
    this.props.navigation.navigate('Pharmacy');
  };

  getIndex = index => {
    this.setState({email: this.state.data[index].id});
  };

  _renderItems = itemData => {
    var item = itemData.item;
    return (
      <TouchableOpacity
        onPress={() => this.selectedFirst(itemData)}
        activeOpacity={0.9}>
        <View
          style={{
            flex: 1,
            marginLeft: 5,
            width: window.width - 20,
            backgroundColor: 'white',
            marginTop: 10,
            marginBottom: 10,
            borderRadius: 10,
            elevation: 5,
          }}>
          <View style={{flexDirection: 'row', width: '100%'}}>
            <Image
              style={{width: 60, height: 60, borderRadius: 30, margin: 10}}
              source={{uri: item.display_image}}
            />

            <View style={{width: window.width - 120}}>
              <Text
                style={{
                  fontFamily: 'Konnect-Medium',
                  color: 'black',
                  fontSize: 15,
                  marginLeft: 4,
                  marginTop: 5,
                }}>
                {item.name}
              </Text>

              <View style={{flexDirection: 'row'}}>
                <Image
                  style={{width: 20, height: 20, resizeMode: 'contain'}}
                  source={require('./location.png')}
                />

                <Text
                  style={{
                    marginLeft: 5,
                    height: 'auto',
                    fontSize: 12,
                    color: '#8F8F8F',
                    fontFamily: 'Konnect-Regular',
                  }}>
                  {item.lat_long_address}
                </Text>
              </View>

              <Text
                style={{
                  fontFamily: 'Konnect-Regular',
                  color: 'black',
                  fontSize: 13,
                  marginLeft: 4,
                  marginTop: 1,
                  marginBottom: 5,
                }}>
                {item.mobile}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  SearchFilterFunction(text) {
    const newData = arrayholder.filter(function (item) {
      const itemData = item.test_name.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      results: newData,
      text: text,
    });
  }

  onMarkerClick = item => {
    //        alert(JSON.stringify(item.id))
    GLOBAL.markid = item.id;
    if (item.id < this.state.results.length && this.flatListRef) {
      this.flatList_Ref.scrollToIndex({animated: true, index: item.id});
    }
  };

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Header
          navigation={this.props.navigation}
          headerName={'LABS NEAR YOU'}
        />

        {/*                    <View style = {{margin :10,width:window.width - 20 ,height:45,borderRadius:20,flexDirection:'row',backgroundColor:'white',}}>

                        <Image style = {{width :18 ,height: 18,alignSelf:'center',resizeMode: 'contain',marginLeft:13}}
                               source={require('./search.png')}/>

                        <TextInput style={{marginLeft:10 ,width:window.width - 100, height:45}}
                                   placeholderTextColor='rgba(0, 0, 0, 0.4)'
                                   onChangeText={(text) => this.SearchFilterFunction(text)}
                                   placeholder={"Search"}/>

                    </View>
*/}

        {/* <Image style = {{width :18 ,height: 18,alignSelf:'center',resizeMode: 'contain',marginLeft:13}}
                               source={require('./speech.png')}/>
*/}

        {this.state.results !== '' &&
          this.state.results == '' &&
          this.state.visiblity == false && (
            <MapView
              provider={PROVIDER_GOOGLE} // remove if not using Google Maps
              style={{
                width: width,
                height: height,
                marginBottom: this.state.marginBottom,
              }}
              region={this.state.initialPosition}
              showsMyLocationButton={true}
              showsUserLocation={true}
              onMapReady={this._onMapReady}>
              {this.state.markers.map((marker, id) => (
                <Marker
                  coordinate={marker.lat_long_array}
                  title={marker.name}
                  description={marker.address}
                  onPress={() => this.onMarkerClick(marker)}
                />
              ))}
            </MapView>
          )}

        <FlatList
          style={{
            flexGrow: 0,
            margin: 8,
            position: 'absolute',
            bottom: 0,
            height: '30%',
          }}
          data={this.state.results}
          numColumns={1}
          ref={ref => {
            this.flatList_Ref = ref;
          }}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this._renderItems}
          extraData={this.state}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    flex: 1,
    backgroundColor: '#f1f1f1',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
});
