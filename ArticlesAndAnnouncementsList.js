import React, {PureComponent} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  FlatList,
  Linking,
  Modal,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
  StatusBar,
  Platform,
  BackHandler,
  ImageBackground,
} from 'react-native';
import store from 'react-native-simple-store';
import {
  Dialog,
  DialogContent,
  DialogComponent,
  DialogTitle,
} from 'react-native-dialog-component';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 200;
import requestCameraAndAudioPermission from './requestCameraAndAudioPermission.js';
import {withNavigationFocus} from 'react-navigation';
import Geolocation from '@react-native-community/geolocation';
import Carousel from 'react-native-banner-carousel';
import Header from './Header.js';
import Button from 'react-native-button';
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import moment from 'moment';
import {debounce} from 'throttle-debounce';

class ArticlesAndAnnouncementsList extends React.Component {
  state = {
    name: '',
    email: '',
    phone: '',
    company: '',
    loading: false,
    visible: false,
    visibleConsult: true,
    searchText: '',
    banner: [],
    booking_type: 'chat',
    value: 0,
    modalVisible: false,
    speciality: [],
    resultstates: [],
    videos_g: [],
    articles: [],
    startConsultation: 0,
  };

  printChange(e) {
    //    e.persist();
    debounce(500, () => {
      // console.log('value :: ','asds');
      // console.log('which :: ', 'asdas');
      // call ajax

      this.getLoadHome();
    })();
  }

  getLoadHome = () => {
    //        console.log('Mahakal')
    const url = GLOBAL.BASE_URL + 'home_patient';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        type: 'home_patient',
        user_id: GLOBAL.user_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //  console.log(JSON.stringify(responseJson))
        // alert(JSON.stringify(responseJson))
        if (responseJson.status == true) {
          //  store.update('responseJson',responseJson)
          //   this.props.navigation.replace('Home')
          GLOBAL.ambulance = responseJson.ambulence_numer;
          GLOBAL.emer = responseJson.emergency_number;

          // GLOBAL.startConsultation = responseJson.is_chat_or_video_start
          // GLOBAL.startConsulBid = responseJson.booking_id
          // GLOBAL.startConsulId = responseJson.chat_g_id
          // GLOBAL.startConsuldocName = responseJson.doctor_name
          // GLOBAL.startConsuldocBookType = responseJson.booking_type
          // GLOBAL.startConsuldocId = responseJson.doctor_id

          this.getRespone(responseJson);
          // this.setState({startConsultation : responseJson.is_chat_or_video_start,
          //     booking_type:responseJson.booking_type
          // })
          // this.setState({startConsulBid : responseJson.booking_id})
          // this.setState({startConsulId : responseJson.chat_g_id})

          // this.setState({booking_type:responseJson.booking_type })
          if (this.state.startConsultation == 0) {
          } else {
            this.dialogComponents.show();
          }
          this.dynamicHome();
        }
      })
      .catch(error => {
        console.error(error);
        // this.hideLoading()
      });
  };

  dynamicHome = () => {
    const url = GLOBAL.BASE_URL + 'home_patient_dynamics';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: GLOBAL.user_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        // console.log(JSON.stringify(responseJson))
        if (responseJson.status == true) {
          GLOBAL.startConsuldocBookType = responseJson.booking_type;
          GLOBAL.startConsultation = responseJson.is_chat_or_video_start;
          GLOBAL.startConsulBid = responseJson.booking_id;
          GLOBAL.startConsulId = responseJson.chat_g_id;
          GLOBAL.startConsuldocName = responseJson.doctor_name;
          GLOBAL.startConsuldocId = responseJson.doctor_id;

          // this.setState({startConsultation : responseJson.is_chat_or_video_start,
          //     booking_type:responseJson.booking_type
          // })
          // this.setState({startConsulBid : responseJson.booking_id})
          // this.setState({startConsulId : responseJson.chat_g_id})

          // this.setState({booking_type:responseJson.booking_type })
          if (GLOBAL.startConsultation == 0) {
          } else {
            this.dialogComponents.show();
          }
          // recursive call

          this.dynamicHome();
        }
      })
      .catch(error => {
        console.error(error);
        // this.hideLoading()
      });
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  getRespone = res => {
    // console.log(JSON.stringify(res.articles))
    this.setState({
      speciality: res.specialty,
      banner: res.banners,
      videos_g: res.videos_l,
      articles: res.articles,
    });
  };

  handleBackButton = () => {
    if (this.props.isFocused) {
      Alert.alert(
        'Confirm Exit',
        'Are you sure you want to exit?',
        [
          {
            text: 'No',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'Yes',
            onPress: () => BackHandler.exitApp(),
          },
        ],
        {
          cancelable: false,
        },
      );
      return true;
    }
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  UNSAFE_componentWillMount() {
    // Geolocation.getCurrentPosition(info => {
    //   console.log(info);
    //   GLOBAL.lat = info.coords.latitude;
    //   GLOBAL.long = info.coords.longitude;
    //   if (Platform.OS === 'android') {
    //     //Request required permissions from Android
    //     requestCameraAndAudioPermission().then(_ => {
    //       console.log('requested!');
    //     });
    //   }
    // });
  }

  componentDidMount() {
    // this._interval = setInterval(() => {
    //     alert('sadsds')
    //   }, 5000);
    GLOBAL.date = moment();
    this.props.navigation.addListener('willFocus', this._handleStateChange);

    // var self=this;
    // store.get('responseJson')
    //     .then((res) =>
    //      self.getRespone(res)

    //        // this.setState({speciality:res.specialty})
    //     )
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  _handleStateChange = state => {
    this.printChange();
    this.getStates();
    this.getData();
  };

  getData = () => {
    const url = GLOBAL.BASE_URL + 'get_profile';

    //        alert(GLOBAL.user_id)
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user_id: GLOBAL.user_id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //                console.log(JSON.stringify(responseJson))
        if (responseJson.status == true) {
          var getgender = responseJson.user_detail.gender;
          GLOBAL.payGender = responseJson.user_detail.gender;
          if (getgender == 'Male') {
            GLOBAL.mygenderedit = 0;
          } else if (getgender == 'Female') {
            GLOBAL.mygenderedit = 1;
          } else if (getgender == 'Transgender') {
            GLOBAL.mygenderedit = 2;
          }
          var getmarital = responseJson.user_detail.maritial_status;
          //                 console.log(getmarital +'value')
          if (getmarital == 'Single') {
            GLOBAL.mymaritaledit = 0;
          } else {
            GLOBAL.mymaritaledit = 1;
          }

          GLOBAL.myname = responseJson.user_detail.name;
          GLOBAL.myDobfrom = responseJson.user_detail.dob;
          GLOBAL.myAddfrom = responseJson.user_detail.address;
          GLOBAL.myCityfrom = responseJson.user_detail.city;
          GLOBAL.myemail = responseJson.user_detail.email;
          GLOBAL.pica_id = responseJson.user_detail.user_unqui_id_prefix;
          GLOBAL.mypimage = responseJson.user_detail.image;
          GLOBAL.myStatefrom = responseJson.user_detail.state;
          //               GLOBAL.myStatefrom= 'Rajasthan'

          GLOBAL.myPaymentStatus = responseJson.user_detail.app_payment_status;
          //                    GLOBAL.myPaymentStatus = 0

          GLOBAL.mymobile = responseJson.user_detail.mobile;
          // this.setState({pic :responseJson.user_detail.user_unqui_id_prefix})
        } else {
          alert('No Data Found');
        }
      })
      .catch(error => {
        console.error(error);
      });
  };

  getStates = () => {
    const url = GLOBAL.BASE_URL + 'state_list';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        key: 'state',
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //                    alert(JSON.stringify(responseJson.list))
        if (responseJson.status == true) {
          //                        console.log('yes')
          //                         var rece = responseJson.list
          //                         const transformed = rece.map(({ id, state_name }) => ({ label: state_name, value: id }));
          //                         console.log(transformed)
          this.setState({resultstates: responseJson.list});
          // //                        arrayholder = responseJson.list
        } else {
          this.setState({resultstates: []});
        }
      })
      .catch(error => {
        console.error(error);
        //  this.hideLoading()
      });
  };

  article = itemData => {
    GLOBAL.aid = itemData.item;
    this.props.navigation.navigate('ArticleDescription');
  };

  renderRowItem3 = itemData => {
    return (
      <TouchableOpacity onPress={() => this.article(itemData)}>
        <View
          style={{
            marginTop: 10,
            marginBottom: 10,
            flexDirection: 'column',
            width: '95%',
            borderRadius: 10,
            borderWidth: 1,
            borderColor: '#00000050',
            alignSelf: 'center',
            //  backgroundColor: 'yellow',
          }}>
          <ImageBackground
            imageStyle={{borderTopLeftRadius: 10, borderTopRightRadius: 10}}
            source={{uri: itemData.item.image}}
            style={{
              width: '100%',
              height: 200,
              resizeMode: 'contain',
            }}></ImageBackground>

          <Text
            style={{
              marginVertical: 10,
              marginHorizontal: 10,
              fontSize: 13,
              fontFamily: 'Konnect-Regular',
              color: 'black',
              lineHeight: 18,
              fontWeight: '600',
            }}
            numberOfLines={2}>
            {itemData.item.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View>
        <Header
          navigation={this.props.navigation}
          headerName={'ARTICLES & ANNOUNCEMENTS'}
        />
        <FlatList
          style={{
            marginBottom: 60,
            width: window.width,
          }}
          data={this.state.articles}
          horizontal={false}
          // numColumns={3}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderRowItem3}
          extraData={this.state}
        />
      </View>
    );
  }
}

export default ArticlesAndAnnouncementsList;

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    backgroundColor: 'white',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
});
