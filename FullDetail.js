import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  Modal,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView,
  Linking,
} from 'react-native';
import Button from 'react-native-button';
const window = Dimensions.get('window');
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import {Marker} from 'react-native-maps';

const GLOBAL = require('./Global');
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};
const {width, height} = Dimensions.get('window');

const SCREEN_HEIGHT = height;
const SCREEN_WIDTH = width;
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Global from './Global';

// var lat = parseFloat(position.coords.latitude)
// var long = parseFloat(position.coords.longitude)

export default class FullDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      recognized: '',
      started: '',
      text: '',
      results: [],
      practice: [],
      firstService: '',
      secondService: '',
      noloc: 0,
      timing: '',
      timings: [],
      marker: [1],
      hospital_website: '',
      cover_image: '',
      initialPosition: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
    };
  }

  componentWillUnmount() {}

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
    };
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }
  _renderItems = ({item, index}) => {
    return (
      <View>
        <Text
          style={{fontSize: 15, color: 'grey', fontFamily: 'Konnect-Regular'}}>
          {item.hospital_name}
        </Text>
        <Text
          style={{fontSize: 15, color: 'grey', fontFamily: 'Konnect-Regular'}}>
          {item.hospital_address}
        </Text>
      </View>
    );
  };

  showLoading() {
    this.setState({loading: true});
  }

  componentDidMount() {
    // alert(Global.appointmentArray.distance);
    //        console.log(GLOBAL.appointmentArray.id+'log')
    const url = GLOBAL.BASE_URL + 'full_dr_detail';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        user_id: GLOBAL.user_id,
        id: GLOBAL.appointmentArray.id,
      }),
    })
      .then(response => response.json())
      .then(responseJson => {
        //    alert(JSON.stringify(responseJson));
        console.log(JSON.stringify(responseJson));

        if (responseJson.status == true) {
          if (
            responseJson.doctor_detail.lat == '' ||
            responseJson.doctor_detail.lon == ''
          ) {
            this.setState({noloc: 1});
          } else {
            var lat = parseFloat(responseJson.doctor_detail.lat);
            var long = parseFloat(responseJson.doctor_detail.lon);

            var initialRegion = {
              latitude: lat,
              longitude: long,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            };
            this.setState({noloc: 0});
            this.setState({initialPosition: initialRegion});
            this.mapView.animateToRegion(initialRegion, 1);
          }

          this.setState({results: responseJson.doctor_detail});
          this.setState({practice: responseJson.doctor_detail.practices_at});
          this.setState({firstService: responseJson.doctor_detail.services[0]});
          this.setState({
            secondService: responseJson.doctor_detail.services[1],
          });

          this.setState({
            timings: responseJson.doctor_detail.normal_timing,

            cover_image: responseJson.doctor_detail.cover_image,
          });

          //        console.log('\x1b[36mresponseJson.doctor_detail.normal_timing\x1b[0m',JSON.stringify(responseJson.doctor_detail.normal_timing))
          //    for (var i = 0 ; i< responseJson.doctor_detail.normal_timing.length;i++){

          // var a  =  ` ${responseJson.doctor_detail.normal_timing[i].start1} - ${responseJson.doctor_detail.normal_timing[i].end1}`;
          // //var b  =  ` ${responseJson.doctor_detail.normal_timing[i].start2} - ${responseJson.doctor_detail.normal_timing[i].end2}`;
          //  this.setState({timings:b})

          //         }
          //alert(this.state.timings)
        }
      })
      .catch(error => {
        console.error(error);
        this.hideLoading();
      });
  }
  _onMapReady = () => this.setState({marginBottom: 0});

  getDirections = () => {
    //    console.log(this.state.initialPosition.latitude)
    Linking.openURL(
      'http://maps.google.com/maps?daddr=' +
        this.state.initialPosition.latitude +
        ',' +
        this.state.initialPosition.longitude,
    );
  };

  renderTimes = ({item, index}) => {
    //  console.log(item);
    return (
      <View
        style={{
          backgroundColor: 'white',
          color: 'white',
          flexDirection: 'row',
          margin: 5,
          flex: 1,
          height: 50,
          alignItems: 'center',
          borderBottomWidth: 1,
          borderBottomColor: '#e9eaeb',
          width: window.width / 1.4,
        }}>
        {item.flag == 0 && (
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 15,
                color: 'grey',
                fontFamily: 'Konnect-Medium',
                marginLeft: 10,
              }}>
              {item.day.toUpperCase()}
            </Text>
            <View style={{flexDirection: 'column', marginRight: 10}}>
              <Text
                style={{
                  fontSize: 15,
                  color: 'grey',
                  fontFamily: 'Konnect-Regular',
                }}>
                {item.start1} - {item.end1}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  color: 'grey',
                  fontFamily: 'Konnect-Regular',
                }}>
                {item.start2} - {item.end2}
              </Text>
            </View>
          </View>
        )}

        {item.flag == 1 && (
          <View
            style={{
              width: '100%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: 15,
                color: 'green',
                fontFamily: 'Konnect-Medium',
                marginLeft: 10,
              }}>
              {item.day.toUpperCase()}
            </Text>
            <View style={{flexDirection: 'column', marginRight: 10}}>
              <Text
                style={{
                  fontSize: 15,
                  color: 'green',
                  fontFamily: 'Konnect-Regular',
                }}>
                {item.start1} - {item.end1}
              </Text>
              <Text
                style={{
                  fontSize: 15,
                  color: 'green',
                  fontFamily: 'Konnect-Regular',
                }}>
                {item.start2} - {item.end2}
              </Text>
            </View>
          </View>
        )}
      </View>
    );
  };

  render() {
    //         console.log(this.state.initialPosition)
    //     console.log(JSON.stringify(GLOBAL.appointmentArray))

    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    // console.log(GLOBAL.appointmentArray)

    return (
      <ScrollView style={styles.container}>
        <View style={{flexDirection: 'column'}}>
          <View style={{flexDirection: 'row', margin: 10}}>
            <Image
              style={{
                width: 20,
                height: 20,
                resizeMode: 'contain',
                marginTop: 3,
              }}
              source={require('./dplus.png')}
            />
            <View style={{flexDirection: 'column', marginLeft: 30}}>
              <Text
                style={{
                  fontSize: 15,
                  color: 'black',
                  fontFamily: 'Konnect-Medium',
                }}>
                Experience
              </Text>
              <Text style={{fontSize: 15, color: 'grey'}}>
                {GLOBAL.appointmentArray.experience} yrs
              </Text>
            </View>
            <Image
              style={{
                width: 20,
                height: 20,
                resizeMode: 'contain',
                marginTop: 3,
                marginLeft: window.width / 6,
              }}
              source={require('./loc.png')}
            />
            <View style={{flexDirection: 'column', marginLeft: 30}}>
              <Text
                style={{
                  fontSize: 15,
                  color: 'black',
                  fontFamily: 'Konnect-Medium',
                }}>
                Distance
              </Text>
              <Text style={{fontSize: 15, color: 'grey'}}>
                {GLOBAL.appointmentArray.distance} Km
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            width: window.width - 20,
            height: 1,
            backgroundColor: '#bfbfbf',
            marginLeft: 10,
            marginRight: 10,
            marginTop: 5,
          }}></View>

        <View style={{flexDirection: 'row', margin: 10}}>
          <Image
            style={{width: 20, height: 20, resizeMode: 'contain', marginTop: 3}}
            source={require('./dre.png')}
          />
          <View style={{flexDirection: 'column', marginLeft: 30}}>
            <Text
              style={{
                fontSize: 15,
                color: 'black',
                fontFamily: 'Konnect-Medium',
              }}>
              Reviews
            </Text>
            <Text style={{fontSize: 15, color: 'grey'}}>
              {GLOBAL.appointmentArray.total_review}{' '}
            </Text>
          </View>
        </View>

        <View
          style={{
            width: window.width - 20,
            height: 1,
            backgroundColor: '#bfbfbf',
            marginLeft: 10,
            marginRight: 10,
            marginTop: 5,
          }}></View>

        <View style={{flexDirection: 'row', margin: 10, width: '95%'}}>
          <Image
            style={{width: 20, height: 20, resizeMode: 'contain', marginTop: 3}}
            source={require('./dhouse.png')}
          />
          <View style={{flexDirection: 'column', marginLeft: 30, width: '80%'}}>
            <Text
              style={{
                fontSize: 15,
                color: 'grey',
                width: '100%',
                fontFamily: 'Konnect-Regular',
                marginBottom: 5,
              }}>
              {this.state.results.lat_long_address}
            </Text>

            {this.state.noloc == 1 && (
              <Text
                style={{
                  fontSize: 15,
                  color: 'black',
                  marginTop: 5,
                  fontFamily: 'Konnect-Regular',
                }}>
                No location added!
              </Text>
            )}

            {this.state.noloc == 0 && (
              <MapView
                clusterColor="#77869E"
                clusterTextColor="white"
                clusterBorderColor="#77869E"
                clusterBorderWidth={4}
                showsUserLocation={true}
                showsMyLocationButton={false}
                pitchEnabled={true}
                rotateEnabled={true}
                zoomEnabled={true}
                scrollEnabled={true}
                onMapReady={this._onMapReady}
                ref={ref => (this.mapView = ref)}
                region={this.state.initialPosition}
                style={{
                  width: 300,
                  height: 250,
                  marginBottom: this.state.marginBottom,
                }}>
                {this.state.marker.map(marker => (
                  <Marker
                    coordinate={this.state.initialPosition}
                    title={GLOBAL.appointmentArray.name}
                    description={this.state.results.lat_long_address}
                  />
                ))}
              </MapView>
            )}
          </View>
        </View>
        <Button
          style={{
            padding: 7,
            marginTop: '2%',
            fontSize: 15,
            color: 'white',
            alignSelf: 'center',
            backgroundColor: '#800000',
            width: '40%',
            fontFamily: 'Konnect-Medium',
            borderRadius: 4,
          }}
          styleDisabled={{color: 'black', backgroundColor: 'grey'}}
          onPress={() => this.getDirections()}>
          GET DIRECTIONS
        </Button>

        <View style={{flexDirection: 'row', margin: 10}}>
          <Image
            style={{width: 20, height: 20, resizeMode: 'contain', marginTop: 3}}
            source={require('./dtime.png')}
          />
          <View style={{flexDirection: 'column', marginLeft: 30}}>
            <Text
              style={{
                fontSize: 15,
                color: '#8BC60D',
                fontFamily: 'Konnect-Medium',
              }}>
              AVAILABLE TIMINGS
            </Text>
            {this.state.timings.length == 0 && (
              <Text
                style={{
                  fontSize: 15,
                  color: 'black',
                  marginTop: 5,
                  fontFamily: 'Konnect-Regular',
                }}>
                No time slots available!
              </Text>
            )}

            {this.state.timings.length != 0 && (
              <FlatList
                style={{
                  elevation: 5,
                  backgroundColor: 'white',
                  marginTop: 5,
                  borderRadius: 5,
                }}
                data={this.state.timings}
                renderItem={this.renderTimes}
                keyExtractor={(item, index) => index.toString()}
              />
            )}
          </View>
        </View>

        <View style={{flexDirection: 'row', margin: 10}}>
          <Image
            style={{width: 20, height: 20, resizeMode: 'contain', marginTop: 3}}
            source={require('./dtick.png')}
          />
          <View style={{flexDirection: 'column', marginLeft: 30}}>
            <Text
              style={{
                fontSize: 15,
                color: '#010101',
                fontFamily: 'Konnect-Medium',
              }}>
              SERVICES
            </Text>

            <Text
              style={{
                fontSize: 15,
                color: 'grey',
                marginTop: 5,
                fontFamily: 'Konnect-Regular',
                marginLeft: 5,
              }}>
              -{this.state.firstService}
            </Text>
            <Text
              style={{
                fontSize: 15,
                color: 'grey',
                marginTop: 5,
                fontFamily: 'Konnect-Regular',
                marginLeft: 5,
              }}>
              -{this.state.secondService}
            </Text>
          </View>
        </View>

        <View style={{flexDirection: 'row', margin: 10}}>
          <Image
            style={{width: 20, height: 20, resizeMode: 'contain', marginTop: 3}}
            source={require('./dtick.png')}
          />
          <View style={{flexDirection: 'column', marginLeft: 30}}>
            <Text
              style={{
                fontSize: 15,
                color: '#010101',
                fontFamily: 'Konnect-Medium',
              }}>
              ALSO PRACTICES AT
            </Text>
            {this.state.practice.length == 0 && (
              <Text
                style={{
                  fontSize: 12,
                  color: 'grey',
                  marginTop: 5,
                  marginLeft: 5,
                }}>
                No data available!
              </Text>
            )}
            {this.state.practice.length != 0 && (
              <FlatList
                style={{flexGrow: 0, margin: 8}}
                data={this.state.practice}
                numColumns={1}
                keyExtractor={(item, index) => index.toString()}
                renderItem={this._renderItems}
              />
            )}
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    backgroundColor: 'white',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
});
