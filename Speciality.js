import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Image,
  Alert,
  FlatList,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
  Platform,
  ImageBackground,
} from 'react-native';
import store from 'react-native-simple-store';
import {DialogComponent, DialogTitle} from 'react-native-dialog-component';
import Header from './Header.js';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 200;
const images = [];
import Carousel from 'react-native-banner-carousel';
import Button from 'react-native-button';
import {TextField} from 'react-native-material-textfield-plus';
type Props = {};

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class Speciality extends Component {
  state = {
    name: '',
    email: '',
    phone: '',
    company: '',
    loading: false,
    visible: false,
    searchText: '',
    banner: [],
    speciality: [],
    article: [],
    selected: false,
    data: [],
    results: [],
    value: 0,
  };

  static navigationOptions = ({navigation}) => {
    return {
      header: () => null,
      animations: {
        setRoot: {
          waitForRender: false,
        },
      },
    };
  };

  showLoading() {
    this.setState({loading: true});
  }

  hideLoading() {
    this.setState({loading: false});
  }

  getRespone = res => {
    this.setState({speciality: res.specialty});
  };

  componentDidMount() {
    var self = this;
    store.get('specialty').then(
      res => self.getRespone(res),

      // this.setState({speciality:res.specialty})
    );
  }

  login = () => {
    if (this.state.value == 0) {
      GLOBAL.myonlinetype = 'online';
      this.props.navigation.navigate('SearchSpeciality');
    } else {
      GLOBAL.myonlinetype = 'offline';
      GLOBAL.onlinetype = 'normal';
      this.props.navigation.navigate('SearchSpeciality');
    }
  };

  selectedFirstsd = item => {
    this.dialogComponent.show();
    GLOBAL.searchSpeciality = item;
    //        this.props.navigation.navigate('SearchSpeciality')
  };

  renderRowItem2 = itemData => {
    var index = itemData.index;

    return (
      <TouchableOpacity
        onPress={() => this.selectedFirstsd(itemData.item.title)}>
        <View
          style={{
            width: window.width / 2 - 12,
            margin: 4,
            height: 125,
            borderRadius: 30,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
          }}>
          {index % 9 == 0 && (
            <Image
              source={require('./firsts.png')}
              style={{
                width: window.width / 2 - 12,
                height: 125,
                borderRadius: 22,
              }}
            />
          )}

          {index % 9 == 1 && (
            <Image
              source={require('./seconds.png')}
              style={{
                width: window.width / 2 - 12,
                height: 125,
                borderRadius: 22,
              }}
            />
          )}

          {index % 9 == 2 && (
            <Image
              source={require('./thirds.png')}
              style={{
                width: window.width / 2 - 12,
                height: 125,
                borderRadius: 22,
              }}
            />
          )}

          {index % 9 == 3 && (
            <Image
              source={require('./patient.png')}
              style={{
                width: window.width / 2 - 12,
                height: 125,
                borderRadius: 22,
              }}
            />
          )}

          {index % 9 == 4 && (
            <Image
              source={require('./doctor.png')}
              style={{
                width: window.width / 2 - 12,
                height: 125,
                borderRadius: 22,
              }}
            />
          )}

          {index % 9 == 5 && (
            <Image
              source={require('./lab.png')}
              style={{
                width: window.width / 2 - 12,
                height: 125,
                borderRadius: 22,
              }}
            />
          )}

          {index % 9 == 6 && (
            <Image
              source={require('./ambulance.png')}
              style={{
                width: window.width / 2 - 12,
                height: 125,
                borderRadius: 22,
              }}
            />
          )}
          {index % 9 == 7 && (
            <Image
              source={require('./healthcare.png')}
              style={{
                width: window.width / 2 - 12,
                height: 125,
                borderRadius: 22,
              }}
            />
          )}
          {index % 9 == 8 && (
            <Image
              source={require('./report.png')}
              style={{
                width: window.width / 2 - 12,
                height: 125,
                borderRadius: 22,
              }}
            />
          )}

          <Image
            source={{uri: itemData.item.image}}
            style={{
              width: 45,
              height: 45,
              marginTop: -80,
              marginLeft: 5,
              resizeMode: 'contain',
            }}
          />

          <Text
            style={{
              fontSize: 11,
              margin: 5,
              marginLeft: 10,
              fontFamily: 'Konnect-Regular',
              color: 'white',
            }}>
            {itemData.item.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  renderRowItem3 = itemData => {
    return (
      <View
        style={{
          width: window.width / 2.5 - 8,
          margin: 4,
          borderRadius: 30,
          shadowColor: '#000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
        }}>
        <Image
          source={{uri: itemData.item.image}}
          style={{
            width: window.width / 2.5 - 12,
            height: 100,
            borderRadius: 22,
          }}
        />
        <Text
          style={{
            fontSize: 13,
            margin: 5,
            fontFamily: 'Konnect-Regular',
            color: 'black',
          }}>
          {itemData.item.title}
        </Text>
      </View>
    );
  };

  selectedFirst = () => {
    this.dialogComponent.show();
  };

  renderRowItem1 = itemData => {
    return (
      <TouchableOpacity onPress={() => this.selectedFirst(itemData.index)}>
        <View
          style={{
            width: window.width / 3 - 8,
            margin: 4,
            height: 100,
            borderRadius: 30,
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 2,
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
          }}>
          <Image
            source={itemData.item.image}
            style={{
              width: window.width / 3 - 12,
              height: 100,
              borderRadius: 22,
            }}
          />
          <Image
            source={itemData.item.back}
            style={{
              width: 45,
              height: 45,
              marginTop: -80,
              marginLeft: 5,
              resizeMode: 'contain',
            }}
          />

          <Text
            style={{
              fontSize: 11,
              margin: 5,
              fontFamily: 'Konnect-Regular',
              color: 'white',
            }}>
            {itemData.item.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  check = () => {
    this.setState({isSecure: !this.state.isSecure});
  };

  getIndex = index => {
    this.setState({email: this.state.data[index].id});
  };
  setSearchText(e) {
    this.setState({
      searchText: e.target.value,
    });
  }
  render() {
    var radio_props = [
      {label: 'Online Consultation', value: 0},
      {label: 'Offline Consultation', value: 1},
    ];

    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator
            style={styles.loading}
            size="large"
            color="#800000"
          />
        </View>
      );
    }
    return (
      <View>
        <Header
          navigation={this.props.navigation}
          headerName={'SPECIALITIES'}
        />
        <FlatList
          style={{
            marginTop: 10,
            marginLeft: 5,
            width: window.width - 10,
            marginBottom: 60,
          }}
          data={this.state.speciality}
          numColumns={2}
          keyExtractor={this._keyExtractor}
          renderItem={this.renderRowItem2}
          extraData={this.state}
        />

        <DialogComponent
          dialogStyle={{backgroundColor: 'transparent'}}
          dialogTitle={<DialogTitle title="Dialog Title" />}
          dismissOnTouchOutside={true}
          dismissOnHardwareBackPress={true}
          ref={dialogComponent => {
            this.dialogComponent = dialogComponent;
          }}>
          <View
            style={{
              width: window.width - 30,
              alignSelf: 'center',
              backgroundColor: 'transparent',
            }}>
            <View
              style={{
                backgroundColor: '#6d0000',
                width: '100%',
                height: 50,
                justifyContent: 'space-between',
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  margin: 15,
                  color: 'white',
                  fontFamily: 'Konnect-Regular',
                  fontSize: 17,
                }}>
                Choose Booking Type
              </Text>
              <TouchableOpacity onPress={() => this.dialogComponent.dismiss()}>
                <Image
                  style={{
                    width: 25,
                    height: 25,
                    resizeMode: 'contain',
                    margin: 15,
                  }}
                  source={require('./cross.png')}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                width: window.width - 30,
                backgroundColor: 'white',
                height: 140,
              }}>
              <View
                style={{
                  marginTop: 20,
                  width: '100%',
                  marginLeft: 20,
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <RadioForm
                  radio_props={radio_props}
                  initial={0}
                  buttonColor={'#800000'}
                  buttonOuterColor={'#800000'}
                  selectedButtonColor={'#800000'}
                  animation={false}
                  buttonSize={12}
                  labelColor={'black'}
                  buttonStyle={{marginTop: 20}}
                  buttonWrapStyle={{marginTop: 20}}
                  labelStyle={{
                    fontSize: 16,
                    fontFamily: 'Konnect-Regular',
                    width: '90%',
                    marginLeft: 10,
                    paddingBottom: 10,
                  }}
                  onPress={value => {
                    this.setState({value: value});
                  }}
                />
              </View>

              <Button
                style={{
                  padding: 6,
                  marginTop: 5,
                  width: 100,
                  fontSize: 14,
                  color: 'white',
                  backgroundColor: '#800000',
                  alignSelf: 'center',
                  height: 30,
                  fontFamily: 'Konnect-Regular',
                  borderRadius: 4,
                }}
                styleDisabled={{color: 'red'}}
                onPress={() => this.login()}>
                SUBMIT
              </Button>
            </View>
          </View>
        </DialogComponent>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},
  container: {
    backgroundColor: 'white',
  },
  loading: {
    position: 'absolute',
    left: window.width / 2 - 30,

    top: window.height / 2,

    opacity: 0.5,

    justifyContent: 'center',
    alignItems: 'center',
  },
});
